----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/03/2019 09:30:10 AM
-- Design Name: 
-- Module Name: registerController - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity registerController is
    Port ( transferSizePerChannel : out STD_LOGIC_VECTOR (31 downto 0);
           channels : out STD_LOGIC_VECTOR (31 downto 0);
           transferTotal : out STD_LOGIC_VECTOR (31 downto 0);
           clockSetup : out STD_LOGIC_VECTOR (31 downto 0);
           timeoutMin : out std_logic_vector(31 downto 0);
           timeoutMax : out std_logic_vector(31 downto 0);
           resetCPU : out std_logic_vector(31 downto 0);
           sizePerFrame : out std_logic_vector(31 downto 0);
           stream0status : in STD_LOGIC_VECTOR (31 downto 0);
           stream1status : in STD_LOGIC_VECTOR (31 downto 0);
           stream2status : in STD_LOGIC_VECTOR (31 downto 0);
           stream3status : in STD_LOGIC_VECTOR (31 downto 0);
           stream4status : in STD_LOGIC_VECTOR (31 downto 0);
           stream5status : in STD_LOGIC_VECTOR (31 downto 0);
           stream6status : in STD_LOGIC_VECTOR (31 downto 0);
           stream7status : in STD_LOGIC_VECTOR (31 downto 0);
           stream8status : in STD_LOGIC_VECTOR (31 downto 0);
           stream9status : in STD_LOGIC_VECTOR (31 downto 0);
           gtwizardstatus : in STD_LOGIC_VECTOR (31 downto 0);
           gtresetdone : in STD_LOGIC_VECTOR (9 downto 0);
           gtFSMresetdone : in STD_LOGIC_VECTOR (9 downto 0);
           databufferstatus : in STD_LOGIC_VECTOR (31 downto 0);
           resetStatus : in STD_LOGIC_VECTOR (31 downto 0);
           moddetrxlosstatus : in STD_LOGIC_VECTOR (31 downto 0);
               MEASURED_TEMP      : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_VCCAUX    : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_VCCINT    : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_VCCBRAM   : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_AUX0      : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_AUX1      : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_AUX2      : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           MEASURED_AUX3      : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           ALM                : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           firmware_version                : in  STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
           sfp_addr : out std_logic_vector(6 downto 0);
           sfp_dout : in std_logic_vector(31 downto 0);
           clk : in STD_LOGIC;
           addrb : out std_logic_vector(31 downto 0);
           clkb : out std_logic;
           dinb : out std_logic_vector(31 downto 0);
           doutb : in std_logic_vector(31 downto 0);
           enb : out std_logic;
           rstb : out std_logic;
           web : out std_logic_vector(3 downto 0);
           polling : out std_logic;
           frame0 : out std_logic_vector(31 downto 0);
           frame0_reset : in std_logic;
           frame1 : out std_logic_vector(31 downto 0);
           frame1_reset : in std_logic;
           frame2 : out std_logic_vector(31 downto 0);
           frame2_reset : in std_logic;
           frame3 : out std_logic_vector(31 downto 0);
           frame3_reset : in std_logic;
           frame4 : out std_logic_vector(31 downto 0);
           frame4_reset : in std_logic;
           frame5 : out std_logic_vector(31 downto 0);
           frame5_reset : in std_logic;
           frame6 : out std_logic_vector(31 downto 0);
           frame6_reset : in std_logic;
           frame7 : out std_logic_vector(31 downto 0);
           frame7_reset : in std_logic;
           frame8 : out std_logic_vector(31 downto 0);
           frame8_reset : in std_logic;
           frame9 : out std_logic_vector(31 downto 0);
           frame9_reset : in std_logic  ;
           interrupt_reset : out std_logic_vector(9 downto 0)
           );
end registerController;

architecture Behavioral of registerController is
 ATTRIBUTE X_INTERFACE_INFO : STRING;
ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
ATTRIBUTE X_INTERFACE_INFO OF doutb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT DOUT";
ATTRIBUTE X_INTERFACE_INFO OF dinb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT DIN";
ATTRIBUTE X_INTERFACE_INFO OF addrb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT ADDR";
ATTRIBUTE X_INTERFACE_INFO OF web: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT WE";
ATTRIBUTE X_INTERFACE_INFO OF enb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT EN";
ATTRIBUTE X_INTERFACE_INFO OF rstb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT RST";
ATTRIBUTE X_INTERFACE_PARAMETER OF clkb: SIGNAL IS "XIL_INTERFACENAME BRAM_PORTOUT, MEM_SIZE 4096, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE BRAM_CTRL, READ_WRITE_MODE READ_WRITE";
ATTRIBUTE X_INTERFACE_INFO OF clkb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT CLK";
type mainState is (start,setRegister0,setRegister1);
signal state : mainState:=start;
signal waitCounter : integer:=0;
signal muxCounter : integer:=0;
 
signal transferSizePerChannel_s :  STD_LOGIC_VECTOR (31 downto 0);
signal channels_s :  STD_LOGIC_VECTOR (31 downto 0);
signal transferTotal_s :  STD_LOGIC_VECTOR (31 downto 0);
signal clockSetup_s :  STD_LOGIC_VECTOR (31 downto 0);
signal timeoutMin_s :  std_logic_vector(31 downto 0);
signal timeoutMax_s :  std_logic_vector(31 downto 0);
signal resetCPU_s :  std_logic_vector(31 downto 0);
signal sizePerFrame_s :  std_logic_vector(31 downto 0);
signal stream0status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream1status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream2status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream3status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream4status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream5status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream6status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream7status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream8status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal stream9status_s :  STD_LOGIC_VECTOR (31 downto 0);
signal gtwizardstatus_s : STD_LOGIC_VECTOR (31 downto 0);
signal gtresetdone_s :  STD_LOGIC_VECTOR (31 downto 0);
signal databufferstatus_s :  STD_LOGIC_VECTOR (31 downto 0);
signal resetStatus_s :  STD_LOGIC_VECTOR (31 downto 0);
signal moddetrxlosstatus_s :  STD_LOGIC_VECTOR (31 downto 0);

signal           MEASURED_TEMP_s      :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_VCCAUX_s    :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_VCCINT_s    :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_VCCBRAM_s   :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_AUX0_s      :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_AUX1_s      :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_AUX2_s      :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           MEASURED_AUX3_s      :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           ALM_s                :   STD_LOGIC_VECTOR (31 downto 0);   -- Output data bus for dynamic reconfiguration port
signal           firmware_version_s                :   STD_LOGIC_VECTOR (31 downto 0);   

signal addrb_s :  std_logic_vector(31 downto 0);
signal dinb_s :  std_logic_vector(31 downto 0);
signal doutb_s :  std_logic_vector(31 downto 0);
signal enb_s :  std_logic;
signal rstb_s :  std_logic;
signal web_s :  std_logic_vector(3 downto 0);

signal polling_s : std_logic;


constant maxRegister  : integer := 146;
signal set : std_logic:='0';
begin

MEASURED_TEMP_s<=MEASURED_TEMP;
MEASURED_VCCAUX_s<=MEASURED_VCCAUX;
MEASURED_VCCINT_s<=MEASURED_VCCINT;
MEASURED_VCCBRAM_s<=MEASURED_VCCBRAM;
MEASURED_AUX0_s<=MEASURED_AUX0;
MEASURED_AUX1_s<=MEASURED_AUX1;
MEASURED_AUX2_s<=MEASURED_AUX2;
MEASURED_AUX3_s<=MEASURED_AUX3;
ALM_s<=ALM;
firmware_version_s<=firmware_version;

transferSizePerChannel<=transferSizePerChannel_s;
channels<=channels_s;
transferTotal<=transferTotal_s;
clockSetup<=clockSetup_s;
timeoutMin<=timeoutMin_s;
timeoutMax<=timeoutMax_s;
resetCPU<=resetCPU_s;
sizePerFrame<=sizePerFrame_s;
addrb<=addrb_s;
dinb<=dinb_s;
doutb_s<=doutb;
enb<=enb_s;
rstb<=rstb_s;
web<=web_s;

stream0status_s<=stream0status;
stream1status_s<=stream1status;
stream2status_s<=stream2status;
stream3status_s<=stream3status;
stream4status_s<=stream4status;
stream5status_s<=stream5status;
stream6status_s<=stream6status;
stream7status_s<=stream7status;
stream8status_s<=stream8status;
stream9status_s<=stream9status;
gtwizardstatus_s<=gtwizardstatus;
gtresetdone_s(9 downto 0)<=gtresetdone;
gtresetdone_s(19 downto 10)<=gtFSMresetdone;
gtresetdone_s(31 downto 20)<=(others=>'0');
databufferstatus_s<=databufferstatus;
resetStatus_s<=resetStatus;
moddetrxlosstatus_s<=moddetrxlosstatus;
clkb<=clk;
rstb_s<='0';
enb_s<='1';

polling<=polling_s;


process(clk)
begin
if rising_edge(clk) then
    addrb_s<=std_logic_vector(to_unsigned(muxCounter*4,addrb_s'length));
    case(muxCounter) is
    when 0=>
        if set='1' then
            transferSizePerChannel_s<=doutb;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 1=>
        if set='1' then
            channels_s<=doutb_s;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 2=>
        if set='1' then
            transferTotal_s<=doutb_s;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 3=>
        if set='1' then
            clockSetup_s<=doutb_s;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 4=>
        if set='1' then
            timeoutMin_s<=doutb_s;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 5=>
        if set='1' then
            timeoutMax_s<=doutb_s;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 6=>
        if set='1' then
            sizePerFrame_s<=doutb_s;
        end if;
        web_s<="0000";
        dinb_s<=(others=>'0');
    when 7=>
            if set='1' then
                resetCPU_s<=doutb_s;
            end if;
            web_s<="0000";
            dinb_s<=(others=>'0');
    when 8=>
      web_s<="1111";
      dinb_s<=stream0status_s;
    when 9=>
      web_s<="1111";
      dinb_s<=stream1status_s;
    when 10=>
      web_s<="1111";
      dinb_s<=stream2status_s;
    when 11=>
      web_s<="1111";
      dinb_s<=stream3status_s;
    when 12=>
        web_s<="1111";
        dinb_s<=stream4status_s;
    when 13=>
          web_s<="1111";
          dinb_s<=stream5status_s;
    when 14=>
            web_s<="1111";
            dinb_s<=stream6status_s;
    when 15=>
              web_s<="1111";
              dinb_s<=stream7status_s;
     when 16=>
                web_s<="1111";
                dinb_s<=stream8status_s;
    when 17=>
                  web_s<="1111";
                  dinb_s<=stream9status_s;
    when 18=>
        web_s<="1111";
        dinb_s<=gtwizardstatus_s;
    when 19=>
        web_s<="1111";
        dinb_s<=gtresetdone_s;
    when 20=>
            web_s<="1111";
            dinb_s<=databufferstatus_s;
   when 22=>
         web_s<="1111";
         dinb_s<=resetStatus_s;
   when 23=>
         web_s<="1111";
         dinb_s<=moddetrxlosstatus_s;
   when 24=>
         web_s<="1111";
         dinb_s<=MEASURED_TEMP_s;     
   when 25=>
         web_s<="1111";
         dinb_s<=MEASURED_VCCAUX_s;
     when 26=>
          web_s<="1111";
          dinb_s<=MEASURED_VCCINT_s;
     when 27=>
          web_s<="1111";
          dinb_s<=MEASURED_VCCBRAM_s;
     when 28=>
          web_s<="1111";
          dinb_s<=MEASURED_AUX0_s;
     when 29=>
          web_s<="1111";
          dinb_s<=MEASURED_AUX1_s;
     when 30=>
          web_s<="1111";
          dinb_s<=MEASURED_AUX2_s;
     when 31=>
          web_s<="1111";
          dinb_s<=MEASURED_AUX3_s;
      when 32=>
          web_s<="1111";
          dinb_s<=ALM_s;
      when 33 to 133 =>
            sfp_addr<=std_logic_vector(to_unsigned(muxCounter-33,sfp_addr'length));
            if set='1' then
                dinb_s<=sfp_dout;
                web_s<="1111";
            else
                web_s<="0000";
            end if;
     when 134=>
            if set='1' then
                polling_s<=doutb_s(0);
            end if;
            web_s<="0000";
            dinb_s<=(others=>'0');
    when 135=>
          if frame0_reset='1' then
            web_s<="1111";
            dinb_s<=(others=>'0');
            frame0<=(others=>'0');
          else
              if set='1' then
                frame0<=doutb_s;
              end if;
          end if;
          
    when 136=>
        if frame1_reset='1' then
          web_s<="1111";
          dinb_s<=(others=>'0');
          frame1<=(others=>'0');
        else
            if set='1' then
                frame1<=doutb_s;
            end if;
        end if; 
    when 137=>
          if frame2_reset='1' then
            web_s<="1111";
            dinb_s<=(others=>'0');
            frame2<=(others=>'0');
          else
              if set='1' then
                  frame2<=doutb_s;
              end if;
           end if;  
    when 138=>
        if frame3_reset='1' then
          web_s<="1111";
          dinb_s<=(others=>'0');
          frame3<=(others=>'0');
        else
            if set='1' then
                frame3<=doutb_s;
            end if;
        end if;                 
    when 139=>
          if frame4_reset='1' then
            web_s<="1111";
            dinb_s<=(others=>'0');
            frame4<=(others=>'0');
          else
              if set='1' then
                  frame4<=doutb_s;
              end if;
          end if;   
    when 140=>
        if frame5_reset='1' then
          web_s<="1111";
          dinb_s<=(others=>'0');
          frame5<=(others=>'0');
        else
            if set='1' then
                frame5<=doutb_s;
            end if;
        end if;
    when 141=>
          if frame6_reset='1' then
            web_s<="1111";
            dinb_s<=(others=>'0');
            frame6<=(others=>'0');
          else
              if set='1' then
                  frame6<=doutb_s;
              end if;
          end if;   
    when 142=>
        if frame7_reset='1' then
          web_s<="1111";
          dinb_s<=(others=>'0');
          frame7<=(others=>'0');
        else
            if set='1' then
                frame7<=doutb_s;
            end if;
        end if;   
    when 143=>
          if frame8_reset='1' then
            web_s<="1111";
            dinb_s<=(others=>'0');
            frame8<=(others=>'0');
          else
              if set='1' then
                  frame8<=doutb_s;
              end if;
          end if;   
    when 144=>
        if frame9_reset='1' then
          web_s<="1111";
          dinb_s<=(others=>'0');
          frame9<=(others=>'0');
        else
            if set='1' then
                frame9<=doutb_s;
            end if;
        end if;
     when 145=>
        web_s<="1111";
        dinb_s<=firmware_version_s;   
    when 146=>
            if set='1' then
                interrupt_reset<=doutb_s(9 downto 0);
            end if;
            web_s<="0000";
            dinb_s<=(others=>'0');         
    when others=>
        web_s<="0000";
    end case;

end if;
end process;


process(clk)
begin
if rising_edge(clk) then
  case(state) is
    when start=>
      state<=setRegister0;
      waitCounter<=0;
      muxCounter<=0;
      set<='0';
    when setRegister0 =>
        if waitCounter=3 then
           state<=setRegister1;
           waitCounter<=0;
        else
            waitCounter<=waitCounter+1;
        end if;
    when setRegister1=>
        if waitCounter=3 then
           state<=setRegister0;
           waitCounter<=0;
           set<='0';
           if muxCounter>=maxRegister then
              muxCounter<=0;
           else
              muxCounter<=muxCounter+1;
           end if;
        else
            waitCounter<=waitCounter+1;
            set<='1';
        end if;
    when others=>
      null;
  end case;

end if;




end process;


end Behavioral;
