----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/24/2019 05:41:10 PM
-- Design Name: 
-- Module Name: sim_gt_controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

use std.textio.all;
use ieee.std_logic_textio.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sim_gt_wizard is
port
(
     Q4_CLK0_GTREFCLK_PAD_N_IN               : in   std_logic;
Q4_CLK0_GTREFCLK_PAD_P_IN               : in   std_logic;
   sysclk                       : in std_logic;
   reset : in std_logic;
RXN_IN                                  : in   std_logic_vector(9 downto 0);
RXP_IN                                  : in   std_logic_vector(9 downto 0);
GT0_RXUSRCLK2 : out std_logic;
gt0 : out std_logic_vector(103 downto 0);
gt0_reset : in std_logic;
GT1_RXUSRCLK2 : out std_logic;
gt1 : out std_logic_vector(103 downto 0);
gt1_reset : in std_logic;
GT2_RXUSRCLK2 : out std_logic;
gt2 : out std_logic_vector(103 downto 0);
gt2_reset : in std_logic;
GT3_RXUSRCLK2 : out std_logic;
gt3 : out std_logic_vector(103 downto 0);
gt3_reset : in std_logic;
GT4_RXUSRCLK2 : out std_logic;
gt4 : out std_logic_vector(103 downto 0);
gt4_reset : in std_logic;
GT5_RXUSRCLK2 : out std_logic;
gt5 : out std_logic_vector(103 downto 0);
gt5_reset : in std_logic;
GT6_RXUSRCLK2 : out std_logic;
gt6 : out std_logic_vector(103 downto 0);
gt6_reset : in std_logic;
GT7_RXUSRCLK2 : out std_logic;
gt7 : out std_logic_vector(103 downto 0);
gt7_reset : in std_logic;
GT8_RXUSRCLK2 : out std_logic;
gt8 : out std_logic_vector(103 downto 0);
gt8_reset : in std_logic;
GT9_RXUSRCLK2 : out std_logic;
gt9 : out std_logic_vector(103 downto 0);
gt9_reset : in std_logic;
crc32 : out std_logic_vector(31 downto 0);
crc64: out std_logic_vector(63 downto 0);
crc_reset : out std_logic;
clk25mhz : in std_logic
);
end sim_gt_wizard;

architecture Behavioral of sim_gt_wizard is

   constant C_FILE_NAME :string  := "new_data.txt";
   constant C_DATA1_W   :integer := 104;

signal data_out: std_logic_vector(103 downto 0):=(others=>'0');
begin
--gt0<=data_out;
GT0_RXUSRCLK2<=sysclk;
GT1_RXUSRCLK2<=sysclk;
GT2_RXUSRCLK2<=sysclk;
GT3_RXUSRCLK2<=sysclk;
GT4_RXUSRCLK2<=sysclk;
GT5_RXUSRCLK2<=sysclk;
GT6_RXUSRCLK2<=sysclk;
GT7_RXUSRCLK2<=sysclk;
GT8_RXUSRCLK2<=sysclk;
GT9_RXUSRCLK2<=sysclk;

crcout:
    process(clk25mhz,reset)
        --variable count: integer range 0 to 2;
        variable toggle:  std_logic:='0';
    begin

        if rising_edge(clk25mhz) then
            if reset='0' then
            if toggle = '0' then
                crc32<=data_out(71 downto 40);
                crc64<=data_out(103 downto 40);
                if data_out(103 downto 72)/=x"00000000" then
                    crc_reset<='0';
                end if;
            else
                crc32<=data_out(103 downto 72);
            end if;
            toggle:=not toggle;
            else
                crc32<=(others=>'0');
                crc64<=(others=>'0');
                crc_reset<='1';
            end if;
            end if;
      end process;
      


main: process
variable counter:integer;

   variable fstatus       :file_open_status;
   
   variable file_line     :line;
   variable var_data1     :std_logic_vector(C_DATA1_W-1 downto 0);
   file fptr: text;

begin
var_data1 := (others => '0');
--eof       <= '0';
   file_open(fstatus, fptr, C_FILE_NAME, read_mode);
   while (not endfile(fptr)) loop
      wait until rising_edge(sysclk);
      readline(fptr, file_line);
        read(file_line, var_data1);
        gt0      <= var_data1;
        gt1      <= (others => '0');-- var_data1;
        gt2      <= (others => '0');--var_data1;
        gt3      <= (others => '0');--var_data1;
        gt4      <= (others => '0');--var_data1;
        gt5      <= var_data1;
        gt6      <= (others => '0');--var_data1;
        gt7      <= (others => '0');--var_data1;
        gt8      <= (others => '0');--var_data1;
        gt9      <= (others => '0');--var_data1;
end loop;
 -- eof       <= '1';
 file_close(fptr);
	assert false
    report "simulation ended"
    severity failure;
    
end process;

main1: process
variable counter:integer;

   variable fstatus       :file_open_status;
   
   variable file_line     :line;
   variable var_data1     :std_logic_vector(C_DATA1_W-1 downto 0);
   file fptr: text;

begin
var_data1 := (others => '0');
--eof       <= '0';
   file_open(fstatus, fptr, C_FILE_NAME, read_mode);
   while (not endfile(fptr)) loop
      wait until rising_edge(sysclk);
      readline(fptr, file_line);
        read(file_line, var_data1);
    --    gt2      <= var_data1;
     --   gt3      <= var_data1;
end loop;
 -- eof       <= '1';
 file_close(fptr);
	assert false
    report "simulation ended"
    severity failure;
    
end process;

main2: process
variable counter:integer;

   variable fstatus       :file_open_status;
   
   variable file_line     :line;
   variable var_data1     :std_logic_vector(C_DATA1_W-1 downto 0);
   file fptr: text;

begin
var_data1 := (others => '0');
--eof       <= '0';
   file_open(fstatus, fptr, C_FILE_NAME, read_mode);
   while (not endfile(fptr)) loop
      wait until rising_edge(sysclk);
      readline(fptr, file_line);
        read(file_line, var_data1);
       -- gt4      <= var_data1;
       -- gt5      <= var_data1;
end loop;
 -- eof       <= '1';
 file_close(fptr);
	assert false
    report "simulation ended"
    severity failure;
    
end process;

main3: process
variable counter:integer;

   variable fstatus       :file_open_status;
   
   variable file_line     :line;
   variable var_data1     :std_logic_vector(C_DATA1_W-1 downto 0);
   file fptr: text;

begin
var_data1 := (others => '0');
--eof       <= '0';
   file_open(fstatus, fptr, C_FILE_NAME, read_mode);
   while (not endfile(fptr)) loop
      wait until rising_edge(sysclk);
      readline(fptr, file_line);
        read(file_line, var_data1);
        --gt6      <= var_data1;
        --gt7      <= var_data1;
end loop;
 -- eof       <= '1';
 file_close(fptr);
	assert false
    report "simulation ended"
    severity failure;
    
end process;

main4: process
variable counter:integer;

   variable fstatus       :file_open_status;
   
   variable file_line     :line;
   variable var_data1     :std_logic_vector(C_DATA1_W-1 downto 0);
   file fptr: text;

begin
var_data1 := (others => '0');
--eof       <= '0';
   file_open(fstatus, fptr, C_FILE_NAME, read_mode);
   while (not endfile(fptr)) loop
      wait until rising_edge(sysclk);
      readline(fptr, file_line);
        read(file_line, var_data1);
        --gt8      <= var_data1;
        --gt9      <= var_data1;
end loop;
 -- eof       <= '1';
 file_close(fptr);
	assert false
    report "simulation ended"
    severity failure;
    
end process;


end Behavioral;