----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/10/2019 03:58:48 PM
-- Design Name: 
-- Module Name: resetController - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity resetController is
    Port ( cpldReset : in STD_LOGIC;--50mhz
           cpuReset : in STD_LOGIC_VECTOR (31 downto 0);--50mhz
           interruptResetIn : in std_logic_vector(9 downto 0);
           resetPipeline0 : out STD_LOGIC;--gt0_clk
           resetPipeline1 : out STD_LOGIC;--gt1_clk
           resetPipeline2 : out STD_LOGIC;--gt2_clk
           resetPipeline3 : out STD_LOGIC;--gt3_clk
           resetPipeline4 : out STD_LOGIC;--gt4_clk
           resetPipeline5 : out STD_LOGIC;--gt5_clk
           resetPipeline6 : out STD_LOGIC;--gt6_clk
           resetPipeline7 : out STD_LOGIC;--gt7_clk
           resetPipeline8 : out STD_LOGIC;--gt8_clk
           resetPipeline9 : out STD_LOGIC;--gt9_clk
           resetInterrupt0: out std_logic; --axi_aclk
           resetInterrupt1: out std_logic; --axi_aclk
           resetInterrupt2: out std_logic; --axi_aclk
           resetInterrupt3: out std_logic; --axi_aclk
           resetInterrupt4: out std_logic; --axi_aclk
           resetInterrupt5: out std_logic; --axi_aclk
           resetInterrupt6: out std_logic; --axi_aclk
           resetInterrupt7: out std_logic; --axi_aclk
           resetInterrupt8: out std_logic; --axi_aclk
           resetInterrupt9: out std_logic; --axi_aclk
           clk : in STD_LOGIC;--50mhz
           resetGTWizard : out STD_LOGIC;--50mhz
           resetGTWizardInv : out STD_LOGIC;--50mhz
           resetDataBufferWriter : out STD_LOGIC;--axi_aclk
           resetDataBufferReader : out STD_LOGIC;--axi_aclk
           resetXADC : out std_logic;--50MHz
           gt0_clk : in STD_LOGIC;
           gt1_clk : in STD_LOGIC;
           gt2_clk : in STD_LOGIC;
           gt3_clk : in STD_LOGIC;
           gt4_clk : in STD_LOGIC;
           gt5_clk : in STD_LOGIC;
           gt6_clk : in STD_LOGIC;
           gt7_clk : in STD_LOGIC;
           gt8_clk : in STD_LOGIC;
           gt9_clk : in STD_LOGIC;
           axi_aclk : in STD_LOGIC;
           MODDET : in std_logic_vector(9 downto 0);
           RX_LOS : in std_logic_vector(9 downto 0);
           resetStatus : out std_logic_vector(31 downto 0);
           rxlosmodsetstatus : out std_logic_vector(31 downto 0);
           cplleset : out std_logic );
end resetController;

architecture Behavioral of resetController is
component clockDomainCrossing is
  Port (clkA : in std_logic;
        clkB : in std_logic;
        inputA : in std_logic;
        outputB: out std_logic );
end component;

component asyncToSync is
  Port (datain: in std_logic;
        dataout : out std_logic;
        clk : in std_logic );
end component;


signal cpuReset_s : std_logic_vector(31 downto 0);
signal resetGTWizard_s : std_logic;
signal resetXADC_s : std_logic;



signal resetInterrupt0_in:std_logic;
signal resetInterrupt0_out:std_logic;

signal resetInterrupt1_in:std_logic;
signal resetInterrupt1_out:std_logic;

signal resetInterrupt2_in:std_logic;
signal resetInterrupt2_out:std_logic;

signal resetInterrupt3_in:std_logic;
signal resetInterrupt3_out:std_logic;

signal resetInterrupt4_in:std_logic;
signal resetInterrupt4_out:std_logic;

signal resetInterrupt5_in:std_logic;
signal resetInterrupt5_out:std_logic;

signal resetInterrupt6_in:std_logic;
signal resetInterrupt6_out:std_logic;

signal resetInterrupt7_in:std_logic;
signal resetInterrupt7_out:std_logic;

signal resetInterrupt8_in:std_logic;
signal resetInterrupt8_out:std_logic;

signal resetInterrupt9_in:std_logic;
signal resetInterrupt9_out:std_logic;

signal resetPipeline0_in:std_logic;
signal resetPipeline0_out:std_logic;

signal resetPipeline1_in:std_logic;
signal resetPipeline1_out:std_logic;

signal resetPipeline2_in:std_logic;
signal resetPipeline2_out:std_logic;

signal resetPipeline3_in:std_logic;
signal resetPipeline3_out:std_logic;

signal resetPipeline4_in:std_logic;
signal resetPipeline4_out:std_logic;

signal resetPipeline5_in:std_logic;
signal resetPipeline5_out:std_logic;

signal resetPipeline6_in:std_logic;
signal resetPipeline6_out:std_logic;

signal resetPipeline7_in:std_logic;
signal resetPipeline7_out:std_logic;

signal resetPipeline8_in:std_logic;
signal resetPipeline8_out:std_logic;

signal resetPipeline9_in:std_logic;
signal resetPipeline9_out:std_logic;

signal resetDataBufferWriter_in:std_logic;
signal resetDataBufferWriter_out:std_logic;

signal resetDataBufferReader_in:std_logic;
signal resetDataBufferReader_out:std_logic;




signal rxlos0_out:std_logic;
signal rxlos1_out:std_logic;
signal rxlos2_out:std_logic;
signal rxlos3_out:std_logic;
signal rxlos4_out:std_logic;
signal rxlos5_out:std_logic;
signal rxlos6_out:std_logic;
signal rxlos7_out:std_logic;
signal rxlos8_out:std_logic;
signal rxlos9_out:std_logic;

signal moddet0_out:std_logic;
signal moddet1_out:std_logic;
signal moddet2_out:std_logic;
signal moddet3_out:std_logic;
signal moddet4_out:std_logic;
signal moddet5_out:std_logic;
signal moddet6_out:std_logic;
signal moddet7_out:std_logic;
signal moddet8_out:std_logic;
signal moddet9_out:std_logic;

signal gt0_counter: integer:=0;
signal gt1_counter: integer:=0;
signal gt2_counter: integer:=0;
signal gt3_counter: integer:=0;
signal gt4_counter: integer:=0;
signal gt5_counter: integer:=0;
signal gt6_counter: integer:=0;
signal gt7_counter: integer:=0;
signal gt8_counter: integer:=0;
signal gt9_counter: integer:=0;

signal resetCPLD_out:std_logic;

signal resetStatus_s: std_logic_vector(31 downto 0);

signal rxlosmodsetstatus_s :  std_logic_vector(31 downto 0);

constant timeout:integer:=50000000;

begin
--async reset
cplleset<=cpureset(15);

resetStatus<=resetStatus_s;
rxlosmodsetstatus<=rxlosmodsetstatus_s;

cpuReset_s<=cpuReset;
resetGTWizard<=resetGTWizard_s;
resetGTWizardInv<=not resetGTWizard_s;
resetXADC<=resetXADC_s;

rxlos0asyncToSync: asyncToSync port map(datain=>RX_LOS(0),dataout=>rxlos0_out,clk=>clk);

rxlos1asyncToSync: asyncToSync port map(datain=>RX_LOS(1),dataout=>rxlos1_out,clk=>clk);

rxlos2asyncToSync: asyncToSync port map(datain=>RX_LOS(2),dataout=>rxlos2_out,clk=>clk);

rxlos3asyncToSync: asyncToSync port map(datain=>RX_LOS(3),dataout=>rxlos3_out,clk=>clk);

rxlos4asyncToSync: asyncToSync port map(datain=>RX_LOS(4),dataout=>rxlos4_out,clk=>clk);

rxlos5asyncToSync: asyncToSync port map(datain=>RX_LOS(5),dataout=>rxlos5_out,clk=>clk);

rxlos6asyncToSync: asyncToSync port map(datain=>RX_LOS(6),dataout=>rxlos6_out,clk=>clk);

rxlos7asyncToSync: asyncToSync port map(datain=>RX_LOS(7),dataout=>rxlos7_out,clk=>clk);

rxlos8asyncToSync: asyncToSync port map(datain=>RX_LOS(8),dataout=>rxlos8_out,clk=>clk);

rxlos9asyncToSync: asyncToSync port map(datain=>RX_LOS(9),dataout=>rxlos9_out,clk=>clk);

moddet0asyncToSync: asyncToSync port map(datain=>MODDET(0),dataout=>moddet0_out,clk=>clk);

moddet1asyncToSync: asyncToSync port map(datain=>MODDET(1),dataout=>moddet1_out,clk=>clk);

moddet2asyncToSync: asyncToSync port map(datain=>MODDET(2),dataout=>moddet2_out,clk=>clk);

moddet3asyncToSync: asyncToSync port map(datain=>MODDET(3),dataout=>moddet3_out,clk=>clk);

moddet4asyncToSync: asyncToSync port map(datain=>MODDET(4),dataout=>moddet4_out,clk=>clk);

moddet5asyncToSync: asyncToSync port map(datain=>MODDET(5),dataout=>moddet5_out,clk=>clk);

moddet6asyncToSync: asyncToSync port map(datain=>MODDET(6),dataout=>moddet6_out,clk=>clk);

moddet7asyncToSync: asyncToSync port map(datain=>MODDET(7),dataout=>moddet7_out,clk=>clk);

moddet8asyncToSync: asyncToSync port map(datain=>MODDET(8),dataout=>moddet8_out,clk=>clk);

moddet9asyncToSync: asyncToSync port map(datain=>MODDET(9),dataout=>moddet9_out,clk=>clk);

CPLDResetasyncToSync: asyncToSync port map(datain=>cpldReset,dataout=>resetCPLD_out,clk=>clk);

resetPipeline0Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt0_clk,inputA=>resetPipeline0_in,outputB=>resetPipeline0_out);
resetPipeline0<=resetPipeline0_out;

resetPipeline1Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt1_clk,inputA=>resetPipeline1_in,outputB=>resetPipeline1_out);
resetPipeline1<=resetPipeline1_out;

resetPipeline2Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt2_clk,inputA=>resetPipeline2_in,outputB=>resetPipeline2_out);
resetPipeline2<=resetPipeline2_out;

resetPipeline3Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt3_clk,inputA=>resetPipeline3_in,outputB=>resetPipeline3_out);
resetPipeline3<=resetPipeline3_out;

resetPipeline4Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt4_clk,inputA=>resetPipeline4_in,outputB=>resetPipeline4_out);
resetPipeline4<=resetPipeline4_out;

resetPipeline5Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt5_clk,inputA=>resetPipeline5_in,outputB=>resetPipeline5_out);
resetPipeline5<=resetPipeline5_out;

resetPipeline6Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt6_clk,inputA=>resetPipeline6_in,outputB=>resetPipeline6_out);
resetPipeline6<=resetPipeline6_out;

resetPipeline7Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt7_clk,inputA=>resetPipeline7_in,outputB=>resetPipeline7_out);
resetPipeline7<=resetPipeline7_out;

resetPipeline8Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt8_clk,inputA=>resetPipeline8_in,outputB=>resetPipeline8_out);
resetPipeline8<=resetPipeline8_out;

resetPipeline9Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>gt9_clk,inputA=>resetPipeline9_in,outputB=>resetPipeline9_out);
resetPipeline9<=resetPipeline9_out;

resetDataBufferWriterCrossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetDataBufferWriter_in,outputB=>resetDataBufferWriter_out);
resetDataBufferWriter<=resetDataBufferWriter_out;

resetDataBufferReaderCrossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetDataBufferReader_in,outputB=>resetDataBufferReader_out);
resetDataBufferReader<=resetDataBufferReader_out;

resetInterrupt0Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt0_in,outputB=>resetInterrupt0_out);
resetInterrupt0<=resetInterrupt0_out;

resetInterrupt1Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt1_in,outputB=>resetInterrupt1_out);
resetInterrupt1<=resetInterrupt1_out;

resetInterrupt2Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt2_in,outputB=>resetInterrupt2_out);
resetInterrupt2<=resetInterrupt2_out;

resetInterrupt3Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt3_in,outputB=>resetInterrupt3_out);
resetInterrupt3<=resetInterrupt3_out;

resetInterrupt4Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt4_in,outputB=>resetInterrupt4_out);
resetInterrupt4<=resetInterrupt4_out;

resetInterrupt5Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt5_in,outputB=>resetInterrupt5_out);
resetInterrupt5<=resetInterrupt5_out;

resetInterrupt6Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt6_in,outputB=>resetInterrupt6_out);
resetInterrupt6<=resetInterrupt6_out;

resetInterrupt7Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt7_in,outputB=>resetInterrupt7_out);
resetInterrupt7<=resetInterrupt7_out;

resetInterrupt8Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt8_in,outputB=>resetInterrupt8_out);
resetInterrupt8<=resetInterrupt8_out;

resetInterrupt9Crossing: clockDomainCrossing port map(clkA=>clk,clkB=>axi_aclk,inputA=>resetInterrupt9_in,outputB=>resetInterrupt9_out);
resetInterrupt9<=resetInterrupt9_out;

rxlosmodsetstatus_s(0)<=rxlos3_out;
rxlosmodsetstatus_s(1)<=rxlos1_out;
rxlosmodsetstatus_s(2)<=rxlos2_out;
rxlosmodsetstatus_s(3)<=rxlos4_out;
rxlosmodsetstatus_s(4)<=rxlos5_out;
rxlosmodsetstatus_s(5)<=rxlos0_out;
rxlosmodsetstatus_s(6)<=rxlos6_out;
rxlosmodsetstatus_s(7)<=rxlos9_out;
rxlosmodsetstatus_s(8)<=rxlos7_out;
rxlosmodsetstatus_s(9)<=rxlos8_out;

rxlosmodsetstatus_s(10)<=moddet3_out;
rxlosmodsetstatus_s(11)<=moddet1_out;
rxlosmodsetstatus_s(12)<=moddet2_out;
rxlosmodsetstatus_s(13)<=moddet4_out;
rxlosmodsetstatus_s(14)<=moddet5_out;
rxlosmodsetstatus_s(15)<=moddet0_out;
rxlosmodsetstatus_s(16)<=moddet6_out;
rxlosmodsetstatus_s(17)<=moddet9_out;
rxlosmodsetstatus_s(18)<=moddet7_out;
rxlosmodsetstatus_s(19)<=moddet8_out;
rxlosmodsetstatus_s(31 downto 20)<=(others=>'0');

resetStatus_s(0)<=resetPipeline3_in;
resetStatus_s(1)<=resetPipeline1_in;
resetStatus_s(2)<=resetPipeline2_in;
resetStatus_s(3)<=resetPipeline4_in;
resetStatus_s(4)<=resetPipeline5_in;
resetStatus_s(5)<=resetPipeline0_in;
resetStatus_s(6)<=resetPipeline6_in;
resetStatus_s(7)<=resetPipeline9_in;
resetStatus_s(8)<=resetPipeline7_in;
resetStatus_s(9)<=resetPipeline8_in;
resetStatus_s(10)<=resetGTWizard_s;
resetStatus_s(11)<=resetDataBufferWriter_in;
resetStatus_s(12)<=resetDataBufferReader_in;
resetStatus_s(13)<='0';
resetStatus_s(14)<=resetXADC_s;
resetStatus_s(15)<=resetInterrupt0_in;
resetStatus_s(16)<=resetInterrupt1_in;
resetStatus_s(17)<=resetInterrupt2_in;
resetStatus_s(18)<=resetInterrupt3_in;
resetStatus_s(19)<=resetInterrupt4_in;
resetStatus_s(20)<=resetInterrupt5_in;
resetStatus_s(21)<=resetInterrupt6_in;
resetStatus_s(22)<=resetInterrupt7_in;
resetStatus_s(23)<=resetInterrupt8_in;
resetStatus_s(24)<=resetInterrupt9_in;
resetStatus_s(31 downto 25)<=(others=>'0');

mainProcess:process(clk)
begin
if rising_edge(clk) then
    if resetCPLD_out='1' then
        resetPipeline0_in<='1';
        resetPipeline1_in<='1';
        resetPipeline2_in<='1';
        resetPipeline3_in<='1';
        resetPipeline4_in<='1';
        resetPipeline5_in<='1';
        resetPipeline6_in<='1';
        resetPipeline7_in<='1';
        resetPipeline8_in<='1';
        resetPipeline9_in<='1';
        resetInterrupt0_in<='1';
        resetInterrupt1_in<='1';
        resetInterrupt2_in<='1';
        resetInterrupt3_in<='1';
        resetInterrupt4_in<='1';
        resetInterrupt5_in<='1';
        resetInterrupt6_in<='1';
        resetInterrupt7_in<='1';
        resetInterrupt8_in<='1';
        resetInterrupt9_in<='1';
        resetGTWizard_s<='1';
        resetXADC_s<='1';
        resetDataBufferWriter_in<='1';
        resetDataBufferReader_in<='1';
        gt0_counter<=timeout;
        gt1_counter<=timeout;
        gt2_counter<=timeout;
        gt3_counter<=timeout;
        gt4_counter<=timeout;
        gt5_counter<=timeout;
        gt6_counter<=timeout;
        gt7_counter<=timeout;
        gt8_counter<=timeout;
        gt9_counter<=timeout;
    else
        resetInterrupt0_in<=interruptResetIn(0);
        resetInterrupt1_in<=interruptResetIn(1);
        resetInterrupt2_in<=interruptResetIn(2);
        resetInterrupt3_in<=interruptResetIn(3);
        resetInterrupt4_in<=interruptResetIn(4);
        resetInterrupt5_in<=interruptResetIn(5);
        resetInterrupt6_in<=interruptResetIn(6);
        resetInterrupt7_in<=interruptResetIn(7);
        resetInterrupt8_in<=interruptResetIn(8);
        resetInterrupt9_in<=interruptResetIn(9);
        resetGTWizard_s<=cpuReset_s(10);
        resetDataBufferWriter_in<=cpuReset_s(11);
        resetDataBufferReader_in<=cpuReset_s(12);
        resetXADC_s<=cpuReset_s(14);
        
        if gt0_counter>=timeout then
            resetPipeline0_in<=cpuReset_s(5);
        else
            resetPipeline0_in<='1';
        end if;
        
        if gt1_counter>=timeout then
            resetPipeline1_in<=cpuReset_s(1);
        else
            resetPipeline1_in<='1';
        end if;
        
         if gt2_counter>=timeout then
            resetPipeline2_in<=cpuReset_s(2);
        else
            resetPipeline2_in<='1';
        end if;
        
        if gt3_counter>=timeout then
            resetPipeline3_in<=cpuReset_s(0);
        else
            resetPipeline3_in<='1';
        end if;
        
         if gt4_counter>=timeout then
            resetPipeline4_in<=cpuReset_s(3);
        else
            resetPipeline4_in<='1';
        end if;
        
        if gt5_counter>=timeout then
            resetPipeline5_in<=cpuReset_s(4);
        else
            resetPipeline5_in<='1';
        end if;
        
        if gt6_counter>=timeout then
            resetPipeline6_in<=cpuReset_s(6);
        else
            resetPipeline6_in<='1';
        end if;
        
        if gt7_counter>=timeout then
            resetPipeline7_in<=cpuReset_s(8);
        else
            resetPipeline7_in<='1';
        end if;
        
         if gt8_counter>=timeout then
            resetPipeline8_in<=cpuReset_s(9);
        else
            resetPipeline8_in<='1';
        end if;
        
        if gt9_counter>=timeout then
            resetPipeline9_in<=cpuReset_s(7);
        else
            resetPipeline9_in<='1';
        end if;
        
    end if;
    if rxlos0_out='1' then
        gt0_counter<=0;
    --1sec delay from stable optics
    elsif gt0_counter<timeout then
        gt0_counter<=gt0_counter+1;
    end if;
   if  rxlos1_out='1' then
        gt1_counter<=0;
    --1sec delay from stable optics
    elsif gt1_counter<timeout then
        gt1_counter<=gt1_counter+1;
    end if;
    
    if  rxlos2_out='1' then
        gt2_counter<=0;
    --1sec delay from stable optics
    elsif gt2_counter<timeout then
        gt2_counter<=gt2_counter+1;
    end if;
    
    if  rxlos3_out='1' then
        gt3_counter<=0;
    --1sec delay from stable optics
    elsif gt3_counter<timeout then
        gt3_counter<=gt3_counter+1;
    end if;
    
     if  rxlos4_out='1' then
        gt4_counter<=0;
    --1sec delay from stable optics
    elsif gt4_counter<timeout then
        gt4_counter<=gt4_counter+1;
    end if;
    
     if rxlos5_out='1' then
        gt5_counter<=0;
    --1sec delay from stable optics
    elsif gt5_counter<timeout then
        gt5_counter<=gt5_counter+1;
    end if;
    
     if  rxlos6_out='1' then
        gt6_counter<=0;
    --1sec delay from stable optics
    elsif gt6_counter<timeout then
        gt6_counter<=gt6_counter+1;
    end if;
    
     if  rxlos7_out='1' then
        gt7_counter<=0;
    --1sec delay from stable optics
    elsif gt7_counter<timeout then
        gt7_counter<=gt7_counter+1;
    end if;
    
     if  rxlos8_out='1' then
        gt8_counter<=0;
    --1sec delay from stable optics
    elsif gt8_counter<timeout then
        gt8_counter<=gt8_counter+1;
    end if;
    
      if  rxlos9_out='1' then
        gt9_counter<=0;
    --1sec delay from stable optics
    elsif gt9_counter<timeout then
        gt9_counter<=gt9_counter+1;
    end if;

end if;

end process;

end Behavioral;
