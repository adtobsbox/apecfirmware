--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Thu Jul  7 15:52:03 2022
--Host        : PCBE16571 running 64-bit Ubuntu 22.04 LTS
--Command     : generate_target design_3_wrapper.bd
--Design      : design_3_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_wrapper is
end design_3_wrapper;

architecture STRUCTURE of design_3_wrapper is
  component design_3 is
  end component design_3;
begin
design_3_i: component design_3
 ;
end STRUCTURE;
