----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/24/2019 05:59:55 PM
-- Design Name: 
-- Module Name: sim_xdma - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sim_xdma is
    Port ( tdata : in STD_LOGIC_VECTOR (255 downto 0);
           tkeep : in STD_LOGIC_VECTOR (31 downto 0);
           tlast : in STD_LOGIC;
           tvalid : in STD_LOGIC;
           tready : out STD_LOGIC;
           clk : in std_logic;
           swapIn : in std_logic);
end sim_xdma;

architecture Behavioral of sim_xdma is
signal counter: integer:=0;
signal tvalid_old:std_logic:='0';
signal treadyCounter: integer:=0;
signal pauseCounter : integer:=0;
type treadyStateType is (idle,receive,pause,pauseReceive);
signal treadyState: treadyStateType:=idle;
begin
main: process(clk)
begin
if rising_edge(clk) then
    tvalid_old<=tvalid;
    if tvalid_old='0' and tvalid='1' then
        counter<=1;
    elsif tvalid_old='1' and tvalid='1' then
        counter<=counter+1;
    elsif tvalid_old='1' and tvalid='0' then
        counter<=0;
    end if;
    if counter=252 then
        tready<='0';
    else
        tready<='1';    
    end if;
    end if;

end process;

end Behavioral;