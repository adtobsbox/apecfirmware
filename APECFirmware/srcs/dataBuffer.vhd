----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    04/30/2019 11:54:56 AM
-- Design Name:
-- Module Name:    dataBuffer - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dataBuffer is
  Port (wr_clk0:   in std_logic;
wr_en0:            in std_logic;
din0:              in std_logic_vector(271 downto 0);
wr_clk1:           in std_logic;
wr_en1:            in std_logic;
din1:              in std_logic_vector(271 downto 0);
wr_clk2:           in std_logic;
wr_en2:            in std_logic;
din2:              in std_logic_vector(271 downto 0);
wr_clk3:           in std_logic;
wr_en3:            in std_logic;
din3:              in std_logic_vector(271 downto 0);
wr_clk4:           in std_logic;
wr_en4:            in std_logic;
din4:              in std_logic_vector(271 downto 0);
wr_clk5:           in std_logic;
wr_en5:            in std_logic;
din5:              in std_logic_vector(271 downto 0);
wr_clk6:           in std_logic;
wr_en6:            in std_logic;
din6:              in std_logic_vector(271 downto 0);
wr_clk7:           in std_logic;
wr_en7:            in std_logic;
din7:              in std_logic_vector(271 downto 0);
wr_clk8:           in std_logic;
wr_en8:            in std_logic;
din8:              in std_logic_vector(271 downto 0);
wr_clk9:           in std_logic;
wr_en9:            in std_logic;
din9:              in std_logic_vector(271 downto 0);
s_axis_c2h_tdata_0 : out std_logic_vector(255 downto 0);
s_axis_c2h_tkeep_0 : out std_logic_vector(31 downto 0);
s_axis_c2h_tlast_0 : out std_logic;
s_axis_c2h_tready_0 : in std_logic;
s_axis_c2h_tvalid_0 : out std_logic;
axi_aclk           : in std_logic;
axi_aresetn         :in std_logic;
  transferSizePerChannel_in : in std_logic_vector(31 downto 0);
channels_in :      in std_logic_vector(31 downto 0);
transferTotal_in : in std_logic_vector(31 downto 0);
timeout_min_in :   in std_logic_vector(31 downto 0);
timeout_max_in :   in std_logic_vector(31 downto 0);
resetReader:       in std_logic;
resetWriter:       in std_logic;
status :           out std_logic_vector(31 downto 0);
swap_out :         out std_logic;
addrb :            out std_logic_vector(31 downto 0);
clkb :             out std_logic;
dinb :             out std_logic_vector(63 downto 0);
doutb :            in std_logic_vector(63 downto 0);
enb :              out std_logic;
rstb :             out std_logic;
web :              out std_logic_vector(7 downto 0);
c2h_sts_n :        in std_logic_vector(7 downto 0);
c2h_dsc_byp_ctl :  out std_logic_vector(15 downto 0);
c2h_dsc_byp_dst_addr : out std_logic_vector(63 downto 0);
c2h_dsc_byp_len :  out std_logic_vector(27 downto 0);
c2h_dsc_byp_load : out std_logic;
c2h_dsc_byp_ready : in std_logic;
c2h_dsc_byp_src_addr : out std_logic_vector(63 downto 0);
polling :          in std_logic;
readingStatus : out std_logic_vector(4 downto 0);
fifo0_reset_in : in std_logic;
fifo1_reset_in : in std_logic;
fifo2_reset_in : in std_logic;
fifo3_reset_in : in std_logic;
fifo4_reset_in : in std_logic;
fifo5_reset_in : in std_logic;
fifo6_reset_in : in std_logic;
fifo7_reset_in : in std_logic;
fifo8_reset_in : in std_logic;
fifo9_reset_in : in std_logic
         );
end dataBuffer;



architecture Behavioral of dataBuffer is
 ATTRIBUTE X_INTERFACE_INFO : STRING;
ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
ATTRIBUTE X_INTERFACE_MODE : STRING;
ATTRIBUTE X_INTERFACE_INFO OF doutb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT DOUT";
ATTRIBUTE X_INTERFACE_INFO OF dinb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT DIN";
ATTRIBUTE X_INTERFACE_INFO OF addrb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT ADDR";
ATTRIBUTE X_INTERFACE_INFO OF web: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT WE";
ATTRIBUTE X_INTERFACE_INFO OF enb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT EN";
ATTRIBUTE X_INTERFACE_INFO OF rstb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT RST";
ATTRIBUTE X_INTERFACE_PARAMETER OF clkb: SIGNAL IS "XIL_INTERFACENAME BRAM_PORTOUT, MEM_SIZE 8192, MEM_WIDTH 64, MEM_ECC NONE, MASTER_TYPE BRAM_CTRL, READ_WRITE_MODE READ_WRITE";
ATTRIBUTE X_INTERFACE_INFO OF clkb: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTOUT CLK";


ATTRIBUTE X_INTERFACE_INFO of s_axis_c2h_tdata_0: SIGNAL is "xilinx.com:interface:axis:1.0 axi TDATA";
ATTRIBUTE X_INTERFACE_INFO of s_axis_c2h_tkeep_0: SIGNAL is "xilinx.com:interface:axis:1.0 axi TKEEP";
ATTRIBUTE X_INTERFACE_INFO of s_axis_c2h_tlast_0: SIGNAL is "xilinx.com:interface:axis:1.0 axi TLAST";
ATTRIBUTE X_INTERFACE_INFO of s_axis_c2h_tvalid_0: SIGNAL is "xilinx.com:interface:axis:1.0 axi TVALID";
ATTRIBUTE X_INTERFACE_INFO of s_axis_c2h_tready_0: SIGNAL is "xilinx.com:interface:axis:1.0 axi TREADY";


attribute X_INTERFACE_MODE of c2h_dsc_byp_ctl: signal is "MASTER";
attribute X_INTERFACE_INFO of c2h_dsc_byp_ctl: signal is "xilinx.com:display_xdma:dsc_bypass_rtl:1.0 my_dsc_byp_c2h dsc_byp_ctl";
attribute X_INTERFACE_INFO of c2h_dsc_byp_len: signal is "xilinx.com:display_xdma:dsc_bypass_rtl:1.0 my_dsc_byp_c2h dsc_byp_len";
attribute X_INTERFACE_INFO of c2h_dsc_byp_dst_addr: signal is "xilinx.com:display_xdma:dsc_bypass_rtl:1.0 my_dsc_byp_c2h dsc_byp_dst_addr";
attribute X_INTERFACE_INFO of c2h_dsc_byp_load: signal is "xilinx.com:display_xdma:dsc_bypass_rtl:1.0 my_dsc_byp_c2h dsc_byp_load";
attribute X_INTERFACE_INFO of c2h_dsc_byp_ready: signal is "xilinx.com:display_xdma:dsc_bypass_rtl:1.0 my_dsc_byp_c2h dsc_byp_ready";
attribute X_INTERFACE_INFO of c2h_dsc_byp_src_addr: signal is "xilinx.com:display_xdma:dsc_bypass_rtl:1.0 my_dsc_byp_c2h dsc_byp_src_addr";

component buffer_input_fifo
  PORT (
  rst : IN STD_LOGIC;
    wr_clk :       IN STD_LOGIC;
    rd_clk :       IN STD_LOGIC;
    din :          IN STD_LOGIC_VECTOR(271 DOWNTO 0);
    wr_en :        IN STD_LOGIC;
    rd_en :        IN STD_LOGIC;
    dout :         OUT STD_LOGIC_VECTOR(271 DOWNTO 0);
    full :         OUT STD_LOGIC;
    empty :        OUT STD_LOGIC;
    prog_full :    out std_logic;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END component ;



--descriptor bypass signals
signal dinb_s :    std_logic_vector(63 downto 0);
signal doutb_s :   std_logic_vector(63 downto 0);
signal web_s :     std_logic_vector(7 downto 0);
signal c2h_sts_n_s : std_logic_vector(7 downto 0);
signal c2h_dsc_byp_ctl_s : std_logic_vector(15 downto 0);
signal c2h_dsc_byp_dst_addr_s : std_logic_vector(63 downto 0);
signal c2h_dsc_byp_len_s : std_logic_vector(27 downto 0);
signal c2h_dsc_byp_load_s : std_logic;
signal c2h_dsc_byp_ready_s : std_logic;
signal c2h_dsc_byp_src_addr_s : std_logic_vector(63 downto 0);


--address to the current descriptor
signal address:    unsigned(31 downto 0):=(others=>'0');
signal currentOffset: unsigned(31 downto 0):=(others=>'0');
constant maxAddress : unsigned(31 downto 0):=x"00000ff0";

--c2h interface
signal s_axis_c2h_tdata_0_out : std_logic_vector(255 downto 0);
signal s_axis_c2h_tkeep_0_out : std_logic_vector(31 downto 0);
signal s_axis_c2h_tlast_0_out : std_logic;
signal s_axis_c2h_tready_0_in : std_logic;
signal s_axis_c2h_tvalid_0_out : std_logic;


signal transferSizePerChannel_in_bytes : std_logic_vector(31 downto 0);

signal transferSizePerChannel : integer;
signal transferSizePerChannelBytes : integer;
signal channels :  integer;
signal transferTotal : integer;
signal status_s :  std_logic_vector(31 downto 0);




--signal fifo_wr_clk0,fifo_wr_clk1,fifo_wr_clk2,fifo_wr_clk3,fifo_wr_clk4,fifo_wr_clk5,fifo_wr_clk6,fifo_wr_clk7,fifo_wr_clk8,fifo_wr_clk9 : STD_LOGIC;
--signal fifo_rd_clk0,fifo_rd_clk1,fifo_rd_clk2,fifo_rd_clk3,fifo_rd_clk4,fifo_rd_clk5,fifo_rd_clk6,fifo_rd_clk7,fifo_rd_clk8,fifo_rd_clk9 : STD_LOGIC;
signal fifo_din0,fifo_din1,fifo_din2,fifo_din3,fifo_din4,fifo_din5,fifo_din6,fifo_din7,fifo_din8,fifo_din9 : STD_LOGIC_VECTOR(271 DOWNTO 0);
signal fifo_wr_en0,fifo_wr_en1,fifo_wr_en2,fifo_wr_en3,fifo_wr_en4,fifo_wr_en5,fifo_wr_en6,fifo_wr_en7,fifo_wr_en8,fifo_wr_en9 : STD_LOGIC;
signal fifo_rd_en0,fifo_rd_en1,fifo_rd_en2,fifo_rd_en3,fifo_rd_en4,fifo_rd_en5,fifo_rd_en6,fifo_rd_en7,fifo_rd_en8,fifo_rd_en9 : STD_LOGIC;
signal fifo_dout0,fifo_dout1,fifo_dout2,fifo_dout3,fifo_dout4,fifo_dout5,fifo_dout6,fifo_dout7,fifo_dout8,fifo_dout9 : STD_LOGIC_VECTOR(271 DOWNTO 0);
signal fifo_full0,fifo_full1,fifo_full2,fifo_full3,fifo_full4,fifo_full5,fifo_full6,fifo_full7,fifo_full8,fifo_full9 : STD_LOGIC;
signal fifo_empty0,fifo_empty1,fifo_empty2,fifo_empty3,fifo_empty4,fifo_empty5,fifo_empty6,fifo_empty7,fifo_empty8,fifo_empty9 : STD_LOGIC;
signal fifo_prog_full0,fifo_prog_full1,fifo_prog_full2,fifo_prog_full3,fifo_prog_full4,fifo_prog_full5,fifo_prog_full6,fifo_prog_full7,fifo_prog_full8,fifo_prog_full9 : std_logic;

signal fifo_reset0,fifo_reset1,fifo_reset2,fifo_reset3,fifo_reset4,fifo_reset5,fifo_reset6,fifo_reset7,fifo_reset8,fifo_reset9 : STD_LOGIC;

signal fifo_wr_rst_busy0,fifo_wr_rst_busy1,fifo_wr_rst_busy2,fifo_wr_rst_busy3,fifo_wr_rst_busy4,fifo_wr_rst_busy5,fifo_wr_rst_busy6,fifo_wr_rst_busy7,fifo_wr_rst_busy8,fifo_wr_rst_busy9 : STD_LOGIC;
signal fifo_rd_rst_busy0,fifo_rd_rst_busy1,fifo_rd_rst_busy2,fifo_rd_rst_busy3,fifo_rd_rst_busy4,fifo_rd_rst_busy5,fifo_rd_rst_busy6,fifo_rd_rst_busy7,fifo_rd_rst_busy8,fifo_rd_rst_busy9 : STD_LOGIC;

--combine signals
signal rd_en :     std_logic_vector(0 to 9);
type dout_array_type is array (0 to 9) of std_logic_vector(271 downto 0);
signal dout_array:dout_array_type;
signal full :      std_logic_vector(0 to 9);
signal empty :     std_logic_vector(0 to 9);
signal prog_full : std_logic_vector(0 to 9);

--stream status signals
signal running :   std_logic_vector(0 to 9);
signal skip :   std_logic_vector(0 to 9);
type counter_array_type is array (0 to 9) of integer;
signal running_counter:counter_array_type;
--If a stream has not produced new data within this limit, it is considered to be idling
constant running_limit : integer:=2000;




signal timeout_counter:integer:=0;

signal timeout_min : integer:=0;
signal timeout_max : integer:=0;

--number of 32 bytes chunks we have written
subtype address_type is integer range 0 to 16384 ;
signal writer_counter : address_type:=0;

--which channel we are currently writing for
signal channel_counter : integer:=0;


signal readingStatus_s : std_logic_vector(4 downto 0);

--used to calculate the descriptor address
signal descriptor_address_result : integer:=0;
signal descriptor_address_result_bytes : integer:=0;


type masterStateType is (idle,prep_desc0,prep_desc1,prep_desc2,prep_desc3,start0,findStartOfFrame0,findStartOfFrame1,findStartOfFrame2,write0,write1,write2,write3,write4,finalizeFrame0,finalizeFrame1,finalizeFrame2,finishDesc0,finishDesc1,finishDesc2,finishDesc3);
signal masterState: masterStateType:=idle;



begin

fifo_reset0<=fifo0_reset_in;
fifo_reset1<=fifo1_reset_in;
fifo_reset2<=fifo2_reset_in;
fifo_reset3<=fifo3_reset_in;
fifo_reset4<=fifo4_reset_in;
fifo_reset5<=fifo5_reset_in;
fifo_reset6<=fifo6_reset_in;
fifo_reset7<=fifo7_reset_in;
fifo_reset8<=fifo8_reset_in;
fifo_reset9<=fifo9_reset_in;

readingStatus<=readingStatus_s;

enb<='1';
clkb<=axi_aclk;
rstb<='0';
addrb<=std_logic_vector(address);

dinb<=dinb_s;
doutb_s<=doutb;

web<=web_s;
c2h_sts_n_s<=c2h_sts_n;
c2h_dsc_byp_ctl<=c2h_dsc_byp_ctl_s;
c2h_dsc_byp_dst_addr<=c2h_dsc_byp_dst_addr_s;
c2h_dsc_byp_len<=c2h_dsc_byp_len_s;
c2h_dsc_byp_load<=c2h_dsc_byp_load_s;
c2h_dsc_byp_ready_s<=c2h_dsc_byp_ready;
c2h_dsc_byp_src_addr<=c2h_dsc_byp_src_addr_s;


swap_out<='0';

status<=status_s;
status_s(0)<=running(0);
status_s(1)<=running(1);
status_s(2)<=running(2);
status_s(3)<=running(3);
status_s(4)<=running(4);
status_s(5)<=running(5);
status_s(6)<=running(6);
status_s(7)<=running(7);
status_s(8)<=running(8);
status_s(9)<=running(9);
status_s(16 downto 14)<=readingStatus_s(2 downto 0);
status_s(24 downto 17)<=c2h_sts_n_s;
status_s(31 downto 25)<=(others=>'0');

transferSizePerChannel <=to_integer(unsigned(transferSizePerChannel_in));
transferSizePerChannel_in_bytes<=transferSizePerChannel_in(26 downto 0)&"00000";
transferSizePerChannelBytes <=to_integer(unsigned(transferSizePerChannel_in_bytes));

channels  <=to_integer(unsigned(channels_in));
transferTotal  <=to_integer(unsigned(transferTotal_in));
timeout_min <=to_integer(unsigned(timeout_min_in));
timeout_max <=to_integer(unsigned(timeout_max_in));



fifo_din0<=din0;
fifo_wr_en0<=wr_en0 and not fifo_full0;
fifo_rd_en0<=rd_en(0);
dout_array(0)<=fifo_dout0;
full(0)<=fifo_full0;
empty(0)<=fifo_empty0;
prog_full(0)<=fifo_prog_full0;


fifo_din1<=din1;
fifo_wr_en1<=wr_en1  and not fifo_full1;
fifo_rd_en1<=rd_en(1);
dout_array(1)<=fifo_dout1;
full(1)<=fifo_full1;
empty(1)<=fifo_empty1;
prog_full(1)<=fifo_prog_full1;

fifo_din2<=din2;
fifo_wr_en2<=wr_en2  and not fifo_full2;
fifo_rd_en2<=rd_en(2);
dout_array(2)<=fifo_dout2;
full(2)<=fifo_full2;
empty(2)<=fifo_empty2;
prog_full(2)<=fifo_prog_full2;

fifo_din3<=din3;
fifo_wr_en3<=wr_en3  and not fifo_full3;
fifo_rd_en3<=rd_en(3);
dout_array(3)<=fifo_dout3;
full(3)<=fifo_full3;
empty(3)<=fifo_empty3;
prog_full(3)<=fifo_prog_full3;

fifo_din4<=din4;
fifo_wr_en4<=wr_en4  and not fifo_full4;
fifo_rd_en4<=rd_en(4);
dout_array(4)<=fifo_dout4;
full(4)<=fifo_full4;
empty(4)<=fifo_empty4;
prog_full(4)<=fifo_prog_full4;


fifo_din5<=din5;
fifo_wr_en5<=wr_en5  and not fifo_full5;
fifo_rd_en5<=rd_en(5);
dout_array(5)<=fifo_dout5;
full(5)<=fifo_full5;
empty(5)<=fifo_empty5;
prog_full(5)<=fifo_prog_full5;

fifo_din6<=din6;
fifo_wr_en6<=wr_en6  and not fifo_full6;
fifo_rd_en6<=rd_en(6);
dout_array(6)<=fifo_dout6;
full(6)<=fifo_full6;
empty(6)<=fifo_empty6;
prog_full(6)<=fifo_prog_full6;

fifo_din7<=din7;
fifo_wr_en7<=wr_en7  and not fifo_full7;
fifo_rd_en7<=rd_en(7);
dout_array(7)<=fifo_dout7;
full(7)<=fifo_full7;
empty(7)<=fifo_empty7;
prog_full(7)<=fifo_prog_full7;

fifo_din8<=din8;
fifo_wr_en8<=wr_en8  and not fifo_full8;
fifo_rd_en8<=rd_en(8);
dout_array(8)<=fifo_dout8;
full(8)<=fifo_full8;
empty(8)<=fifo_empty8;
prog_full(8)<=fifo_prog_full8;

fifo_din9<=din9;
fifo_wr_en9<=wr_en9  and not fifo_full9;
fifo_rd_en9<=rd_en(9);
dout_array(9)<=fifo_dout9;
full(9)<=fifo_full9;
empty(9)<=fifo_empty9;
prog_full(9)<=fifo_prog_full9;

fifo0:             buffer_input_fifo PORT MAP(rst=>fifo_reset0,wr_clk =>wr_clk0,rd_clk =>axi_aclk,din =>fifo_din0,wr_en =>fifo_wr_en0,rd_en=>fifo_rd_en0,dout =>fifo_dout0,full =>fifo_full0,empty=>fifo_empty0,prog_full=>fifo_prog_full0,wr_rst_busy=>fifo_wr_rst_busy0,rd_rst_busy=>fifo_rd_rst_busy0);
fifo1:             buffer_input_fifo PORT MAP(rst=>fifo_reset1,wr_clk =>wr_clk1,rd_clk =>axi_aclk,din =>fifo_din1,wr_en =>fifo_wr_en1,rd_en=>fifo_rd_en1,dout =>fifo_dout1,full =>fifo_full1,empty=>fifo_empty1,prog_full=>fifo_prog_full1,wr_rst_busy=>fifo_wr_rst_busy1,rd_rst_busy=>fifo_rd_rst_busy1);
fifo2:             buffer_input_fifo PORT MAP(rst=>fifo_reset2,wr_clk =>wr_clk2,rd_clk =>axi_aclk,din =>fifo_din2,wr_en =>fifo_wr_en2,rd_en=>fifo_rd_en2,dout =>fifo_dout2,full =>fifo_full2,empty=>fifo_empty2,prog_full=>fifo_prog_full2,wr_rst_busy=>fifo_wr_rst_busy2,rd_rst_busy=>fifo_rd_rst_busy2);
fifo3:             buffer_input_fifo PORT MAP(rst=>fifo_reset3,wr_clk =>wr_clk3,rd_clk =>axi_aclk,din =>fifo_din3,wr_en =>fifo_wr_en3,rd_en=>fifo_rd_en3,dout =>fifo_dout3,full =>fifo_full3,empty=>fifo_empty3,prog_full=>fifo_prog_full3,wr_rst_busy=>fifo_wr_rst_busy3,rd_rst_busy=>fifo_rd_rst_busy3);
fifo4:             buffer_input_fifo PORT MAP(rst=>fifo_reset4,wr_clk =>wr_clk4,rd_clk =>axi_aclk,din =>fifo_din4,wr_en =>fifo_wr_en4,rd_en=>fifo_rd_en4,dout =>fifo_dout4,full =>fifo_full4,empty=>fifo_empty4,prog_full=>fifo_prog_full4,wr_rst_busy=>fifo_wr_rst_busy4,rd_rst_busy=>fifo_rd_rst_busy4);
fifo5:             buffer_input_fifo PORT MAP(rst=>fifo_reset5,wr_clk =>wr_clk5,rd_clk =>axi_aclk,din =>fifo_din5,wr_en =>fifo_wr_en5,rd_en=>fifo_rd_en5,dout =>fifo_dout5,full =>fifo_full5,empty=>fifo_empty5,prog_full=>fifo_prog_full5,wr_rst_busy=>fifo_wr_rst_busy5,rd_rst_busy=>fifo_rd_rst_busy5);
fifo6:             buffer_input_fifo PORT MAP(rst=>fifo_reset6,wr_clk =>wr_clk6,rd_clk =>axi_aclk,din =>fifo_din6,wr_en =>fifo_wr_en6,rd_en=>fifo_rd_en6,dout =>fifo_dout6,full =>fifo_full6,empty=>fifo_empty6,prog_full=>fifo_prog_full6,wr_rst_busy=>fifo_wr_rst_busy6,rd_rst_busy=>fifo_rd_rst_busy6);
fifo7:             buffer_input_fifo PORT MAP(rst=>fifo_reset7,wr_clk =>wr_clk7,rd_clk =>axi_aclk,din =>fifo_din7,wr_en =>fifo_wr_en7,rd_en=>fifo_rd_en7,dout =>fifo_dout7,full =>fifo_full7,empty=>fifo_empty7,prog_full=>fifo_prog_full7,wr_rst_busy=>fifo_wr_rst_busy7,rd_rst_busy=>fifo_rd_rst_busy7);
fifo8:             buffer_input_fifo PORT MAP(rst=>fifo_reset8,wr_clk =>wr_clk8,rd_clk =>axi_aclk,din =>fifo_din8,wr_en =>fifo_wr_en8,rd_en=>fifo_rd_en8,dout =>fifo_dout8,full =>fifo_full8,empty=>fifo_empty8,prog_full=>fifo_prog_full8,wr_rst_busy=>fifo_wr_rst_busy8,rd_rst_busy=>fifo_rd_rst_busy8);
fifo9:             buffer_input_fifo PORT MAP(rst=>fifo_reset9,wr_clk =>wr_clk9,rd_clk =>axi_aclk,din =>fifo_din9,wr_en =>fifo_wr_en9,rd_en=>fifo_rd_en9,dout =>fifo_dout9,full =>fifo_full9,empty=>fifo_empty9,prog_full=>fifo_prog_full9,wr_rst_busy=>fifo_wr_rst_busy9,rd_rst_busy=>fifo_rd_rst_busy9);



s_axis_c2h_tdata_0<=s_axis_c2h_tdata_0_out;
s_axis_c2h_tkeep_0<=s_axis_c2h_tkeep_0_out ;
s_axis_c2h_tlast_0 <=s_axis_c2h_tlast_0_out;
s_axis_c2h_tready_0_in <=s_axis_c2h_tready_0;
s_axis_c2h_tvalid_0 <=s_axis_c2h_tvalid_0_out;




masterProcess:     process(axi_aclk,axi_aresetn)
begin
if rising_edge(axi_aclk) then
    if axi_aresetn='1' and resetReader='0' then
       case (masterState) is
            when idle=>
            --wait for all acive channels to have enough data to send
                timeout_counter<=timeout_counter+1;
                if (running(0)='0' or (running(0)='1' and prog_full(0)='1')) and (running(1)='0' or (running(1)='1' and prog_full(1)='1')) and (running(2)='0' or (running(2)='1' and prog_full(2)='1')) and (running(3)='0' or (running(3)='1' and prog_full(3)='1'))  and (running(4)='0' or (running(4)='1' and prog_full(4)='1')) and (running(5)='0' or (running(5)='1' and prog_full(5)='1')) and (running(6)='0' or (running(6)='1' and prog_full(6)='1')) and (running(7)='0' or (running(7)='1' and prog_full(7)='1')) and (running(8)='0' or (running(8)='1' and prog_full(8)='1')) and (running(9)='0' or (running(9)='1' and prog_full(9)='1')) then
                    if timeout_counter>=timeout_min then
                        masterState<=prep_desc0;
                        status_s(10)<='1';
                    end if;
                end if;
                --or timeout
                if timeout_counter>=timeout_max then
                    masterState<=prep_desc0;
                    status_s(10)<='1';
                end if;
                skip(0)<=not running(0) or not prog_full(0);
                skip(1)<=not running(1) or not prog_full(1);
                skip(2)<=not running(2) or not prog_full(2);
                skip(3)<=not running(3) or not prog_full(3);
                skip(4)<=not running(4) or not prog_full(4);
                skip(5)<=not running(5) or not prog_full(5);
                skip(6)<=not running(6) or not prog_full(6);
                skip(7)<=not running(7) or not prog_full(7);
                skip(8)<=not running(8) or not prog_full(8);
                skip(9)<=not running(9) or not prog_full(9);
                channel_counter<=0;
                readingStatus_s<="00000";
            when prep_desc0 =>
                timeout_counter<=0;
                --check that the engine is running
                if c2h_sts_n_s(6)='1' then
                    masterState<=prep_desc1;
                end if;
                --just to be sure
                 s_axis_c2h_tdata_0_out <=(others=>'0');
                 s_axis_c2h_tkeep_0_out <=(others=>'0');
                 s_axis_c2h_tvalid_0_out <='0';
                 s_axis_c2h_tlast_0_out<='0';
                 descriptor_address_result<=channel_counter*transferSizePerChannel;
                 descriptor_address_result_bytes<=channel_counter*transferSizePerChannelBytes;
                 writer_counter<=0;
                 address<=currentOffset;
                 readingStatus_s<="00001";
                --prepare the descriptor
            when prep_desc1=>
                --wait for the descriptor bypass to be ready to accept an address
                if c2h_dsc_byp_ready_s='1' then
                    masterState<=prep_desc2;
                end if;
                readingStatus_s<="00010";
                --can't have a zero descriptor
             when prep_desc2 =>
                if doutb_s/= x"0000000000000000" then
                    masterState<=prep_desc3;
                end if;
                readingStatus_s<="00011";
                --load the descriptor
            when prep_desc3=>
                --desc address + channel*sizePerChannel (must be 4k aligned) and is in terms of bytes and not 32k
                  c2h_dsc_byp_dst_addr_s<=std_logic_vector(unsigned(doutb_s)+descriptor_address_result_bytes);
                  --only interrupt on last stream
                  if channel_counter=(channels-1) then
                    c2h_dsc_byp_ctl_s<=(1=>'1',others=>'0');-- interrupt on descriptor_complete
                  else
                    c2h_dsc_byp_ctl_s<=(others=>'0');-- interrupt on descriptor_complete
                  end if;
                  --transferSizePerChannel is in number of packages of 32 bytes and we need total bytes
                  c2h_dsc_byp_len_s<=transferSizePerChannel_in_bytes(27 downto 0);
                  c2h_dsc_byp_load_s<='1';
                  c2h_dsc_byp_src_addr_s<=(others=>'0');-- no writeback
                  masterState<=start0;
              readingStatus_s<="00100";
          when start0=>
            c2h_dsc_byp_load_s<='0';
            if s_axis_c2h_tready_0_in='1' then
                if skip(channel_counter)='0' then
                    masterState<=findStartOfFrame0;
                else
                    masterState<=finalizeFrame0;
                end if;
            end if;
             readingStatus_s<="00101";
         when findStartOfFrame0=>
             --start of a frame
                 if dout_array(channel_counter)(1)='1' then
                    --burst test
                   -- rd_en(channel_counter)<='1';
                    masterState<=write0;
                 else
                    rd_en(channel_counter)<='1';
                    masterState<=findStartOfFrame1;
                 end if;
                 readingStatus_s<="00110";
         --wait two clock cycles
         when findStartOfFrame1=>
            rd_en(channel_counter)<='0';
            masterState<=findStartOfFrame2;
            readingStatus_s<="00111";
         --wait two clock cycles
         when findStartOfFrame2=>
            masterState<=findStartOfFrame0;
            readingStatus_s<="01000";
         when write0=>
            if s_axis_c2h_tready_0_in='1' then
               --s_axis_c2h_tdata_0_out<=dout_array(channel_counter)(271 downto 16);
               for I in 0 to 31 loop
                        s_axis_c2h_tdata_0_out(255-I*8 downto 248-I*8)<=dout_array(channel_counter)(23+I*8  downto I*8+16 );
                    end loop;

               s_axis_c2h_tkeep_0_out <=(others=>'1');
               s_axis_c2h_tvalid_0_out <='1';
               writer_counter<=writer_counter+1;
               --end of frame
               if dout_array(channel_counter)(0)='1' and writer_counter<(transferSizePerChannel-1) then
                    masterState<=finalizeFrame0;
                   -- rd_en(channel_counter)<='0';
               elsif writer_counter>=(transferSizePerChannel-1) then
                    masterState<=finishDesc0;
                    --rd_en(channel_counter)<='0';
                    s_axis_c2h_tlast_0_out<='1';
                else
                 --burst test
                   masterState<=write1;
                end if;
            else
                s_axis_c2h_tdata_0_out <=(others=>'0');
                s_axis_c2h_tkeep_0_out <=(others=>'0');
                s_axis_c2h_tvalid_0_out <='0';
                s_axis_c2h_tlast_0_out<='0';

            end if;
            readingStatus_s<="01001";
         when write1=>
             s_axis_c2h_tdata_0_out <=(others=>'0');
             s_axis_c2h_tkeep_0_out <=(others=>'0');
             s_axis_c2h_tvalid_0_out <='0';
             s_axis_c2h_tlast_0_out<='0';
            rd_en(channel_counter)<='1';
            masterState<=write2;
         when write2=>
            rd_en(channel_counter)<='0';
            masterState<=write3;
            readingStatus_s<="01010";
         when write3=>
            masterState<=write4;
            readingStatus_s<="01011";
        when write4=>
            --we fucked up somehow and we reached start of a new frame
            if dout_array(channel_counter)(1)='1' then
                masterState<=finalizeFrame0;
            else
                masterState<=write0;
            end if;
            readingStatus_s<="01100";
         when finalizeFrame0=>
             s_axis_c2h_tdata_0_out <=(others=>'0');
             s_axis_c2h_tkeep_0_out <=(others=>'0');
             s_axis_c2h_tvalid_0_out <='0';
             s_axis_c2h_tlast_0_out<='0';
             masterState<=finalizeFrame1;
            readingStatus_s<="01101";
        when finalizeFrame1=>
            masterState<=finalizeFrame2;
            readingStatus_s<="01110";
         when finalizeFrame2=>
            if s_axis_c2h_tready_0_in='1' then
               s_axis_c2h_tdata_0_out<=(others=>'0');
               s_axis_c2h_tkeep_0_out <=(others=>'1');
               s_axis_c2h_tvalid_0_out <='1';
               if writer_counter>=(transferSizePerChannel-1) then
                    masterState<=finishDesc0;
                    s_axis_c2h_tlast_0_out<='1';
                else
                    s_axis_c2h_tlast_0_out<='0';
                    masterState<=finalizeFrame0;
                end if;

               writer_counter<=writer_counter+1;
            else
                s_axis_c2h_tdata_0_out <=(others=>'0');
                s_axis_c2h_tkeep_0_out <=(others=>'0');
                s_axis_c2h_tvalid_0_out <='0';
                s_axis_c2h_tlast_0_out<='0';

            end if;
            readingStatus_s<="01111";
         when finishDesc0=>
                 if s_axis_c2h_tready_0_in='0' and c2h_sts_n_s(4)='1' then
                    masterState<=finishDesc1;
                 end if;
                 readingStatus_s<="10000";
        when finishDesc1 =>
                s_axis_c2h_tdata_0_out <=(others=>'0');
                s_axis_c2h_tkeep_0_out <=(others=>'0');
                s_axis_c2h_tvalid_0_out <='0';
                s_axis_c2h_tlast_0_out<='0';
                if channel_counter=(channels-1) then
                    web_s<="11111111";
                    dinb_s<=(others=>'0');--we overwrite the last used descriptor so we dont reuse it, the host must write new descriptors
                    masterState<=finishDesc2;
                 else
                    channel_counter<=channel_counter+1;
                    masterState<=prep_desc0;
                 end if;
                 writer_counter<=0;
                 readingStatus_s<="10001";
        when finishDesc2=>
           address<=x"00000ffc";--1023

            web_s<="11111111";
            dinb_s<=x"00000000"&std_logic_vector(currentOffset);
            if currentOffset>=maxAddress then --1022 last descriptor
                currentOffset<=(others=>'0');
            else
                currentOffset<=currentOffset+8;
            end if;
            masterState<=finishDesc3;
            readingStatus_s<="10010";
        when finishDesc3 =>
            address<=currentOffset;
            web_s<="00000000";
            dinb_s<=(others=>'0');
            masterState<=idle;
            readingStatus_s<="10011";

       end case;
    else
        rd_en<=(others=>'0');
        masterState<=idle;
        timeout_counter<=0;
        currentOffset<=(others=>'0');
       -- channel_counter<=0;
       -- descriptor_address_result<=0;
        --descriptor_address_result_bytes<=0;
       -- s_axis_c2h_tdata_0_out <=(others=>'0');
       -- s_axis_c2h_tkeep_0_out <=(others=>'0');
        s_axis_c2h_tvalid_0_out <='0';
       -- s_axis_c2h_tlast_0_out<='0';
       status_s(10)<='0';
        --writer_counter<=0;
    end if;
end if;

end process;




--check which streams that are running
checkStream:       process(axi_aclk,axi_aresetn)
begin
if rising_edge(axi_aclk) then
if axi_aresetn='1' and resetWriter='0' then

        --sfp 1
        if channels>=1 then
            if empty(0)='0' then
                running_counter(0)<=0;
                running(0)<='1';
            elsif running_counter(0)>=running_limit then
                running(0)<='0';
            else
                running_counter(0)<=running_counter(0)+1;
            end if;
        else
            running(0)<='0';
        end if;

        --sfp 2
        if channels>=2 then
            if empty(1)='0' then
                running_counter(1)<=0;
                running(1)<='1';
            elsif running_counter(1)>=running_limit then
                running(1)<='0';
            else
                running_counter(1)<=running_counter(1)+1;
            end if;
        else
            running(1)<='0';
        end if;

        --sfp 3
        if channels>=3 then
            if empty(2)='0' then
                running_counter(2)<=0;
                running(2)<='1';
            elsif running_counter(2)>=running_limit then
                running(2)<='0';
            else
                running_counter(2)<=running_counter(2)+1;
            end if;
        else
            running(2)<='0';
        end if;

        --sfp 4
        if channels>=4 then
            if empty(3)='0' then
                running_counter(3)<=0;
                running(3)<='1';
            elsif running_counter(3)>=running_limit then
                running(3)<='0';
            else
                running_counter(3)<=running_counter(3)+1;
            end if;
        else
            running(3)<='0';
        end if;

        --sfp 5
        if channels>=5 then
            if empty(4)='0' then
                running_counter(4)<=0;
                running(4)<='1';
            elsif running_counter(4)>=running_limit then
                running(4)<='0';
            else
                running_counter(4)<=running_counter(4)+1;
            end if;
        else
            running(4)<='0';
        end if;

        --sfp 6
        if channels>=6 then
            if empty(5)='0' then
                running_counter(5)<=0;
                running(5)<='1';
            elsif running_counter(5)>=running_limit then
                running(5)<='0';
            else
                running_counter(5)<=running_counter(5)+1;
            end if;
        else
            running(5)<='0';
        end if;

        --sfp 7
        if channels>=7 then
            if empty(6)='0' then
                running_counter(6)<=0;
                running(6)<='1';
            elsif running_counter(6)>=running_limit then
                running(6)<='0';
            else
                running_counter(6)<=running_counter(6)+1;
            end if;
        else
            running(6)<='0';
        end if;

        --sfp 8
        if channels>=8 then
            if empty(7)='0' then
                running_counter(7)<=0;
                running(7)<='1';
            elsif running_counter(7)>=running_limit then
                running(7)<='0';
            else
                running_counter(7)<=running_counter(7)+1;
            end if;
        else
            running(7)<='0';
        end if;

        --sfp 9
        if channels>=9 then
            if empty(8)='0' then
                running_counter(8)<=0;
                running(8)<='1';
            elsif running_counter(8)>=running_limit then
                running(8)<='0';
            else
                running_counter(8)<=running_counter(8)+1;
            end if;
        else
            running(8)<='0';
        end if;

        --sfp 10
        if channels>=10 then
            if empty(9)='0' then
                running_counter(9)<=0;
                running(9)<='1';
            elsif running_counter(9)>=running_limit then
                running(9)<='0';
            else
                running_counter(9)<=running_counter(9)+1;
            end if;
        else
            running(9)<='0';
        end if;





else
    running_counter<=(others=>running_limit);
    running<=(others=>'0');
end if;


end if;

end process;

end Behavioral;
