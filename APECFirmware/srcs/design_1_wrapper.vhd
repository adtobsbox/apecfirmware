--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Wed Jul 13 16:39:21 2022
--Host        : PCBE16571 running 64-bit Ubuntu 22.04 LTS
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    CPLD_FIRMWARE : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DDR3_CLK_clk_n : in STD_LOGIC;
    DDR3_CLK_clk_p : in STD_LOGIC;
    DIR : out STD_LOGIC;
    FEX0_P : in STD_LOGIC;
    FEX_DIR : in STD_LOGIC;
    GBTCLK0_M2C_N : in STD_LOGIC;
    GBTCLK0_M2C_P : in STD_LOGIC;
    I2C_SCL : out STD_LOGIC;
    I2C_SDA : inout STD_LOGIC;
    LA00_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    LA00_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    LA01_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    MODDET : in STD_LOGIC_VECTOR ( 9 downto 0 );
    PCIE_CLK_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    PCIE_CLK_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    PCIe_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIe_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIe_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIe_txp : out STD_LOGIC_VECTOR ( 7 downto 0 );
    RXN_IN : in STD_LOGIC_VECTOR ( 9 downto 0 );
    RXP_IN : in STD_LOGIC_VECTOR ( 9 downto 0 );
    RX_LOS : in STD_LOGIC_VECTOR ( 9 downto 0 );
    TXN_OUT : out STD_LOGIC_VECTOR ( 9 downto 0 );
    TXP_OUT : out STD_LOGIC_VECTOR ( 9 downto 0 );
    VAUXN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    VAUXP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    VN : in STD_LOGIC;
    VP : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    CPLD_FIRMWARE : in STD_LOGIC_VECTOR ( 9 downto 0 );
    DIR : out STD_LOGIC;
    FEX0_P : in STD_LOGIC;
    FEX_DIR : in STD_LOGIC;
    GBTCLK0_M2C_N : in STD_LOGIC;
    GBTCLK0_M2C_P : in STD_LOGIC;
    I2C_SCL : out STD_LOGIC;
    I2C_SDA : inout STD_LOGIC;
    LA00_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    LA00_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    LA01_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    MODDET : in STD_LOGIC_VECTOR ( 9 downto 0 );
    RXN_IN : in STD_LOGIC_VECTOR ( 9 downto 0 );
    RXP_IN : in STD_LOGIC_VECTOR ( 9 downto 0 );
    RX_LOS : in STD_LOGIC_VECTOR ( 9 downto 0 );
    TXN_OUT : out STD_LOGIC_VECTOR ( 9 downto 0 );
    TXP_OUT : out STD_LOGIC_VECTOR ( 9 downto 0 );
    VAUXN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    VAUXP : in STD_LOGIC_VECTOR ( 3 downto 0 );
    VN : in STD_LOGIC;
    VP : in STD_LOGIC;
    DDR3_CLK_clk_n : in STD_LOGIC;
    DDR3_CLK_clk_p : in STD_LOGIC;
    PCIe_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIe_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIe_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIe_txp : out STD_LOGIC_VECTOR ( 7 downto 0 );
    PCIE_CLK_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    PCIE_CLK_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      CPLD_FIRMWARE(9 downto 0) => CPLD_FIRMWARE(9 downto 0),
      DDR3_CLK_clk_n => DDR3_CLK_clk_n,
      DDR3_CLK_clk_p => DDR3_CLK_clk_p,
      DIR => DIR,
      FEX0_P => FEX0_P,
      FEX_DIR => FEX_DIR,
      GBTCLK0_M2C_N => GBTCLK0_M2C_N,
      GBTCLK0_M2C_P => GBTCLK0_M2C_P,
      I2C_SCL => I2C_SCL,
      I2C_SDA => I2C_SDA,
      LA00_N(0) => LA00_N(0),
      LA00_P(0) => LA00_P(0),
      LA01_P(0) => LA01_P(0),
      MODDET(9 downto 0) => MODDET(9 downto 0),
      PCIE_CLK_clk_n(0) => PCIE_CLK_clk_n(0),
      PCIE_CLK_clk_p(0) => PCIE_CLK_clk_p(0),
      PCIe_rxn(7 downto 0) => PCIe_rxn(7 downto 0),
      PCIe_rxp(7 downto 0) => PCIe_rxp(7 downto 0),
      PCIe_txn(7 downto 0) => PCIe_txn(7 downto 0),
      PCIe_txp(7 downto 0) => PCIe_txp(7 downto 0),
      RXN_IN(9 downto 0) => RXN_IN(9 downto 0),
      RXP_IN(9 downto 0) => RXP_IN(9 downto 0),
      RX_LOS(9 downto 0) => RX_LOS(9 downto 0),
      TXN_OUT(9 downto 0) => TXN_OUT(9 downto 0),
      TXP_OUT(9 downto 0) => TXP_OUT(9 downto 0),
      VAUXN(3 downto 0) => VAUXN(3 downto 0),
      VAUXP(3 downto 0) => VAUXP(3 downto 0),
      VN => VN,
      VP => VP
    );
end STRUCTURE;
