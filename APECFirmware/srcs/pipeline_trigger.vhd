----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/21/2022 01:27:57 PM
-- Design Name: 
-- Module Name: pipeline_trigger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity pipeline_trigger is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           interupt : out STD_LOGIC;
           interupt_ack : in STD_LOGIC;
           interupt_rst : in std_logic;
           data0 : in STD_LOGIC_VECTOR (271 downto 0);
           wr_en0 : in std_logic;
           wr_clk0 : in std_logic;           
      clka : IN STD_LOGIC;
      rsta : IN STD_LOGIC;
      ena : IN STD_LOGIC;
      wea : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      addra : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
end pipeline_trigger;

architecture Behavioral of pipeline_trigger is
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
    ATTRIBUTE X_INTERFACE_INFO OF douta: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  ATTRIBUTE X_INTERFACE_INFO OF dina: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  ATTRIBUTE X_INTERFACE_INFO OF addra: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  ATTRIBUTE X_INTERFACE_INFO OF wea: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
  ATTRIBUTE X_INTERFACE_INFO OF ena: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  ATTRIBUTE X_INTERFACE_INFO OF rsta: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clka: SIGNAL IS "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 0, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE BRAM_CTRL, READ_WRITE_MODE READ_WRITE, READ_LATENCY 1";
  ATTRIBUTE X_INTERFACE_INFO OF clka: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  
  
  component fifo_trigger_0
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(271 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(271 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END component ;
  
  signal interupt_r : std_Logic;
  
  type data_array_type is array (0 to 4) of std_logic_vector(31 downto 0);
  signal stream0_mask:data_array_type;
  signal stream0_data:data_array_type;
  signal stream0_offset:data_array_type;
  signal stream0_misc:data_array_type;
  
  signal stream0_reset : std_logic;
  signal stream0_rd_en : std_logic;
  signal stream0_dout : std_logic_vector(271 downto 0);
  signal stream0_full : std_logic;
  signal stream0_empty : std_logic;
  signal stream0_wr_rst_busy : std_logic;
  signal stream0_rd_rst_busy : std_logic;
  
  signal stream0_trigger : std_logic_vector(3 downto 0);
  
  signal address :  integer;
  
  subtype stream_integer_type is integer range 0 to 3 ;
  signal stream : stream_integer_type:=0;
  
  subtype vector_integer_type is integer range 0 to 3 ;
  signal vector : vector_integer_type:=0;

  subtype vector_element_integer_type is integer range 0 to 4 ;
  signal vector_element : vector_element_integer_type:=0;
  
type bus_array_type is array (0 to 7) of std_logic_vector(31 downto 0);
signal data0_r : std_logic_vector(271 downto 0);
signal stream0_input:bus_array_type;
signal stream0_counter : std_logic_vector(13 downto 0);


type triggerStateType is (idle,trigger, waitForAck,waitForReset);
signal stream0TriggerState : triggerStateType:=idle;


begin

stream0_reset<=not reset;
stream0_rd_en<= not stream0_empty;

fifo0: fifo_trigger_0 PORT MAP(rst=>stream0_reset,wr_clk =>wr_clk0,rd_clk =>clk,din =>data0,wr_en =>wr_en0,rd_en=>stream0_rd_en,dout =>stream0_dout,full =>stream0_full,empty=>stream0_empty,wr_rst_busy=>stream0_wr_rst_busy,rd_rst_busy=>stream0_rd_rst_busy);

interupt<=interupt_r;
vector  <=to_integer(unsigned(addra(7 downto 6)));
vector_element  <=to_integer(unsigned(addra(5 downto 2)));

stream0_counter<=data0_r(15 downto 2);

stream0_input(0)<=data0_r(271 downto 240);
stream0_input(1)<=data0_r(239 downto 208);
stream0_input(2)<=data0_r(207 downto 176);
stream0_input(3)<=data0_r(175 downto 144);
stream0_input(4)<=data0_r(143 downto 112);
stream0_input(5)<=data0_r(111 downto 80);
stream0_input(6)<=data0_r(79 downto 48);
stream0_input(7)<=data0_r(47 downto 16);


inputProcess: process(clk)
begin
    if rising_edge(clk) then
        if reset='1' then
            if stream0_empty='0' then
                data0_r<=stream0_dout;
            else
                data0_r<=(others=>'0');
            end if;    
        else
            data0_r<=(others=>'0');
        end if;
    end if;
end process;

stream0TriggerProcess: process(clk)
begin
 if rising_edge(clk) then
 if interupt_rst='0' and reset='1' then
 case (stream0TriggerState) is
    when idle =>
        stream0_misc(0)(7 downto 0) <="00000001";
        if stream0_trigger(0)='1' then
            stream0_misc(1)<=std_logic_vector( to_unsigned( 0, stream0_misc(1)'length));
            stream0_misc(2)<=stream0_mask(0);
            stream0_misc(3)<=stream0_data(0);
            stream0_misc(4)<=stream0_offset(0);
            stream0TriggerState<=trigger;
        end if;
        if stream0_trigger(1)='1' then
            stream0_misc(1)<=std_logic_vector( to_unsigned( 1, stream0_misc(1)'length));
            stream0_misc(2)<=stream0_mask(1);
            stream0_misc(3)<=stream0_data(1);
            stream0_misc(4)<=stream0_offset(1);
            stream0TriggerState<=trigger;
        end if;
         if stream0_trigger(2)='1' then
            stream0_misc(1)<=std_logic_vector( to_unsigned( 2, stream0_misc(1)'length));
            stream0_misc(2)<=stream0_mask(2);
            stream0_misc(3)<=stream0_data(2);
            stream0_misc(4)<=stream0_offset(2);
            stream0TriggerState<=trigger;
        end if;
        if stream0_trigger(3)='1' then
            stream0_misc(1)<=std_logic_vector( to_unsigned( 3, stream0_misc(1)'length));
            stream0_misc(2)<=stream0_mask(3);
            stream0_misc(3)<=stream0_data(3);
            stream0_misc(4)<=stream0_offset(3);
            stream0TriggerState<=trigger;
        end if;
--        if stream0_trigger(4)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 4, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(4);
--            stream0_misc(3)<=stream0_data(4);
--            stream0_misc(4)<=stream0_offset(4);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(5)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 5, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(5);
--            stream0_misc(3)<=stream0_data(5);
--            stream0_misc(4)<=stream0_offset(5);
--            stream0TriggerState<=trigger;
--        end if;
--                if stream0_trigger(6)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 6, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(6);
--            stream0_misc(3)<=stream0_data(6);
--            stream0_misc(4)<=stream0_offset(6);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(7)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 7, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(7);
--            stream0_misc(3)<=stream0_data(7);
--            stream0_misc(4)<=stream0_offset(7);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(8)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 8, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(8);
--            stream0_misc(3)<=stream0_data(8);
--            stream0_misc(4)<=stream0_offset(8);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(9)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 9, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(9);
--            stream0_misc(3)<=stream0_data(9);
--            stream0_misc(4)<=stream0_offset(9);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(10)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 10, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(10);
--            stream0_misc(3)<=stream0_data(10);
--            stream0_misc(4)<=stream0_offset(10);
--            stream0TriggerState<=trigger;
--        end if;
--         if stream0_trigger(11)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 11, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(11);
--            stream0_misc(3)<=stream0_data(11);
--            stream0_misc(4)<=stream0_offset(11);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(12)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 12, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(12);
--            stream0_misc(3)<=stream0_data(12);
--            stream0_misc(4)<=stream0_offset(12);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(13)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 13, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(13);
--            stream0_misc(3)<=stream0_data(13);
--            stream0_misc(4)<=stream0_offset(13);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(14)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 14, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(14);
--            stream0_misc(3)<=stream0_data(14);
--            stream0_misc(4)<=stream0_offset(14);
--            stream0TriggerState<=trigger;
--        end if;
--        if stream0_trigger(15)='1' then
--            stream0_misc(1)<=std_logic_vector( to_unsigned( 15, stream0_misc(1)'length));
--            stream0_misc(2)<=stream0_mask(15);
--            stream0_misc(3)<=stream0_data(15);
--            stream0_misc(4)<=stream0_offset(15);
--            stream0TriggerState<=trigger;
--        end if;
     when trigger=>
        
        stream0_misc(0)(7 downto 0) <="00000010";
            interupt_r<='1';
            stream0TriggerState<=waitForAck;
     when waitForAck =>
        stream0_misc(0)(7 downto 0) <="00000011";
        if interupt_ack='1' then
            stream0TriggerState<=waitForReset;
        end if;
    when waitForReset=>
        stream0_misc(0)(7 downto 0) <="00000100";
        null;
    when others=>
        stream0_misc(0)(7 downto 0) <="00000101";
        null;
 end case;
 
 else
    stream0TriggerState<=idle;
    stream0_misc(0)<=(others=>'0');
    interupt_r<='0';
 
 end if;
 end if;
end process;



stream0Process0: process(clk)
constant number: integer:=0;
begin
if rising_edge(clk) then
if reset='1' then
        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
                stream0_trigger(number)<='1';
             else
                stream0_trigger(number)<='0';
             end if;
        else
            stream0_trigger(number)<='0';
        end if;
else
    stream0_trigger(number)<='0';
end if;
end if;
end process;

stream0Process1: process(clk)
constant number: integer:=1;
begin
if rising_edge(clk) then
if reset='1' then
        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
                stream0_trigger(number)<='1';
             else
                stream0_trigger(number)<='0';
             end if;
        else
            stream0_trigger(number)<='0';
        end if;
else
    stream0_trigger(number)<='0';
end if;
end if;
end process;


stream0Process2: process(clk)
constant number: integer:=2;
begin
if rising_edge(clk) then
if reset='1' then
        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
                stream0_trigger(number)<='1';
             else
                stream0_trigger(number)<='0';
             end if;
        else
            stream0_trigger(number)<='0';
        end if;
else
    stream0_trigger(number)<='0';
end if;
end if;
end process;

stream0Process3: process(clk)
constant number: integer:=3;
begin
if rising_edge(clk) then
if reset='1' then
        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
                stream0_trigger(number)<='1';
             else
                stream0_trigger(number)<='0';
             end if;
        else
            stream0_trigger(number)<='0';
        end if;
else
    stream0_trigger(number)<='0';
end if;
end if;
end process;

--stream0Process4: process(clk)
--constant number: integer:=4;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process5: process(clk)
--constant number: integer:=5;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process6: process(clk)
--constant number: integer:=6;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;

--stream0Process7: process(clk)
--constant number: integer:=7;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process8: process(clk)
--constant number: integer:=8;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process9: process(clk)
--constant number: integer:=9;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;

--stream0Process10: process(clk)
--constant number: integer:=10;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process11: process(clk)
--constant number: integer:=11;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process12: process(clk)
--constant number: integer:=12;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process13: process(clk)
--constant number: integer:=13;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process14: process(clk)
--constant number: integer:=14;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;


--stream0Process15: process(clk)
--constant number: integer:=15;
--begin
--if rising_edge(clk) then
--if reset='1' then
--        if stream0_mask(number)/="00000000000000000000000000000000" and stream0_offset(number)(16 downto 3)=stream0_counter(13 downto 0) then
--             if (stream0_mask(number) and (stream0_data(number) xor stream0_input(to_integer(unsigned(stream0_offset(number)(2 downto 0)))) ))="00000000000000000000000000000000" then
--                stream0_trigger(number)<='1';
--             else
--                stream0_trigger(number)<='0';
--             end if;
--        else
--            stream0_trigger(number)<='0';
--        end if;
--else
--    stream0_trigger(number)<='0';
--end if;
--end if;
--end process;




memoryWriteProcess : process(clka)
begin
if rising_edge(clka) then
    if rsta='0' then
    if ena='1' and wea="1111" then 

            if vector=0 then
                stream0_mask(vector_element)<=dina;
            elsif vector=1 then
                stream0_data(vector_element)<=dina;
            elsif vector=2 then
                stream0_offset(vector_element)<=dina;
            end if;

    end if;
    else
        for i in 0 to 4 loop
            stream0_mask(i) <= (others=>'0');
            stream0_data(i) <= (others=>'0');
            stream0_offset(i) <= (others=>'0');

        end loop;
    end if; 

end if;
end process;



memoryReadProcess : process(clka)
begin
if rising_edge(clka) then
    if rsta='0' and ena='1'  then 
            if vector=0 then
                douta<=stream0_mask(vector_element);
            elsif vector=1 then
                douta<=stream0_data(vector_element);
            elsif vector=2 then
                douta<=stream0_offset(vector_element);
            elsif vector=3 then
                douta<=stream0_misc(vector_element);
            end if;
      
  else
    douta<=(others=>'0');
  end if; 

end if;
end process;

end Behavioral;
