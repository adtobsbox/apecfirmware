----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/13/2019 11:57:22 AM
-- Design Name: 
-- Module Name: SFPRegisterReader - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity SFPRegisterReader is
    Port ( I2C_SCL : out STD_LOGIC;
           I2C_SDA : inout STD_LOGIC;
           scl : in STD_LOGIC;
           reset : in STD_LOGIC;
           DIR : out STD_LOGIC;
           addrb : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
           doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
           I2C_SDA_DBG: out std_logic);
           
end SFPRegisterReader;

architecture Behavioral of SFPRegisterReader is

constant SFP8 : std_logic_vector(14 downto 0) := "1110001"& "00000001";
constant SFP7 : std_logic_vector(14 downto 0) := "1110001"& "00000010";
constant SFP2 : std_logic_vector(14 downto 0) := "1110001"& "00000100";
constant SFP1 : std_logic_vector(14 downto 0) := "1110001"& "00001000";
constant SFP3 : std_logic_vector(14 downto 0) := "1110001"& "00010000";
constant SFP4 : std_logic_vector(14 downto 0) := "1110001"& "00100000";
constant SFP5 : std_logic_vector(14 downto 0) := "1110001"& "01000000";
constant SFP6 : std_logic_vector(14 downto 0) := "1110001"& "10000000";
constant SFP9 	: std_logic_vector(14 downto 0) := "1110011"& "00010000";
constant SFP10 	: std_logic_vector(14 downto 0) := "1110011"& "00100000";

   signal temporal: STD_LOGIC:='0';
    signal counter : integer range 0 to 16 := 0;
    signal clk400k :std_logic:='0';
    
    
    signal i2c_addr : std_logic_vector(6 downto 0); -- i.e slave address
    signal i2c_reg: std_logic_vector(7 downto 0); -- register
    signal i2c_data_w : std_logic_vector(7 downto 0); -- data to set in register
    signal i2c_data_r : std_logic_vector(7 downto 0); -- data to read from register
    signal i2c_wr : std_logic;    -- 1=write
    signal i2c_ena : std_logic; -- enable the state machine
    signal i2c_busy : std_logic;-- 1= transmitting
    signal i2c_ack : std_logic; -- 1 if slave acknowledge transmission
    signal i2c_bus_switch : std_logic; --tell the I2C logic that we are setting the I2C bus Switch
    signal i2c_retry : std_logic;
    
    signal i2c2_sdaoen:  std_logic;
    signal i2c2_sdao: std_logic;
    signal i2c2_sdai: std_logic;
    
    signal wea :  STD_LOGIC_VECTOR(0 DOWNTO 0);
    signal addra :  STD_LOGIC_VECTOR(6 DOWNTO 0);
    signal dina :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal addrb_s :  STD_LOGIC_VECTOR(6 DOWNTO 0);
    signal doutb_s :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    
    component SFP_MEM IS
      PORT (
        clka : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        clkb : IN STD_LOGIC;
        addrb : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );
    END component;
    
begin
addrb_s<=addrb;
doutb<=doutb_s;
IOBUF_inst:IOBUF generic map(DRIVE=>12,IOSTANDARD=>"DEFAULT",SLEW=>"SLOW") 
port map(O=>i2c2_sdai,--Bufferoutput
IO=>I2C_SDA,--Bufferinoutport(connectdirectlytotop-levelport)
I=>i2c2_sdao,--Bufferinput
T=>i2c2_sdaoen--3-stateenableinput,high=input,low=output
);

    SFP_MEM_INST: SFP_MEM 
      PORT MAP(
        clka=>scl,
        wea =>wea,
        addra =>addra,
        dina =>dina,
        clkb =>scl,
        addrb =>addrb_s,
        doutb =>doutb_s
      );

    frequency_divider: process ( scl) 
    begin

        if rising_edge(scl) then
            if (counter = 16) then
                temporal <= NOT(temporal);
                counter <= 0;
            else
                counter <= counter + 1;
            end if;
        end if;
    end process;
    
    clk400k <= temporal;

--continously read and verify the status of each sfp and store the data in the SFPRAM
debugProcess: process(scl,reset)
begin
if rising_edge(scl) then
        if i2c2_sdaoen='0' then
           --sending
           I2C_SDA_DBG<=i2c2_sdao;
        else
           --receiving
           I2C_SDA_DBG<=i2c2_sdai;
        end if;
    end if;
    end process;
-- I tried to use the hard IP EBR I2C core but I hard troubles to read from the I2C slaves
-- and the logic for interfacing with the wishbone bus was more complicated than this so I went this way
process(clk400k,reset)
--transmit
variable sdaTransmitState : std_logic_vector(75 downto 0);
variable sclTransmitState : std_logic_vector(75 downto 0);
variable sdaOutEnTransmitState : std_logic_vector(75 downto 0);
--send
variable sdaSendState : std_logic_vector(39 downto 0);
variable sclSendState : std_logic_vector(39 downto 0);
variable sdaOutEnSendState : std_logic_vector(39 downto 0);
--receive
variable sdaRecvState : std_logic_vector(83 downto 0);
variable sclRecvState : std_logic_vector(83 downto 0);
variable sdaOutEnRecvState : std_logic_vector(83 downto 0);


variable I2CTransmitCounter : integer;
variable I2CSecondCounter : integer;
variable I2CWrite : std_logic;
variable I2CRetryCounter : integer;
begin
	if rising_edge(clk400k) then
		if reset='1' then
			I2CTransmitCounter:=sclTransmitState'length -1;
			I2CWrite:='0';
			i2c_busy<='0';
			i2c_ack<='1';
			I2CRetryCounter:=0;
			--DIR<='0' is send
			DIR<='1';

			I2C_SCL<='1';
			
			i2c2_sdaoen<='1';
			i2c2_sdao<='1';
			  --			   start addr 													ack 	reg 													 ack		 	
			sclTransmitState:="1110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110";
			sdaTransmitState:="1100"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000";
	       sdaOutEnTransmitState:="1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"0000"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"0000";
			--       		data 													ack 	stop
			sclSendState:="0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0111";
			sdaSendState:="0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0011";
			sdaOutEnSendState:="1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"0000"&"1111";
			--				stop  start   adr                    							w/r   ack  data from slave    										 nack   stop
		     sclRecvState:="0111"&"1110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0110"&"0111";
		     sdaRecvState:="0011"&"1100"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"1111"&"0011";
			sdaOutEnRecvState:="1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"1111"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"0000"&"1111"&"1111";
		else
			--nothing
			if i2c_busy='0' and i2c_ena='0' then
				i2c_ack<='1';
			--setup
			elsif i2c_busy='0' and i2c_ena='1' then

				--slave address
				sdaTransmitState(71 downto 68):=(others=>i2c_addr(6));
				sdaTransmitState(67 downto 64):=(others=>i2c_addr(5));
				sdaTransmitState(63 downto 60):=(others=>i2c_addr(4));
				sdaTransmitState(59 downto 56):=(others=>i2c_addr(3));
				sdaTransmitState(55 downto 52):=(others=>i2c_addr(2));
				sdaTransmitState(51 downto 48):=(others=>i2c_addr(1));
				sdaTransmitState(47 downto 44):=(others=>i2c_addr(0));
				sdaTransmitState(43 downto 40):=(others=>'0');

				--slave address for recv
				sdaRecvState(75 downto 72):=(others=>i2c_addr(6));
				sdaRecvState(71 downto 68):=(others=>i2c_addr(5));
				sdaRecvState(67 downto 64):=(others=>i2c_addr(4));
				sdaRecvState(63 downto 60):=(others=>i2c_addr(3));
				sdaRecvState(59 downto 56):=(others=>i2c_addr(2));
				sdaRecvState(55 downto 52):=(others=>i2c_addr(1));
				sdaRecvState(51 downto 48):=(others=>i2c_addr(0));
				sdaRecvState(47 downto 44):=(others=>'1');


				--register
				sdaTransmitState(35 downto 32):=(others=>i2c_reg(7));
				sdaTransmitState(31 downto 28):=(others=>i2c_reg(6));
				sdaTransmitState(27 downto 24):=(others=>i2c_reg(5));
				sdaTransmitState(23 downto 20):=(others=>i2c_reg(4));
				sdaTransmitState(19 downto 16):=(others=>i2c_reg(3));
				sdaTransmitState(15 downto 12):=(others=>i2c_reg(2));
				sdaTransmitState(11 downto 8):=(others=>i2c_reg(1));
				sdaTransmitState(7 downto 4):=(others=>i2c_reg(0));

				-- data not used if recv
				sdaSendState(39 downto 36):=(others=>i2c_data_w(7));
				sdaSendState(35 downto 32):=(others=>i2c_data_w(6));
				sdaSendState(31 downto 28):=(others=>i2c_data_w(5));
				sdaSendState(27 downto 24):=(others=>i2c_data_w(4));
				sdaSendState(23 downto 20):=(others=>i2c_data_w(3));
				sdaSendState(19 downto 16):=(others=>i2c_data_w(2));
				sdaSendState(15 downto 12):=(others=>i2c_data_w(1));
				sdaSendState(11 downto 8):=(others=>i2c_data_w(0));

				i2c_busy<='1';
				I2CWrite:=i2c_wr;
				I2CTransmitCounter:=sclTransmitState'length -1;
				i2c_ack<='1';
				I2CRetryCounter:=0;

			--run the sequence
			else
				--we are running the transmit sequence
				if I2CTransmitCounter>=0 then
					--i2c clk
					I2C_SCL<=sclTransmitState(I2CTransmitCounter);

					 --i2c sda
					i2c2_sdao<=sdaTransmitState(I2CTransmitCounter);
					if sdaTransmitState(I2CTransmitCounter)='0' and sdaOutEnTransmitState(I2CTransmitCounter)='1' then
						i2c2_sdaoen<='0';
					else
						i2c2_sdaoen<='1';
					end if;
					DIR<=not sdaOutEnTransmitState(I2CTransmitCounter);
					-- check ack, we just and all
					if I2CTransmitCounter=37 or I2CTransmitCounter=1 then
						i2c_ack<=i2c_ack and (not i2c2_sdai);
					end if;
					if I2CTransmitCounter=0 then
						if I2CWrite='1' then
							--if we are only setting the busswitch then we only need to send a stop
							if i2c_bus_switch='0' then
								I2CSecondCounter:=sclSendState'length-1;
							else
								I2CSecondCounter:=3;
							end if;
						else
							I2CSecondCounter:=sclRecvState'length-1;
						end if;
					end if;
					I2CTransmitCounter:=I2CTransmitCounter-1;
				else
					--we are running the send sequence
					if I2CWrite='1' then
						I2C_SCL<=sclSendState(I2CSecondCounter);

					 	 --i2c sda
						i2c2_sdao<=sdaSendState(I2CSecondCounter);
						DIR<=not sdaOutEnSendState(I2CSecondCounter);
						if sdaSendState(I2CSecondCounter)='0' and sdaOutEnSendState(I2CSecondCounter)='1' then
							i2c2_sdaoen<='0';
						else
							i2c2_sdaoen<='1';
						end if;
						if I2CSecondCounter=5 then
							i2c_ack<=i2c_ack and (not i2c2_sdai);
						end if;
					else
					--we are running the recv sequence
						I2C_SCL<=sclRecvState(I2CSecondCounter);

					 	 --i2c sda
						i2c2_sdao<=sdaRecvState(I2CSecondCounter);
						DIR<=not sdaOutEnRecvState(I2CSecondCounter);
						if sdaRecvState(I2CSecondCounter)='0' and sdaOutEnRecvState(I2CSecondCounter)='1' then
							i2c2_sdaoen<='0';
						else
							i2c2_sdaoen<='1';
						end if;
						--ack
						if I2CSecondCounter=41 then
							i2c_ack<=i2c_ack and (not i2c2_sdai);
						end if;

						--recv data
						if I2CSecondCounter=37 then
							i2c_data_r(7)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=33 then
							i2c_data_r(6)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=29 then
							i2c_data_r(5)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=25 then
							i2c_data_r(4)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=21 then
							i2c_data_r(3)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=17 then
							i2c_data_r(2)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=13 then
							i2c_data_r(1)<=i2c2_sdai;
						end if;
						if I2CSecondCounter=9 then
							i2c_data_r(0)<=i2c2_sdai;
						end if;
					end if;
					--we are done
					if I2CSecondCounter=0 then
						--if the slave responded correctly or we have tried to send 10 times
						if i2c_ack='1' or I2CRetryCounter>10 or i2c_retry='0' then
							i2c_busy<='0';
						--if not then we try again
						else
							I2CTransmitCounter:=sclTransmitState'length -1;
							i2c_ack<='1';
							I2CRetryCounter:=I2CRetryCounter+1;
						end if;
					end if;
					I2CSecondCounter:=I2CSecondCounter-1;

				end if;
			end if;
		end if;
	end if;
end process;


--continously read and verify the status of each sfp and store the data in the SFPRAM
SFPReader: process(scl,reset)
type SFPArray is array (9 downto 0) of std_logic_vector(14 downto 0);
variable SFPs : SFPArray;
variable SFPCounter,SFPRegisterCounter,SFPRAMWaitCounter : integer;
type SFPReadStateType is (start,initBusSwitches0,initBusSwitches1,readFromSFP0,readFromSFP1,readFromSFP2,readFromSFP3,readFromSFP4,readFromSFP5,readFromSFP6,readFromSFP7,waitForI2CTransmission0,waitForI2CTransmission1);
type SFPRegistersType is array(9 downto 0) of integer;
variable SFPRegisters: SFPRegistersType;
variable SFPReadState,SFPReadStateNext: SFPReadStateType;
begin
if rising_edge(scl) then
	if reset='1' then
	--the busswitch address and the control register setup, the slave address is the same for all
		SFPs:=(SFP10,SFP9,SFP8,SFP7,SFP6,SFP5,SFP4,SFP3,SFP2,SFP1);
		--which register we should read,update readFromSFP2 if more registers are added
		SFPRegisters:=(117,116,113,112,105,104,99,98,97,96);
		SFPCounter:=0;
		SFPRegisterCounter:=0;
		SFPRAMWaitCounter:=0;
		SFPReadState:=start;
		--i2c default state
		i2c_reg<=(others=>'0');
		i2c_addr<=(others=>'0');
		i2c_data_w<=(others=>'0');
		i2c_wr<='0';
		i2c_ena<='0';
		i2c_retry<='0'; --we dont care to retry a transmission if it fails
	else
	case SFPReadState is
		--wait until we get the go signal from the main state machine
		when start=>
            SFPReadState:=initBusSwitches0;
		--set the busswitches to 0 so we dont have two sfps colliding
		when initBusSwitches0=>
			i2c_reg<="00000000";
			i2c_wr<='1';
			i2c_ena<='1';
			i2c_bus_switch<='1';
			i2c_addr<="1110001";
			SFPReadState:=waitForI2CTransmission0;
			SFPReadStateNext:=initBusSwitches1;
			if SFPCounter>=SFPs'length then
				SFPCounter:=0;
			end if;
		when initBusSwitches1=>
			i2c_reg<="00000000";
			i2c_wr<='1';
			i2c_ena<='1';
			i2c_bus_switch<='1';
			i2c_addr<="1110011";
			SFPReadState:=waitForI2CTransmission0;
			SFPReadStateNext:=readFromSFP0;
		--Setup the bus switch to read from the current SFP
		when readFromSFP0=>
			i2c_reg<=SFPs(SFPCounter)(7 downto 0);
			i2c_wr<='1';
			i2c_ena<='1';
			i2c_bus_switch<='1';
			i2c_addr<=SFPs(SFPCounter)(14 downto 8);
			SFPReadState:=waitForI2CTransmission0;
			SFPReadStateNext:=readFromSFP1;
			--read from the current SFP and the current register
		when readFromSFP1=>
			i2c_reg<=std_logic_vector(to_unsigned(SFPRegisters(SFPRegisterCounter), i2c_reg'length));
			i2c_wr<='0';
			i2c_ena<='1';
			i2c_bus_switch<='0';
			i2c_addr<="1010001";
			SFPReadState:=waitForI2CTransmission0;
			SFPReadStateNext:=readFromSFP2;

		--write the data into the corrent line in SFPRAM
		when readFromSFP2=>
			--16 byte align to make it easier to read the data from the Virtex
			addra<=std_logic_vector(to_unsigned(SFPCounter*10+SFPRegisterCounter, addra'length));
			dina(7 downto 0)<=i2c_data_r;
			dina(31 downto 8)<=(others=>'0');
			wea(0)<='1';
			SFPReadState:=readFromSFP3;
			SFPRAMWaitCounter:=0;
		--wait a few cycles for the RAM
		when readFromSFP3=>
			SFPRAMWaitCounter:=SFPRAMWaitCounter+1;
			if SFPRAMWaitCounter>=10 then
				SFPReadState:=readFromSFP4;
				wea(0)<='0';
			end if;
		--increment the register counter
		when readFromSFP4=>
			SFPRegisterCounter:=SFPRegisterCounter+1;
			SFPReadState:=readFromSFP5;
		-- if the registerCounter is larger then the array then we reset it and goto ReadFromSFP5(change SFP), else readFromSFP7
		when readFromSFP5=>
			if SFPRegisterCounter>=SFPRegisters'length then
				SFPRegisterCounter:=0;
				SFPCounter:=SFPCounter+1;
				SFPReadState:=initBusSwitches0;
			else
				SFPReadState:=readFromSFP1;
			end if;
		when waitForI2CTransmission0 =>
			if i2c_busy='1'then
				i2c_ena<='0';
				SFPReadState:=waitForI2CTransmission1;
			end if;
		--wait for I2C module to be done and go to next state
		when waitForI2CTransmission1=>
			if i2c_busy='0'then
				SFPReadState:=SFPReadStateNext;
			end if;

		when others=>
			null;
		end case;

null;
	end if;
end if;
end process;


end Behavioral;
