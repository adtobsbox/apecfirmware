----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/21/2019 04:22:18 PM
-- Design Name: 
-- Module Name: clockDomainCrossing - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity clockDomainCrossing is
  Port (clkA : in std_logic;
        clkB : in std_logic;
        inputA : in std_logic;
        outputB: out std_logic );
end clockDomainCrossing;

architecture Behavioral of clockDomainCrossing is

signal Q : std_logic;
signal C : std_logic;
signal CE : std_logic;
signal CLR : std_logic;
signal D : std_logic;

signal Q1 : std_logic;
signal C1 : std_logic;
signal CE1 : std_logic;
signal CLR1 : std_logic;
signal D1 : std_logic;

signal Q2 : std_logic;
signal C2 : std_logic;
signal CE2 : std_logic;
signal CLR2 : std_logic;
signal D2 : std_logic;

begin
--first FF
C<=clkA;
CE<='1';
CLR<='0';
D<=inputA;

C1<=clkB;
CE1<='1';
CLR1<='0';
D1<=Q;

C2<=clkB;
CE2<='1';
CLR2<='0';
D2<=Q1;

outputB<=Q2;



FDCE_inst0: FDCE
generic map(INIT=>'0')
port map(
Q=>Q,
C=>C,
CE=>CE,
CLR=>CLR,
D=>D
);

FDCE_inst1: FDCE
generic map(INIT=>'0')
port map(
Q=>Q1,
C=>C1,
CE=>CE1,
CLR=>CLR1,
D=>D1
);

FDCE_inst2: FDCE
generic map(INIT=>'0')
port map(
Q=>Q2,
C=>C2,
CE=>CE2,
CLR=>CLR2,
D=>D2
);




end Behavioral;
