set_false_path -through [get_nets design_1_i/registerController_0_polling]
set_false_path -through [get_nets [list {design_1_i/dataBuffer_0/U0/transferOffset90[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[7]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[7]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[7]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[7]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[3]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[3]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[3]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[13]_i_2_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[7]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[7]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[0]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[8]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[8]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[8]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[8]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[4]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset70[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[4]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[4]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[12]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[12]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[12]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[12]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[13]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[8]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset90[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[8]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[8]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[4]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[0]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[7]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[7]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[7]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[7]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[3]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[3]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[3]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[13]_i_2_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[11]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[11]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[3]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[3]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[3]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[13]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[13]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[13]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[11]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[11]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[11]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[11]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[11]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[11]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[4]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[11]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[11]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[4]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[12]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[7]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[11]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[12]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg[3]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[1]} \
          design_1_i/dataBuffer_0/U0/transferOffset2 \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg[4]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[7]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[11]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[0]} \
          design_1_i/dataBuffer_0/U0/transferOffset1 \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[0]} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[7]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[3]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[12]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[12]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3[7]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[3]} \
          design_1_i/dataBuffer_0/U0/transferOffset7 \
          {design_1_i/dataBuffer_0/U0/transferOffset50[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[5]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[8]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset30[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset2_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset3_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4[13]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset4_reg_n_0_[9]} \
          design_1_i/dataBuffer_0/U0/transferOffset8 \
          design_1_i/dataBuffer_0/U0/transferOffset5 \
          {design_1_i/dataBuffer_0/U0/transferOffset7[11]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[11]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[13]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[11]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg[11]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset50[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[12]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[12]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[12]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[12]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[13]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[4]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[4]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[4]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[8]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[8]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[8]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5[8]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset7_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[12]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[13]_i_2_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[12]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset8_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[12]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[9]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[12]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[9]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[4]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[13]_i_2_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[4]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[9]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[4]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[5]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[4]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[5]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[8]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[9]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[8]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[13]_i_6_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[8]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[13]_i_5_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg[8]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset9[13]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[0]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[5]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[13]_i_2_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[11]} \
          design_1_i/dataBuffer_0/U0/transferOffset9 \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[9]_i_1_n_1} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[5]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[1]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[5]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[5]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[9]_i_1_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[9]_i_1_n_2} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg[9]_i_1_n_3} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[0]} \
          {design_1_i/dataBuffer_0/U0/transferOffset5_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[9]} \
          design_1_i/dataBuffer_0/U0/transferOffset6 \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[10]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[11]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[12]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[13]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[13]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[3]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[3]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[3]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[4]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[3]_i_4_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[5]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[7]_i_2_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset7[7]_i_3_n_0} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[8]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6_reg_n_0_[7]} \
          {design_1_i/dataBuffer_0/U0/transferOffset60[9]} \
          {design_1_i/dataBuffer_0/U0/transferOffset9_reg_n_0_[2]} \
          {design_1_i/dataBuffer_0/U0/transferOffset1_reg_n_0_[6]} \
          {design_1_i/dataBuffer_0/U0/transferOffset6[13]_i_3_n_0}]]
set_property PACKAGE_PIN P6 [get_ports GBTCLK0_M2C_P]

set_property PACKAGE_PIN AG17 [get_ports DDR3_CLK_clk_p]
set_property IOSTANDARD DIFF_SSTL15 [get_ports DDR3_CLK_clk_p]

#Active-high reset from the CPLD
set_property PACKAGE_PIN F34 [get_ports FEX_DIR]
set_property IOSTANDARD LVCMOS18 [get_ports FEX_DIR]

set_property PACKAGE_PIN AD6 [get_ports {PCIE_CLK_clk_p[0]}]

create_clock -period 10.000 -name sys_clk [get_ports PCIE_CLK_clk_p]

#PCIe edge reset that is passed through the cpld
set_property PACKAGE_PIN L33 [get_ports FEX0_P]
set_property IOSTANDARD LVCMOS18 [get_ports FEX0_P]

set_false_path -through [get_nets design_1_i/registerController_0_transferTotal*]
set_false_path -through [get_nets design_1_i/registerController_0_transferSizePerChannel*]
set_false_path -through [get_nets design_1_i/registerController_0_channels*]
#set_false_path -through [get_nets design_1_i/registerController_0_clockSetup*]
set_false_path -through [get_nets design_1_i/registerController_0_timeoutMin*]
set_false_path -through [get_nets design_1_i/registerController_0_timeoutMax*]
set_false_path -through [get_nets design_1_i/gt_pipeline_*_status*]
#set_false_path -through [get_nets design_1_i/bypassController_0_status*]
set_false_path -through [get_nets design_1_i/dataBuffer_0_status*]
set_false_path -through [get_nets design_1_i/gtwizard_wrapper_0_gt_FSM_resets_done*]
set_false_path -through [get_nets design_1_i/gtwizard_wrapper_0_gt_resets_done*]
set_false_path -through [get_nets design_1_i/resetController_0_rxlosmodsetstatus*]
set_false_path -through [get_nets design_1_i/resetController_0_resetStatus*]
set_false_path -through [get_nets design_1_i/registerController_0_sizePerFrame*]

set_false_path -through [get_nets -hierarchical -top_net_of_hierarchical_group *transferOffset*]

set_false_path -through [get_nets -hierarchical -top_net_of_hierarchical_group *bufferOffset*]

set_property PACKAGE_PIN E23 [get_ports {MODDET[0]}]
set_property PACKAGE_PIN H23 [get_ports {MODDET[1]}]
set_property PACKAGE_PIN J20 [get_ports {MODDET[2]}]
set_property PACKAGE_PIN F21 [get_ports {MODDET[3]}]
set_property PACKAGE_PIN C22 [get_ports {MODDET[4]}]
set_property PACKAGE_PIN D24 [get_ports {MODDET[5]}]
set_property PACKAGE_PIN M20 [get_ports {MODDET[6]}]
set_property PACKAGE_PIN A19 [get_ports {MODDET[7]}]
set_property PACKAGE_PIN F19 [get_ports {MODDET[8]}]
set_property PACKAGE_PIN K21 [get_ports {MODDET[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports MODDET*]

set_property PACKAGE_PIN F23 [get_ports {RX_LOS[0]}]
set_property PACKAGE_PIN J22 [get_ports {RX_LOS[1]}]
set_property PACKAGE_PIN H22 [get_ports {RX_LOS[2]}]
set_property PACKAGE_PIN G23 [get_ports {RX_LOS[3]}]
set_property PACKAGE_PIN A24 [get_ports {RX_LOS[4]}]
set_property PACKAGE_PIN C23 [get_ports {RX_LOS[5]}]
set_property PACKAGE_PIN B22 [get_ports {RX_LOS[6]}]
set_property PACKAGE_PIN A20 [get_ports {RX_LOS[7]}]
set_property PACKAGE_PIN J21 [get_ports {RX_LOS[8]}]
set_property PACKAGE_PIN A21 [get_ports {RX_LOS[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports RX_LOS*]

set_property PACKAGE_PIN E21 [get_ports {LA00_P[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {LA00_P[0]}]

set_property PACKAGE_PIN D21 [get_ports {LA00_N[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {LA00_N[0]}]

set_property PACKAGE_PIN G21 [get_ports {LA01_P[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {LA01_P[0]}]

set_property IOSTANDARD LVCMOS18 [get_ports {VAUXP[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {VAUXP[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {VAUXP[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {VAUXP[0]}]

set_property PACKAGE_PIN V29 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS18 [get_ports I2C_SDA]

set_property PACKAGE_PIN W29 [get_ports I2C_SCL]
set_property IOSTANDARD LVCMOS18 [get_ports I2C_SCL]

set_property PACKAGE_PIN L34 [get_ports DIR]
set_property IOSTANDARD LVCMOS18 [get_ports DIR]

# gt0=x0y14 = sfp6
# gt1=x0y15 = sfp2
# gt2=x0y16 = sfp3
# gt3=x0y17 = sfp1
# gt4=x0y18 = sfp4
# gt5=x0y19 = sfp5
# gt6=x0y20 = sfp7
# gt7=x0y21 = sfp9
# gt8=x0y22 = sfp10
# gt9=x0y23 = sfp8


set_property PACKAGE_PIN F31 [get_ports {CPLD_FIRMWARE[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[9]}]

set_property PACKAGE_PIN F30 [get_ports {CPLD_FIRMWARE[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[8]}]

set_property PACKAGE_PIN E33 [get_ports {CPLD_FIRMWARE[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[7]}]

set_property PACKAGE_PIN E32 [get_ports {CPLD_FIRMWARE[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[6]}]

set_property PACKAGE_PIN G32 [get_ports {CPLD_FIRMWARE[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[5]}]

set_property PACKAGE_PIN H32 [get_ports {CPLD_FIRMWARE[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[4]}]

set_property PACKAGE_PIN H34 [get_ports {CPLD_FIRMWARE[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[3]}]

set_property PACKAGE_PIN H33 [get_ports {CPLD_FIRMWARE[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[2]}]

set_property PACKAGE_PIN J34 [get_ports {CPLD_FIRMWARE[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[1]}]

set_property PACKAGE_PIN K34 [get_ports {CPLD_FIRMWARE[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {CPLD_FIRMWARE[0]}]