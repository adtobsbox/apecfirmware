----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/21/2019 04:07:24 PM
-- Design Name: 
-- Module Name: asyncToSync - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity asyncToSync is
  Port (datain: in std_logic;
        dataout : out std_logic;
        clk : in std_logic );
end asyncToSync;

architecture Behavioral of asyncToSync is
signal Q : std_logic;
signal C : std_logic;
signal CE : std_logic;
signal D : std_logic;
signal PRE : std_logic;

signal Q1 : std_logic;
signal C1 : std_logic;
signal CE1 : std_logic;
signal D1 : std_logic;
signal PRE1 : std_logic;

begin

C<=clk;
CE<='1';
D<='0';
PRE<=datain;
D1<=Q;

C1<=clk;
CE1<='1';
PRE1<=datain;
dataout<=Q1;

FDPE0: FDPE
generic map (
INIT => '0')
-- synthesis translate_on
port map (Q => Q,
C => C,
CE => CE,
D => D,
PRE => PRE
);

FDPE1: FDPE
generic map (
INIT => '0')
-- synthesis translate_on
port map (Q => Q1,
C => C1,
CE => CE1,
D => D1,
PRE => PRE1
);


end Behavioral;