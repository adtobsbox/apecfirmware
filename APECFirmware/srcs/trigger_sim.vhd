----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/05/2022 02:28:15 PM
-- Design Name: 
-- Module Name: trigger_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trigger_sim is
    Port (      
       clk : in std_logic;
       rst : in std_logic;
     bram_rst_a : OUT STD_LOGIC;
      bram_clk_a : OUT STD_LOGIC;
      bram_en_a : OUT STD_LOGIC;
      bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_addr_a : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
           interrupt_ack : out STD_LOGIC_VECTOR (9 downto 0);
           interrupt_rst : out STD_LOGIC_VECTOR (9 downto 0);
           interrupt : in STD_LOGIC_VECTOR (9 downto 0));
end trigger_sim;

architecture Behavioral of trigger_sim is

  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF bram_rddata_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT";
  ATTRIBUTE X_INTERFACE_INFO OF bram_wrdata_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN";
  ATTRIBUTE X_INTERFACE_INFO OF bram_addr_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR";
  ATTRIBUTE X_INTERFACE_INFO OF bram_we_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA WE";
  ATTRIBUTE X_INTERFACE_INFO OF bram_en_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA EN";
  ATTRIBUTE X_INTERFACE_INFO OF bram_clk_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_rst_a: SIGNAL IS "XIL_INTERFACENAME BRAM_PORTA, MASTER_TYPE BRAM_CTRL, MEM_SIZE 4096, MEM_WIDTH 32, MEM_ECC NONE, READ_WRITE_MODE READ_WRITE, READ_LATENCY 1";
  ATTRIBUTE X_INTERFACE_INFO OF bram_rst_a: SIGNAL IS "xilinx.com:interface:bram:1.0 BRAM_PORTA RST";

begin
bram_clk_a<=clk;
bram_rst_a<=not rst;
writeProcess: process
begin
    --tested with new_data.txt
    bram_en_a<='0';
    bram_we_a<="0000";
    bram_addr_a<=(others=>'0');
    bram_wrdata_a<=(others=>'0');
    wait until rst='0';
    wait until rst='1';
    wait until falling_edge(clk);
    bram_en_a<='1';
    bram_we_a<="1111";
    bram_addr_a<="000000" &"01" &"0000";--  data interupt 0
    bram_wrdata_a<=x"624f3173" ; --OBS1
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0000";--  mask interupt 0
    bram_wrdata_a<=x"ffffffff" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0000";--  offset interupt 0
    bram_wrdata_a<=std_logic_vector( to_unsigned( 0, bram_wrdata_a'length)) ;--data    first 32bit word in the frame
    wait until falling_edge(clk);
    
    bram_addr_a<="000000" &"01" &"0001";--  data interupt 1
    bram_wrdata_a<=x"2e000500" ;
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0001";--  mask interupt 1
    bram_wrdata_a<=x"ffffffff" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0001";--  offset interupt 1
    bram_wrdata_a<=std_logic_vector( to_unsigned( 9, bram_wrdata_a'length)) ;--data    
    wait until falling_edge(clk);
    
    bram_addr_a<="000000" &"01" &"0010";--  data interupt 2
    bram_wrdata_a<=x"a8d3ccd4" ;
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0010";--  mask interupt 2
    bram_wrdata_a<=x"ffffffff" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0010";--  offset interupt 2
    bram_wrdata_a<=std_logic_vector( to_unsigned( 18, bram_wrdata_a'length)) ;--data    
    wait until falling_edge(clk);
    
    bram_addr_a<="000000" &"01" &"0011";--  data interupt 3
    bram_wrdata_a<=x"94e91eeb" ;
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0011";--  mask interupt 3
    bram_wrdata_a<=x"ffffffff" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0011";--  offset interupt 3
    bram_wrdata_a<=std_logic_vector( to_unsigned( 27, bram_wrdata_a'length)) ;--data    
    wait until falling_edge(clk);

    bram_addr_a<="000000" &"01" &"0100";--  data interupt 4
    bram_wrdata_a<=x"f2da1cd9" ;
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0100";--  mask interupt 4
    bram_wrdata_a<=x"ffffffff" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0100";--  offset interupt 4
    bram_wrdata_a<=std_logic_vector( to_unsigned( 36, bram_wrdata_a'length)) ;--data    
    wait until falling_edge(clk);
    
    bram_addr_a<="000000" &"01" &"0101";--  data interupt 5
    bram_wrdata_a<=x"f2da1cd8" ;
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0101";--  mask interupt 5
    bram_wrdata_a<=x"ffffffff" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0101";--  offset interupt 5
    bram_wrdata_a<=std_logic_vector( to_unsigned( 36, bram_wrdata_a'length)) ;--data    
    wait until falling_edge(clk);
    
    bram_addr_a<="000000" &"01" &"0110";--  data interupt 6
    bram_wrdata_a<=x"f2da1cd8" ;
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"00" &"0110";--  mask interupt 6
    bram_wrdata_a<=x"fffffff0" ;--mask
    wait until falling_edge(clk);
    bram_addr_a<="000000" &"10" &"0110";--  offset interupt 6
    bram_wrdata_a<=std_logic_vector( to_unsigned( 36, bram_wrdata_a'length)) ;--data    
    wait until falling_edge(clk);
    
    bram_en_a<='0';
    bram_we_a<="0000";
    bram_addr_a<="000000" &"00" &"0000";--offset
    bram_wrdata_a<="00000000000000000000000000000000" ;--data
    wait;
end process;

interrruptProcess: process
begin
interrupt_ack<=(others=>'0');
interrupt_rst<=(others=>'0');
wait until interrupt/="0000000000";
wait until rising_edge(clk);
interrupt_ack<=interrupt;
wait until rising_edge(clk);
interrupt_rst<=interrupt;
wait until rising_edge(clk);
interrupt_rst<=(others=>'0');

end process;

end Behavioral;
