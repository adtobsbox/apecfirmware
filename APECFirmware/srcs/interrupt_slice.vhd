----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/07/2022 04:40:48 PM
-- Design Name: 
-- Module Name: interrupt_slice - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity interrupt_slice is
    Port ( interrupt_in : in STD_LOGIC_VECTOR (9 downto 0);
           interrupt0 : out STD_LOGIC;
           interrupt1 : out STD_LOGIC;
           interrupt2 : out STD_LOGIC;
           interrupt3 : out STD_LOGIC;
           interrupt4 : out STD_LOGIC;
           interrupt5 : out STD_LOGIC;
           interrupt6 : out STD_LOGIC;
           interrupt7 : out STD_LOGIC;
           interrupt8 : out STD_LOGIC;
           interrupt9 : out STD_LOGIC);
end interrupt_slice;

architecture Behavioral of interrupt_slice is

begin
interrupt0<=interrupt_in(0);
interrupt1<=interrupt_in(1);
interrupt2<=interrupt_in(2);
interrupt3<=interrupt_in(3);
interrupt4<=interrupt_in(4);
interrupt5<=interrupt_in(5);
interrupt6<=interrupt_in(6);
interrupt7<=interrupt_in(7);
interrupt8<=interrupt_in(8);
interrupt9<=interrupt_in(9);
end Behavioral;
