----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/07/2020 04:46:32 PM
-- Design Name: 
-- Module Name: delay - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_generic is
  Generic (
        CLK_DELAYS : integer := 2;
        BUS_WIDTH : integer := 64
        );
    Port ( data_in : in STD_LOGIC_VECTOR (BUS_WIDTH-1 downto 0);
           data_out : out STD_LOGIC_VECTOR (BUS_WIDTH-1 downto 0);
           rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC);
end delay_generic;

architecture Behavioral of delay_generic is

type data_type is array (CLK_DELAYS  downto 0) of std_logic_vector(BUS_WIDTH-1 downto 0);
signal data : data_type;

begin

process(rx_usrclk)
begin
if rising_edge(rx_usrclk) then
  if reset_in='0' then
    data <= data(data'high - 1 downto data'low) & data_in;
    data_out <= data(data'high);
    
  else
    data<=(others=>(others=>'0'));
    data_out<=(others=>'0');
  end if;

end if;

end process;


end Behavioral;
