----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/08/2020 07:15:23 PM
-- Design Name: 
-- Module Name: aligner - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity aligner is
    Port ( rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           valid_frame_in : in STD_LOGIC_VECTOR (7 downto 0);
           valid_frame_out : out STD_LOGIC_VECTOR (7 downto 0);
           rx_data_in : in STD_LOGIC_VECTOR (63 downto 0);
           data_avail_in : in STD_LOGIC;
           rx_data_out : out STD_LOGIC_VECTOR (63 downto 0);
           data_avail_out : out STD_LOGIC;
           end_of_frame_out : out STD_LOGIC;
           start_of_frame_out : out STD_LOGIC);
end aligner;

architecture Behavioral of aligner is

signal valid_frame_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal valid_frame_out_s : STD_LOGIC_VECTOR(7 downto 0);

signal valid_bytes_in_buffer : STD_LOGIC_VECTOR(7 downto 0);

signal rx_data_in_s : STD_LOGIC_VECTOR(63 downto 0);
signal data_avail_in_s: STD_LOGIC;
signal rx_data_out_s : STD_LOGIC_VECTOR(63 downto 0);
signal data_avail_out_s : STD_LOGIC;
signal end_of_frame_out_s : STD_LOGIC;
signal end_of_frame_in_buffer_s : STD_LOGIC;
signal start_of_frame_out_s : STD_LOGIC;
signal first :STD_LOGIC;

signal rx_data_out_buffer_s : STD_LOGIC_VECTOR(63 downto 0);

subtype offsetType is integer range 0 to 8;
signal offset:offsetType:=0;

type alignStateType is (idle,frame);
signal alignState : alignStateType:=idle;

begin
inputProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			valid_frame_in_s<=valid_frame_in;
			valid_frame_out<=valid_frame_out_s;
			rx_data_in_s<=rx_data_in;
			data_avail_in_s<=data_avail_in;
			rx_data_out<=rx_data_out_s;
			data_avail_out<=data_avail_out_s;
			end_of_frame_out<=end_of_frame_out_s;
			start_of_frame_out<=start_of_frame_out_s;
		else
			valid_frame_in_s<=(others=>'0');
			valid_frame_out<=(others=>'0');
			rx_data_in_s<=(others=>'0');
			data_avail_in_s<='0';
			rx_data_out<=(others=>'0');
			data_avail_out<='0';
			end_of_frame_out<='0';
			start_of_frame_out<='0';
		end if;
	end if;

end process;

alignProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
	
				case(alignState) is
					when idle=>
						end_of_frame_out_s<=end_of_frame_in_buffer_s;
						end_of_frame_in_buffer_s<='0';
						rx_data_out_s<=rx_data_out_buffer_s;
						valid_frame_out_s<=valid_bytes_in_buffer;
						valid_bytes_in_buffer<=(others=>'0');
						first<='1';

						if valid_bytes_in_buffer/="00000000" then
							data_avail_out_s<='1';
						else
							data_avail_out_s<='0';
						end if;
						--state7
						if valid_frame_in_s(7 downto 0)="11111111" then
							rx_data_out_buffer_s<=rx_data_in_s;
							offset<=0;
							alignState<=frame;
						--state6
						elsif valid_frame_in_s(7 downto 0)="01111111" then
							rx_data_out_buffer_s(63 downto 8)<=rx_data_in_s(55 downto 0);
							offset<=1;
							alignState<=frame;
						--state5
						elsif valid_frame_in_s(7 downto 0)="00111111" then
							rx_data_out_buffer_s(63 downto 16)<=rx_data_in_s(47 downto 0);
							offset<=2;
							alignState<=frame;
						--state4
						elsif valid_frame_in_s(7 downto 0)="00011111" then
							rx_data_out_buffer_s(63 downto 24)<=rx_data_in_s(39 downto 0);
							offset<=3;
							alignState<=frame;
						--state3
						elsif valid_frame_in_s(7 downto 0)="00001111" then
							rx_data_out_buffer_s(63 downto 32)<=rx_data_in_s(31 downto 0);
							offset<=4;
							alignState<=frame;
						--state2
						elsif valid_frame_in_s(7 downto 0)="00000111" then
							rx_data_out_buffer_s(63 downto 40)<=rx_data_in_s(23 downto 0);
							offset<=5;
							alignState<=frame;
						--state1
						elsif valid_frame_in_s(7 downto 0)="00000011" then
							rx_data_out_buffer_s(63 downto 48)<=rx_data_in_s(15 downto 0);
							offset<=6;
							alignState<=frame;
						--state0
						elsif valid_frame_in_s(7 downto 0)="00000001" then
							rx_data_out_buffer_s(63 downto 56)<=rx_data_in_s(7 downto 0);
							offset<=7;
							alignState<=frame;
						end if;	
					when frame=>
						data_avail_out_s<=data_avail_in_s;
						if data_avail_in_s='1' then
							if valid_frame_in_s/="11111111" then
								alignState<=idle;
							else
								valid_frame_out_s<=(others=>'1');
								end_of_frame_out_s<='0';
								end_of_frame_in_buffer_s<='0';
							end if;
							if first='1' then
								start_of_frame_out_s<='1';
								first<='0';
							else
								start_of_frame_out_s<='0';
							end if;
							if offset=0 then
								rx_data_out_buffer_s<=rx_data_in_s;
								rx_data_out_s<=rx_data_out_buffer_s;
								valid_frame_out_s<="11111111";
								valid_bytes_in_buffer<=valid_frame_in_s;
								if valid_frame_in_s="00000000" then
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111111" then
									null;
								else
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=1 then
								rx_data_out_buffer_s(63 downto 8)<=rx_data_in_s(55 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 8) & rx_data_in_s(63 downto 56);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';							
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="10000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11100000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11110000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11111000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11111100";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=2 then
								rx_data_out_buffer_s(63 downto 16)<=rx_data_in_s(47 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 16) & rx_data_in_s(63 downto 48);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="11111100";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="10000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11100000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11110000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11111000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=3 then
								rx_data_out_buffer_s(63 downto 24)<=rx_data_in_s(39 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 24) & rx_data_in_s(63 downto 40);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="11111000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11111100";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="10000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11100000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11110000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=4 then
								rx_data_out_buffer_s(63 downto 32)<=rx_data_in_s(31 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 32) & rx_data_in_s(63 downto 32);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="11110000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';					
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11111000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11111100";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="10000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11100000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=5 then
								rx_data_out_buffer_s(63 downto 40)<=rx_data_in_s(23 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 40) & rx_data_in_s(63 downto 24);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="11100000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11110000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11111000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11111100";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="10000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="11000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=6 then
								rx_data_out_buffer_s(63 downto 48)<=rx_data_in_s(15 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 48) & rx_data_in_s(63 downto 16);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="11000000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11100000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11110000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11111000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111100";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="10000000";
									end_of_frame_out_s<='0';
									end_of_frame_in_buffer_s<='1';
								end if;
							elsif offset=7 then
								rx_data_out_buffer_s(63 downto 56)<=rx_data_in_s(7 downto 0);
								rx_data_out_s<=rx_data_out_buffer_s(63 downto 56) & rx_data_in_s(63 downto 8);
								if valid_frame_in_s="00000000" then
									valid_frame_out_s<="10000000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';						
								elsif valid_frame_in_s="10000000" then
									valid_frame_out_s<="11000000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11000000" then
									valid_frame_out_s<="11100000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11100000" then
									valid_frame_out_s<="11110000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11110000" then
									valid_frame_out_s<="11111000";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111000" then
									valid_frame_out_s<="11111100";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111100" then
									valid_frame_out_s<="11111110";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								elsif valid_frame_in_s="11111110" then
									valid_frame_out_s<="11111111";
									valid_bytes_in_buffer<="00000000";
									end_of_frame_out_s<='1';
									end_of_frame_in_buffer_s<='0';
								end if;
							end if;
					end if;
				end case;
		else
			rx_data_out_s<=(others=>'0');
			rx_data_out_buffer_s<=(others=>'0');
			offset<=0;
			valid_bytes_in_buffer<=(others=>'0');
			alignState<=idle;
			valid_frame_out_s<=(others=>'0');
			end_of_frame_out_s<='0';
			end_of_frame_in_buffer_s<='0';
			start_of_frame_out_s<='0';
			first<='0';

		end if;
	end if;

end process;


end Behavioral;
