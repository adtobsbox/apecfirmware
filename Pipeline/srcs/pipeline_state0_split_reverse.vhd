----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/12/2020 12:35:24 PM
-- Design Name: 
-- Module Name: pipeline_state0_split_reverse - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_state0_split_reverse is
    Port ( rx_data_in : in STD_LOGIC_VECTOR (103 downto 0);
           rxdata_out : out STD_LOGIC_VECTOR (63 downto 0);
           rxdisperr_out : out STD_LOGIC_VECTOR (7 downto 0);
           rxnotintable_out : out STD_LOGIC_VECTOR (7 downto 0);
           rxchariscomma_out : out STD_LOGIC_VECTOR (7 downto 0);
           rxcharisk_out : out STD_LOGIC_VECTOR (7 downto 0);
           rxbufstat_out : out STD_LOGIC_VECTOR (2 downto 0);
           rxbyteisaligned_out : out STD_LOGIC;
           rxbyterealign_out : out STD_LOGIC;
           rxcommadet_out : out STD_LOGIC);
end pipeline_state0_split_reverse;

architecture Behavioral of pipeline_state0_split_reverse is

  function reverse_bytes (a : in std_logic_vector)
    return std_logic_vector is
    variable result : std_logic_vector(63 downto 0);
  begin
    result(63 downto 56) := a(47 downto 40);
    result(55 downto 48) := a(55 downto 48);
    result(47 downto 40) := a(63 downto 56);
    result(39 downto 32) := a(71 downto 64);
    result(31 downto 24) := a(79 downto 72);
    result(23 downto 16) := a(87 downto 80);
    result(15 downto 8)  := a(95 downto 88);
    result(7 downto 0)   := a(103 downto 96);
    return result;
  end; -- function reverse_any_vector
  
    function reverse_any_vector (a : in std_logic_vector)
    return std_logic_vector is
    variable result : std_logic_vector(a'RANGE);
    alias aa        : std_logic_vector(a'REVERSE_RANGE) is a;
  begin
    for i in aa'RANGE loop
      result(i) := aa(i);
    end loop;
    return result;
  end; -- function reverse_any_vector

begin

splitprocess: process(rx_data_in) 
begin
 rxdata_out         <= reverse_bytes(rx_data_in(103 downto 40));
 rxdisperr_out  <= reverse_any_vector(rx_data_in(39 downto 32));
  rxnotintable_out  <= reverse_any_vector(rx_data_in(31 downto 24));
rxchariscomma_out  <= reverse_any_vector(rx_data_in(23 downto 16));
rxcharisk_out      <= reverse_any_vector(rx_data_in(15 downto 8));
rxbufstat_out<= rx_data_in(7 downto 5);
rxbyteisaligned_out<=rx_data_in(4);
rxbyterealign_out<=rx_data_in(3);
rxcommadet_out<=rx_data_in(2);


end process;


end Behavioral;
