----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Martin Soderen
-- 
-- Create Date: 05/09/2019 01:57:43 PM
-- Design Name: 
-- Module Name: streamConcat - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--  The purpose of this core is to remove unwanted bytes from a stream. This is done by a pipeline with 8 stages where each is capable of shifting the data one step
--Example: data_in: x7 x6 x5 x4 x3 x2 x1 x0 data_keep: 01010101 (counter=0)
--after stage0 :    x6 x5 x4 x3 x2 x1 x0 data_keep: 1010101 (counter=0)
--after stage1 :    x6 x5 x4 x3 x2 x1 x0 data_keep: 010101 (counter=1)
--after stage2 :    x6 x4 x3 x2 x1 x0 data_keep: 10101 (counter=1)
--after stage3 :    x6 x4 x3 x2 x1 x0 data_keep: 0101 (counter=2)
--after stage4 :    x6 x4 x2 x1 x0 data_keep: 101 (counter=2)
--after stage5 :    x6 x4 x2 x1 x0 data_keep: 01 (counter=3)
--after stage6 :    x6 x4 x2 x0 data_keep: 1 (counter=3)
--after stage7 :    x6 x4 x2 x0 data_keep: 0 (counter=4)

--So there is a 8 clockcycle delay through the pipeline

----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_concat is
	generic (width :    positive := 8);
	Port ( data_in : in STD_LOGIC_VECTOR (8*width-1 downto 0);
		data_keep  : in  STD_LOGIC_VECTOR (7 downto 0);
		data_out   : out STD_LOGIC_VECTOR (8*width-1 downto 0);
		data_avail : out STD_LOGIC;
		clk        : in  STD_LOGIC;
		reset      : in  std_logic);
end pipeline_concat;

architecture Behavioral of pipeline_concat is
	--own types
	type pipeline_array is array(7 downto 0) of std_logic_vector(width- 1 downto 0);
	subtype byte_counter_type is integer range 0 to 8;

	component pipeline_concat_sub is
		generic (width :    positive := 8);
		port ( data_in : in std_logic_vector( width*8 - 1 downto 0);
			keep_in     : in  std_logic_vector(7 downto 0);
			counter_in  : in  integer range 0 to 8;
			clk         : in  std_logic;
			reset       : in  std_logic;
			data_out    : out std_logic_vector(width*8 - 1 downto 0);
			keep_out    : out std_logic_vector(7 downto 0);
			counter_out : out integer range 0 to 8);
	end component;

	--i/o registers
	signal data_in_s    : STD_LOGIC_VECTOR (8*width-1 downto 0);
	signal data_keep_s  : STD_LOGIC_VECTOR (7 downto 0);
	signal data_out_s   : pipeline_array;
	signal data_avail_s : STD_LOGIC;

	--stage0 
	signal shift0   : std_logic_vector( width*8 - 1 downto 0);
	signal keep0    : std_logic_vector(7 downto 0);
	signal counter0 : byte_counter_type := 0;

	--stage1
	signal shift1   : std_logic_vector( width*8 - 1 downto 0);
	signal keep1    : std_logic_vector(7 downto 0);
	signal counter1 : byte_counter_type := 0;
	--stage2
	signal shift2   : std_logic_vector( width*8 - 1 downto 0);
	signal keep2    : std_logic_vector(7 downto 0);
	signal counter2 : byte_counter_type := 0;

	--stage3
	signal shift3   : std_logic_vector( width*8 - 1 downto 0);
	signal keep3    : std_logic_vector(7 downto 0);
	signal counter3 : byte_counter_type := 0;

	--stage4
	signal shift4   : std_logic_vector( width*8 - 1 downto 0);
	signal keep4    : std_logic_vector(7 downto 0);
	signal counter4 : byte_counter_type := 0;

	--stage5
	signal shift5   : std_logic_vector( width*8 - 1 downto 0);
	signal keep5    : std_logic_vector(7 downto 0);
	signal counter5 : byte_counter_type := 0;

	--stage6
	signal shift6   : std_logic_vector( width*8 - 1 downto 0);
	signal keep6    : std_logic_vector(7 downto 0);
	signal counter6 : byte_counter_type := 0;

	--stage7
	signal shift7_out   : std_logic_vector( width*8 - 1 downto 0);
	signal keep7_out    : std_logic_vector(7 downto 0);
	signal counter7_out : byte_counter_type := 0;

	signal shift7   : pipeline_array;
	signal keep7    : std_logic_vector(7 downto 0);
	signal counter7 : byte_counter_type := 0;

	signal temp_data    : pipeline_array;
	signal temp_counter : byte_counter_type := 0;

begin
		stage1 : pipeline_concat_sub generic map (width => width) port map (data_in => shift0, keep_in => keep0, counter_in => counter0,clk => clk,reset => reset,data_out => shift1,keep_out => keep1,counter_out => counter1);
		stage2 : pipeline_concat_sub generic map (width => width) port map (data_in => shift1, keep_in => keep1, counter_in => counter1,clk => clk,reset => reset,data_out => shift2,keep_out => keep2,counter_out => counter2);
		stage3 : pipeline_concat_sub generic map (width => width) port map (data_in => shift2, keep_in => keep2, counter_in => counter2,clk => clk,reset => reset,data_out => shift3,keep_out => keep3,counter_out => counter3);
		stage4 : pipeline_concat_sub generic map (width => width) port map (data_in => shift3, keep_in => keep3, counter_in => counter3,clk => clk,reset => reset,data_out => shift4,keep_out => keep4,counter_out => counter4);
		stage5 : pipeline_concat_sub generic map (width => width) port map (data_in => shift4, keep_in => keep4, counter_in => counter4,clk => clk,reset => reset,data_out => shift5,keep_out => keep5,counter_out => counter5);
		stage6 : pipeline_concat_sub generic map (width => width) port map (data_in => shift5, keep_in => keep5, counter_in => counter5,clk => clk,reset => reset,data_out => shift6,keep_out => keep6,counter_out => counter6);
		stage7 : pipeline_concat_sub generic map (width => width) port map (data_in => shift6, keep_in => keep6, counter_in => counter6,clk => clk,reset => reset,data_out => shift7_out,keep_out => keep7_out,counter_out => counter7_out);


	ioprocess : process(clk)
	begin
		if rising_edge(clk) then
			if reset='0' then
				data_in_s   <= data_in;
				data_keep_s <= data_keep;
				data_avail  <= data_avail_s;
				for I in 7 downto 0 loop
					data_out((I+1)*width-1 downto I*width) <= data_out_s(I);
				end loop;
				for I in 7 downto 0 loop
					shift7(I) <= shift7_out((I+1)*width-1 downto I*width);
					keep7     <= keep7_out;
					counter7  <= counter7_out;
				end loop;
			else
				data_in_s   <= (others => '0');
				data_keep_s <= (others => '0');
				data_avail  <= '0';
				data_avail  <= data_avail_s;
				for I in 7 downto 0 loop
					data_out((I+1)*width-1 downto I*width) <= (others => '0');
				end loop;
			end if;
		end if;

	end process;

	--removes any unwanted bytes from pos 7
	stage0 : process(clk)
	begin
		if rising_edge(clk) then
			if reset='0' then
				--skip first byte
				if data_keep_s(7)='0' then
					for I in 6 downto 0 loop
						shift0((I+2)*width-1 downto (I+1)*width) <= data_in_s((I+1)*width-1 downto I*width );
					end loop;
					shift0(width-1 downto 0) <= (others => '0');
					counter0                 <= 0;
				else
					for I in 7 downto 0 loop
						shift0((I+1)*width-1 downto I*width) <= data_in_s((I+1)*width-1 downto I*width);
					end loop;
					counter0 <= 1;
				end if;
				keep0 <= data_keep_s(6 downto 0)&'0';
			else
				for I in 7 downto 0 loop
					shift0((I+1)*width-1 downto I*width) <= (others => '0');
				end loop;
				keep0    <= (others => '0');
				counter0 <= 0;
			end if;
		end if;

	end process;

	bufferProcess : process(clk)
	begin
		if rising_edge(clk) then
			if reset='0' then
				--we have 8 or more bytes
				if (temp_counter + counter7)>=8 then
					data_avail_s <= '1';
					temp_counter <= (temp_counter + counter7)-8;
					case temp_counter is
						--we take one byte from temp_data and 7 bytes from shift7
						when 0 =>
							for I in 7 downto 0 loop
								data_out_s(I) <= shift7(I);
							end loop;
						when 1 =>
							for I in 7 downto 7 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-7);
							end loop;
							for I in 6 downto 0 loop
								data_out_s(I) <= shift7(I+1);
							end loop;
						--we take two bytes from temp_data and 6 bytes from shift7
						when 2 =>
							for I in 7 downto 6 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-6);
							end loop;
							for I in 5 downto 0 loop
								data_out_s(I) <= shift7(I+2);
							end loop;
						when 3 =>
							for I in 7 downto 5 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-5);
							end loop;
							for I in 4 downto 0 loop
								data_out_s(I) <= shift7(I+3);
							end loop;
						when 4 =>
							for I in 7 downto 4 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-4);
							end loop;
							for I in 3 downto 0 loop
								data_out_s(I) <= shift7(I+4);
							end loop;
						when 5 =>
							for I in 7 downto 3 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-3);
							end loop;
							for I in 2 downto 0 loop
								data_out_s(I) <= shift7(I+5);
							end loop;
						when 6 =>
							for I in 7 downto 2 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-2);
							end loop;
							for I in 1 downto 0 loop
								data_out_s(I) <= shift7(I+6);
							end loop;
						when 7 =>
							for I in 7 downto 1 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I-1);
							end loop;
							for I in 0 downto 0 loop
								data_out_s(I) <= shift7(I+7);
							end loop;
						when 8 =>
							for I in 7 downto 0 loop
								data_out_s(I) <= temp_data(I);
								temp_data(I)  <= shift7(I);
							end loop;
						when others =>
							null;
					end case;
				else
					data_avail_s <= '0';
					temp_counter <= temp_counter+counter7;
					case temp_counter is
						when 0 =>
							for I in 7 downto 0 loop
								temp_data(I) <= shift7(I);
							end loop;
						when 1 =>
							for I in 6 downto 0 loop
								temp_data(I) <= shift7(I+1);
							end loop;
						when 2 =>
							for I in 5 downto 0 loop
								temp_data(I) <= shift7(I+2);
							end loop;
						when 3 =>
							for I in 4 downto 0 loop
								temp_data(I) <= shift7(I+3);
							end loop;
						when 4 =>
							for I in 3 downto 0 loop
								temp_data(I) <= shift7(I+4);
							end loop;
						when 5 =>
							for I in 2 downto 0 loop
								temp_data(I) <= shift7(I+5);
							end loop;
						when 6 =>
							for I in 1 downto 0 loop
								temp_data(I) <= shift7(I+6);
							end loop;
						when others =>
							null;
					end case;
				end if;
			else
				temp_counter <= 0;
				data_avail_s <= '0';
				for I in 7 downto 0 loop
					data_out_s(I) <= (others => '0');
				end loop;

			end if;

		end if;

	end process;

end Behavioral;
