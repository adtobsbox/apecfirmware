----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/08/2020 11:15:34 AM
-- Design Name: 
-- Module Name: valid_frame - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity valid_frame is
    Port ( three_commas_found_in : in STD_LOGIC_VECTOR (7 downto 0);
           header_found_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           valid_frame_out : out STD_LOGIC_VECTOR(7 downto 0)
           );
end valid_frame;

architecture Behavioral of valid_frame is
type validFrameStateType is (idle,frame);
signal validFrameState : validFrameStateType:=idle;

signal three_commas_found_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal header_found_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal valid_frame_out_s : STD_LOGIC_VECTOR(7 downto 0);

begin

inputProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			three_commas_found_in_s<=three_commas_found_in;
			header_found_in_s<=header_found_in;
			valid_frame_out<=valid_frame_out_s;
		else
			three_commas_found_in_s<=(others=>'0');
			header_found_in_s<=(others=>'0');
			valid_frame_out<=(others=>'0');
		end if;
	end if;

end process;


validFrameProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			case (validFrameState) is
				when idle=>
					if header_found_in_s(7)='1' then
						valid_frame_out_s<="11111111";
						validFrameState<=frame;
					elsif header_found_in_s(6)='1' then
						valid_frame_out_s<="01111111";
						validFrameState<=frame;
					elsif header_found_in_s(5)='1' then
						valid_frame_out_s<="00111111";
						validFrameState<=frame;
					elsif header_found_in_s(4)='1' then
						valid_frame_out_s<="00011111";
						validFrameState<=frame;
					elsif header_found_in_s(3)='1' then
						valid_frame_out_s<="00001111";
						validFrameState<=frame;
					elsif header_found_in_s(2)='1' then
						valid_frame_out_s<="00000111";
						validFrameState<=frame;
					elsif header_found_in_s(1)='1' then
						valid_frame_out_s<="00000011";
						validFrameState<=frame;
					elsif header_found_in_s(0)='1' then
						valid_frame_out_s<="00000001";
						validFrameState<=frame;
					else
						valid_frame_out_s<=(others=>'0');
					end if;
				when frame=>
					if three_commas_found_in_s(7)='1' then
						valid_frame_out_s<="00000000";
						validFrameState<=idle;
					elsif three_commas_found_in_s(6)='1' then
						valid_frame_out_s<="10000000";
						validFrameState<=idle;
					elsif three_commas_found_in_s(5)='1' then
						valid_frame_out_s<="11000000";
						validFrameState<=idle;
					elsif three_commas_found_in_s(4)='1' then
						valid_frame_out_s<="11100000";
						validFrameState<=idle;
					elsif three_commas_found_in_s(3)='1' then
						valid_frame_out_s<="11110000";
						validFrameState<=idle;
					elsif three_commas_found_in_s(2)='1' then
						valid_frame_out_s<="11111000";
						validFrameState<=idle;
					elsif three_commas_found_in_s(1)='1' then
						valid_frame_out_s<="11111100";
						validFrameState<=idle;
					elsif three_commas_found_in_s(0)='1' then
						valid_frame_out_s<="11111110";
						validFrameState<=idle;
					else
						valid_frame_out_s<="11111111";
					end if;
				when others =>
					null;
			end case;

		else
			validFrameState<=idle;
		end if;
	end if;

end process;


end Behavioral;
