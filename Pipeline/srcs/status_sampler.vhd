----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/16/2020 05:49:28 PM
-- Design Name: 
-- Module Name: status_sampler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity status_sampler is
    Port ( rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           three_commas_found_in : in STD_LOGIC;
           header_found_in : in STD_LOGIC;
           rx_disperr_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_notintable_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_bufstat_in : in STD_LOGIC_VECTOR (2 downto 0);
           rx_byteisaligned_in : in STD_LOGIC;
           rx_byterealign : in STD_LOGIC;
           rx_commadet : in STD_LOGIC;
           header_error_corrected : in std_logic;
           status_out : out STD_LOGIC_VECTOR(31 downto 0)
           );
end status_sampler;

architecture Behavioral of status_sampler is

signal status_s : STD_LOGIC_VECTOR(31 downto 0);
   signal status_counter:integer:=0;
begin
  status_out<=status_s;

inputProcess : process(rx_usrclk)
begin

  if rising_edge(rx_usrclk) then
    if reset_in='0' then
        if status_counter<=50000000 then
            status_counter<=status_counter+1;
            status_s(0)<=status_s(0) or three_commas_found_in;
            status_s(1)<=status_s(1) or header_found_in;
            status_s(9 downto 2)<=status_s(9 downto 2) or rx_disperr_in;--disperr
            status_s(17 downto 10)<=status_s(17 downto 10) or rx_notintable_in;--notintable
            status_s(20 downto 18)<=status_s(20 downto 18) or rx_bufstat_in;--bufstat
            status_s(21)<=status_s(21) or rx_byteisaligned_in;
            status_s(22)<=status_s(22) or rx_byterealign;
            status_s(23)<=status_s(23) or rx_commadet;
            status_s(24)<=status_s(24) or header_error_corrected;
        else
           status_counter<=0;
           status_s(0)<= three_commas_found_in;
           status_s(1)<= header_found_in;
           status_s(9 downto 2)<= rx_disperr_in;--disperr
           status_s(17 downto 10)<= rx_notintable_in;--notintable
           status_s(20 downto 18)<= rx_bufstat_in;--bufstat
           status_s(21)<= rx_byteisaligned_in; --byt is aligned
           status_s(22)<= rx_byterealign; --byt is aligned
           status_s(23)<= rx_commadet; --byt is aligned
           status_s(24)<= header_error_corrected;
        end if;
        status_s(31 downto 25)<=(others=>'0');

    else
      status_s<=(others=>'0');
      status_counter<=0;
    end if;
  end if;

end process;

end Behavioral;
