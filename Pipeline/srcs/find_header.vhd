----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/08/2020 09:53:47 AM
-- Design Name: 
-- Module Name: find_header - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity find_header is
    Port ( rx_data_in : in STD_LOGIC_VECTOR (63 downto 0);
           rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           header_found_out : out STD_LOGIC_VECTOR (7 downto 0));
end find_header;

architecture Behavioral of find_header is
type header_type is array (3  downto 0) of std_logic_vector(7 downto 0);
constant HEADER : header_type :=("01100010","01001111","00110001","01110011");

type data_type is array (15  downto 0) of std_logic_vector(7 downto 0);
signal data : data_type;
signal header_found :std_logic_vector(15 downto 0);

begin

inputProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			data(15)<=data(7);
			data(14)<=data(6);
			data(13)<=data(5);
			data(12)<=data(4);
			data(11)<=data(3);
			data(10)<=data(2);
			data(9)<=data(1);
			data(8)<=data(0);
			data(7)<=rx_data_in(63 downto 56);
			data(6)<=rx_data_in(55 downto 48);
			data(5)<=rx_data_in(47 downto 40);
			data(4)<=rx_data_in(39 downto 32);
			data(3)<=rx_data_in(31 downto 24);
			data(2)<=rx_data_in(23 downto 16);
			data(1)<=rx_data_in(15 downto 8);
			data(0)<=rx_data_in(7 downto 0);
			header_found_out<=header_found(15 downto 8);
		else
			data<=(others=>(others=>'0'));
		end if;
	end if;

end process;

findHeaderProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			if (data(15) = HEADER(3) and data(14)=HEADER(2) and data(13)=HEADER(1) and data(12)=HEADER(0)) or 
								header_found(7)='1' then
				header_found(15)<='1';
			else
			 	header_found(15)<='0';
			end if; 
			
			if (data(15) = HEADER(3) and data(14)=HEADER(2) and data(13)=HEADER(1) and data(12)=HEADER(0)) or 
								(data(14) = HEADER(3) and data(13)=HEADER(2) and data(12)=HEADER(1) and data(11)=HEADER(0)) or
								header_found(6)='1' then
				header_found(14)<='1';
			else
			 	header_found(14)<='0';
			 end if; 
		
			if (data(15) = HEADER(3) and data(14)=HEADER(2) and data(13)=HEADER(1) and data(12)=HEADER(0)) or 
								(data(14) = HEADER(3) and data(13)=HEADER(2) and data(12)=HEADER(1) and data(11)=HEADER(0)) or
								(data(13) = HEADER(3) and data(12)=HEADER(2) and data(11)=HEADER(1) and data(10)=HEADER(0)) or
								header_found(5)='1' then
				header_found(13)<='1';
			else
			 	header_found(13)<='0';
			end if; 
			
			if (data(15) = HEADER(3) and data(14)=HEADER(2) and data(13)=HEADER(1) and data(12)=HEADER(0)) or 
								(data(14) = HEADER(3) and data(13)=HEADER(2) and data(12)=HEADER(1) and data(11)=HEADER(0)) or
								(data(13) = HEADER(3) and data(12)=HEADER(2) and data(11)=HEADER(1) and data(10)=HEADER(0)) or
								(data(12) = HEADER(3) and data(11)=HEADER(2) and data(10)=HEADER(1) and data(9)=HEADER(0)) then
				header_found(12)<='1';
			else
				header_found(12)<='0';
			end if;

			if (data(14) = HEADER(3) and data(13)=HEADER(2) and data(12)=HEADER(1) and data(11)=HEADER(0)) or 
								(data(13) = HEADER(3) and data(12)=HEADER(2) and data(11)=HEADER(1) and data(10)=HEADER(0)) or
								(data(12) = HEADER(3) and data(11)=HEADER(2) and data(10)=HEADER(1) and data(9)=HEADER(0)) or
								(data(11) = HEADER(3) and data(10)=HEADER(2) and data(9)=HEADER(1) and data(8)=HEADER(0)) then
				header_found(11)<='1';
			else
				header_found(11)<='0';
			end if;
			
			if (data(13) = HEADER(3) and data(12)=HEADER(2) and data(11)=HEADER(1) and data(10)=HEADER(0)) or 
								(data(12) = HEADER(3) and data(11)=HEADER(2) and data(10)=HEADER(1) and data(9)=HEADER(0)) or
								(data(11) = HEADER(3) and data(10)=HEADER(2) and data(9)=HEADER(1) and data(8)=HEADER(0)) or
								(data(10) = HEADER(3) and data(9)=HEADER(2) and data(8)=HEADER(1) and data(7)=HEADER(0)) then
				header_found(10)<='1';
			else
				header_found(10)<='0';
			end if;

			
			if (data(12) = HEADER(3) and data(11)=HEADER(2) and data(10)=HEADER(1) and data(9)=HEADER(0)) or 
								(data(11) = HEADER(3) and data(10)=HEADER(2) and data(9)=HEADER(1) and data(8)=HEADER(0)) or
								(data(10) = HEADER(3) and data(9)=HEADER(2) and data(8)=HEADER(1) and data(7)=HEADER(0)) or
								(data(9) = HEADER(3) and data(8)=HEADER(2) and data(7)=HEADER(1) and data(6)=HEADER(0)) then
				header_found(9)<='1';
			else
				header_found(9)<='0';
			end if;					
			
			if (data(11) = HEADER(3) and data(10)=HEADER(2) and data(9)=HEADER(1) and data(8)=HEADER(0)) or 
								(data(10) = HEADER(3) and data(9)=HEADER(2) and data(8)=HEADER(1) and data(7)=HEADER(0)) or
								(data(9) = HEADER(3) and data(8)=HEADER(2) and data(7)=HEADER(1) and data(6)=HEADER(0)) or
								(data(8) = HEADER(3) and data(7)=HEADER(2) and data(6)=HEADER(1) and data(5)=HEADER(0)) then
				header_found(8)<='1';
			else
				header_found(8)<='0';
			end if;					
			
			if (data(10) = HEADER(3) and data(9)=HEADER(2) and data(8)=HEADER(1) and data(7)=HEADER(0)) or 
								(data(9) = HEADER(3) and data(8)=HEADER(2) and data(7)=HEADER(1) and data(6)=HEADER(0)) or
								(data(8) = HEADER(3) and data(7)=HEADER(2) and data(6)=HEADER(1) and data(5)=HEADER(0)) then
				header_found(7)<='1';
			else
				header_found(7)<='0';
			end if;					

			if (data(9) = HEADER(3) and data(8)=HEADER(2) and data(7)=HEADER(1) and data(6)=HEADER(0)) or 
								(data(8) = HEADER(3) and data(7)=HEADER(2) and data(6)=HEADER(1) and data(5)=HEADER(0)) then
				header_found(6)<='1';
			else
				header_found(6)<='0';
			end if;

	
			if (data(8) = HEADER(3) and data(7)=HEADER(2) and data(6)=HEADER(1) and data(5)=HEADER(0)) then
				header_found(5)<='1';
			else
				header_found(5)<='0';
			end if;
								 					 
		else
			header_found<=(others=>'0');
		end if;
	end if;

end process;


end Behavioral;
