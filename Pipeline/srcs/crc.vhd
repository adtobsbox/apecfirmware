----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/09/2020 04:12:53 PM
-- Design Name: 
-- Module Name: crc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity crc is
    Port ( rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           valid_frame_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_data_in : in STD_LOGIC_VECTOR (63 downto 0);
           data_avail_in : in STD_LOGIC;
           end_of_frame_in : in STD_LOGIC;
           start_of_frame_in : in STD_LOGIC;
           crc_ok_out : out STD_LOGIC;
           alignment_error : out STD_LOGIC;
           CRC_calc_out     : out    std_logic_Vector(31 downto 0) );
end crc;

architecture Behavioral of crc is

component CRC32GenD64 is
  port(
    Clk     : in     std_logic ;
    Clr_CRC : in     std_logic ;
    Din     : in     std_logic_Vector(63 downto 0) ;
    CRC     : out    std_logic_Vector(31 downto 0) ;
    Calc    : in     std_logic ) ;
end  component ;

signal valid_frame_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal rx_data_in_s : STD_LOGIC_VECTOR(63 downto 0);
signal data_avail_in_s : STD_LOGIC;
signal end_of_frame_in_s : STD_LOGIC;
signal start_of_frame_in_s : STD_LOGIC;
signal crc_ok_out_s : STD_LOGIC;
signal alignment_error_s : STD_LOGIC;
signal crc_calc_out_s     :     std_logic_Vector(31 downto 0) ;

signal crc_clr :      std_logic ;
signal crc_din     :      std_logic_Vector(63 downto 0) ;
signal crc_out     :     std_logic_Vector(31 downto 0) ;
signal crc_calc    :      std_logic; 
signal check : STD_LOGIC;

type crcStateType is (idle,calc);
signal crcState : crcStateType:=idle;

signal crc_checksum : STD_LOGIC_VECTOR(31 downto 0);
signal crc_checksum_calc : STD_LOGIC_VECTOR(31 downto 0);

    function reverse_any_vector (a : in std_logic_vector)
    return std_logic_vector is
    variable result : std_logic_vector(a'RANGE);
    alias aa        : std_logic_vector(a'REVERSE_RANGE) is a;
  begin
    for i in aa'RANGE loop
      result(i) := aa(i);
    end loop;
    return result;
  end; -- function reverse_any_vector

begin


crccalc: CRC32GenD64 
  port map(
    Clk     =>rx_usrclk,
    Clr_CRC =>crc_clr,
    Din     =>crc_din,
    CRC     =>crc_out,
    Calc    =>crc_calc) ;

inputProcess : process(rx_usrclk)
begin	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			valid_frame_in_s<=valid_frame_in;
			rx_data_in_s<=rx_data_in;
			data_avail_in_s<=data_avail_in;
			end_of_frame_in_s<=end_of_frame_in;
			start_of_frame_in_s<=start_of_frame_in;
			crc_ok_out<=crc_ok_out_s;
			alignment_error<=alignment_error_s;
			crc_calc_out<=crc_calc_out_s;
		else
			valid_frame_in_s<=(others=>'0');
			rx_data_in_s<=(others=>'0');
			data_avail_in_s<='0';
			end_of_frame_in_s<='0';
			start_of_frame_in_s<='0';
			crc_ok_out<='0';
			alignment_error<='0';
			crc_calc_out<=(others=>'0'); 
		end if;
	end if;

end process;

crcProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			if crc_checksum=crc_checksum_calc then
				crc_ok_out_s<='1';
			else
				crc_ok_out_s<='0';
			end if;
			crc_calc_out_s<= crc_checksum_calc(23 downto 16) & crc_checksum_calc(31 downto 24) & crc_checksum_calc(7 downto 0) & crc_checksum_calc(15 downto 8) ;
			case(crcState) is
				when idle=>
					if start_of_frame_in_s='1' then
						crc_clr<='0';
						crc_din(63 downto 56) <=reverse_any_vector(rx_data_in_s(63 downto 56));
						crc_din(55 downto 48)<= reverse_any_vector(rx_data_in_s(55 downto 48));
						crc_din(47 downto 40)<= reverse_any_vector(rx_data_in_s(47 downto 40));
						crc_din(39 downto 32)<= reverse_any_vector(rx_data_in_s(39 downto 32));
						crc_din(31 downto 24)<= reverse_any_vector(rx_data_in_s(31 downto 24));
						crc_din(23 downto 16)<= reverse_any_vector(rx_data_in_s(23 downto 16));
						crc_din(15 downto 8)<= reverse_any_vector(rx_data_in_s(15 downto 8));
						crc_din(7 downto 0)<= reverse_any_vector(rx_data_in_s(7 downto 0));
						crc_calc<='1';
						crcState<=calc;
					else
						crc_clr<='1';

					end if;
					if check='1' then
						crc_checksum_calc <=not reverse_any_vector(crc_out);
						check<='0';
					end if;
				when calc =>
					if data_avail_in_s='1' then
						if end_of_frame_in_s='0' then
							crc_clr<='0';
						crc_din(63 downto 56) <=reverse_any_vector(rx_data_in_s(63 downto 56));
						crc_din(55 downto 48)<= reverse_any_vector(rx_data_in_s(55 downto 48));
						crc_din(47 downto 40)<= reverse_any_vector(rx_data_in_s(47 downto 40));
						crc_din(39 downto 32)<= reverse_any_vector(rx_data_in_s(39 downto 32));
						crc_din(31 downto 24)<= reverse_any_vector(rx_data_in_s(31 downto 24));
						crc_din(23 downto 16)<= reverse_any_vector(rx_data_in_s(23 downto 16));
						crc_din(15 downto 8)<= reverse_any_vector(rx_data_in_s(15 downto 8));
						crc_din(7 downto 0)<= reverse_any_vector(rx_data_in_s(7 downto 0));
							crc_calc<='1';
							crcState<=calc;
						else
							check<='1';
							crcState<=idle;
							crc_clr<='1';
							crc_calc<='1';
							--last four bytes was the crc
							if valid_frame_in_s="11110000" then
								crc_checksum<= rx_data_in_s(55 downto 48) & rx_data_in_s(63 downto 56) & rx_data_in_s(39 downto 32) & rx_data_in_s(47 downto 40);
								alignment_error_s<='0';
							else
								alignment_error_s<='1';
							end if;

						end if;
					else
						crc_calc<='0';
					end if;

			end case;

		else
			crcState<=idle;
			crc_clr<='1';
			crc_din<=(others=>'0');
			crc_calc<='0';
			crc_checksum_calc<=(others=>'0');
			crc_checksum<=(others=>'1');
			alignment_error_s<='0';
			check<='0';
		end if;
	end if;

end process;


end Behavioral;
