----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/16/2020 01:31:03 PM
-- Design Name: 
-- Module Name: sampler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sampler is
    Port ( rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           data_avail_in : in STD_LOGIC;
           start_of_frame_in : in STD_LOGIC;
           end_of_frame_in : in STD_LOGIC;
           rx_disperr_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_notintable_in : in STD_LOGIC_VECTOR (7 downto 0);
           three_commas_found_in : in STD_LOGIC_VECTOR (7 downto 0);
           header_found_in : in STD_LOGIC_VECTOR (7 downto 0);

           rx_disperr_out : out STD_LOGIC_VECTOR (7 downto 0);
           rx_notintable_out : out STD_LOGIC_VECTOR (7 downto 0);
           three_commas_found_out : out STD_LOGIC;
           header_found_out : out STD_LOGIC );
end sampler;

architecture Behavioral of sampler is
signal data_avail_in_s :  STD_LOGIC;
signal start_of_frame_in_s :  STD_LOGIC;
signal end_of_frame_in_s :  STD_LOGIC;
signal rx_disperr_in_s :  STD_LOGIC_VECTOR (7 downto 0);
signal rx_notintable_in_s :  STD_LOGIC_VECTOR (7 downto 0);
signal three_commas_found_in_s :  STD_LOGIC_VECTOR (7 downto 0);
signal header_found_in_s :  STD_LOGIC_VECTOR (7 downto 0);

signal rx_disperr_out_s :  STD_LOGIC_VECTOR (7 downto 0);
signal rx_notintable_out_s :  STD_LOGIC_VECTOR (7 downto 0);
signal three_commas_found_out_s :  STD_LOGIC;
signal header_found_out_s :  STD_LOGIC;

signal rx_disperr_samp :  STD_LOGIC_VECTOR (7 downto 0);
signal rx_notintable_samp :  STD_LOGIC_VECTOR (7 downto 0);
signal three_commas_found_samp :  STD_LOGIC;
signal header_found_samp :  STD_LOGIC;


type samplerStateType is (idle,frame);
signal samplerState : samplerStateType:=idle;


begin
inputProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			data_avail_in_s<=data_avail_in;
			start_of_frame_in_s<=start_of_frame_in;
			end_of_frame_in_s<=end_of_frame_in;
			rx_disperr_in_s<=rx_disperr_in;
			rx_notintable_in_s<=rx_notintable_in;
			three_commas_found_in_s<=three_commas_found_in;
			header_found_in_s<=header_found_in;

			rx_disperr_out<=rx_disperr_out_s;
			rx_notintable_out<=rx_notintable_out_s;
			three_commas_found_out<=three_commas_found_out_s;
			header_found_out<=header_found_out_s;

		else
			data_avail_in_s<='0';
			start_of_frame_in_s<='0';
			end_of_frame_in_s<='0';
			rx_disperr_in_s<=(others=>'0');
			rx_notintable_in_s<=(others=>'0');
			three_commas_found_in_s<=(others=>'0');
			header_found_in_s<=(others=>'0');

			rx_disperr_out<=(others=>'0');
			rx_notintable_out<=(others=>'0');
			three_commas_found_out<='0';
			header_found_out<='0';
		end if;
	end if;

end process;


samplerProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			if data_avail_in_s='1' then
			case(samplerState) is
				when idle=>
					if start_of_frame_in_s='1' then
						rx_disperr_samp<=rx_disperr_in_s;
						rx_notintable_samp<=rx_notintable_in_s;
						if three_commas_found_in_s/="00000000" then
							three_commas_found_samp<='1';
						else
							three_commas_found_samp<='0';
						end if;
						if header_found_in_s/="00000000" then
							header_found_samp<='1';
						else
							header_found_samp<='0';
						end if;
						samplerState<=frame;
					end if;
				when frame=>
						rx_disperr_samp<=rx_disperr_in_s or rx_disperr_samp;
						rx_notintable_samp<=rx_notintable_in_s or rx_notintable_samp;
						if three_commas_found_in_s/="00000000" or three_commas_found_samp='1' then
							three_commas_found_samp<='1';
						else
							three_commas_found_samp<='0';
						end if;
						if header_found_in_s/="00000000" or header_found_samp='1' then
							header_found_samp<='1';
						else
							header_found_samp<='0';
						end if;
						if end_of_frame_in_s='1' then
							samplerState<=idle;
							rx_disperr_out_s<=rx_disperr_samp;
							rx_notintable_out_s<=rx_notintable_samp;
							three_commas_found_out_s<=three_commas_found_samp;
							header_found_out_s<=header_found_samp;

						end if;
				end case;

			end if;

		else
			rx_disperr_samp<=(others=>'0');
			rx_notintable_samp<=(others=>'0');
			three_commas_found_samp<='0';
			header_found_samp<='0';
			samplerState<=idle;
			rx_disperr_out_s<=(others=>'0');
			rx_notintable_out_s<=(others=>'0');
			three_commas_found_out_s<='0';
			header_found_out_s<='0';
		end if;
	end if;

end process;


end Behavioral;
