----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/16/2020 02:38:03 PM
-- Design Name: 
-- Module Name: sender - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sender is
    Port ( rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           rx_data_in : in STD_LOGIC_VECTOR (63 downto 0);
           data_avail_in : in STD_LOGIC;
           end_of_frame_in : in STD_LOGIC;
           start_of_frame_in : in STD_LOGIC;
           rx_disperr_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_notintable_in : in STD_LOGIC_VECTOR (7 downto 0);
           crc_ok_in : in STD_LOGIC;
           din_dataFIFO_out :out STD_LOGIC_VECTOR(271 DOWNTO 0);
           wr_en_dataFIFO_out :out  STD_LOGIC;
           crc_calc_in : in STD_LOGIC_VECTOR(31 downto 0);
           aligment_error_in : in STD_LOGIC);
end sender;

architecture Behavioral of sender is
signal rx_data_in_s :  STD_LOGIC_VECTOR (63 downto 0);
signal data_avail_in_s : STD_LOGIC;
signal end_of_frame_in_s :  STD_LOGIC;
signal start_of_frame_in_s :  STD_LOGIC;
signal rx_disperr_in_s :  STD_LOGIC_VECTOR (7 downto 0);
signal rx_notintable_in_s :  STD_LOGIC_VECTOR (7 downto 0);
signal crc_ok_in_s :  STD_LOGIC;
signal din_dataFIFO_out_s : STD_LOGIC_VECTOR(271 DOWNTO 0);
signal wr_en_dataFIFO_out_s :  STD_LOGIC;
signal crc_calc_in_s : STD_LOGIC_VECTOR(31 downto 0);
signal aligment_error_in_s : STD_LOGIC;
signal first_send : STD_LOGIC;

signal temp_data : STD_LOGIC_VECTOR(63 downto 0);

type senderStateType is (idle,first,second,third,fourth,last,recover);
signal senderState : senderStateType:=idle;
signal counter:unsigned(13 downto 0);
alias firstSlot : std_logic_vector(63 downto 0) is din_dataFIFO_out_s(271 downto 208);
alias secondSlot : std_logic_vector(63 downto 0) is din_dataFIFO_out_s(207 downto 144);
alias thirdSlot : std_logic_vector(63 downto 0) is din_dataFIFO_out_s(143 downto 80);
alias fourthSlot : std_logic_vector(63 downto 0) is din_dataFIFO_out_s(79 downto 16);
alias counterSlot : std_logic_vector(13 downto 0) is din_dataFIFO_out_s(15 downto 2);
alias frameSlot : std_logic_vector(1 downto 0) is din_dataFIFO_out_s(1 downto 0);

begin

inputProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			rx_data_in_s<=rx_data_in;
			data_avail_in_s<=data_avail_in;
			end_of_frame_in_s<=end_of_frame_in;
			start_of_frame_in_s<=start_of_frame_in;
			rx_disperr_in_s<=rx_disperr_in;
			rx_notintable_in_s<=rx_notintable_in;
			crc_ok_in_s<=crc_ok_in;
			din_dataFIFO_out<=din_dataFIFO_out_s;
			wr_en_dataFIFO_out<=wr_en_dataFIFO_out_s;
			crc_calc_in_s<=crc_calc_in;
			aligment_error_in_s<=aligment_error_in;
		else
			rx_data_in_s<=(others=>'0');
			data_avail_in_s<='0';
			end_of_frame_in_s<='0';
			start_of_frame_in_s<='0';
			rx_disperr_in_s<=(others=>'0');
			rx_notintable_in_s<=(others=>'0');
			crc_ok_in_s<='0';
			din_dataFIFO_out<=(others=>'0');
			wr_en_dataFIFO_out<='0';
			crc_calc_in_s<=(others=>'0');
			aligment_error_in_s<='0';
		end if;
	end if;

end process;

senderProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
		case(senderState) is
			when idle=>
				if start_of_frame_in_s='1' and data_avail_in_s='1'  then
					firstSlot<=rx_data_in_s;
					senderState<=second;
				end if;
				wr_en_dataFIFO_out_s<='0';
				first_send<='1';
			when first=>
				if  data_avail_in_s='1' and end_of_frame_in_s='0'  then
					firstSlot<=rx_data_in_s;
					secondSlot<=(others=>'0');
					thirdSlot<=(others=>'0');
					fourthSlot<=(others=>'0');
					senderState<=second;
					wr_en_dataFIFO_out_s<='0';
				elsif data_avail_in_s='1' and end_of_frame_in_s='1' then
					firstSlot<=rx_data_in_s;
					secondSlot<=(others=>'0');
					thirdSlot<=(others=>'0');
					fourthSlot<=(others=>'0');
					senderState<=last;
					wr_en_dataFIFO_out_s<='1';
					counterSlot<=STD_LOGIC_VECTOR(counter);
					counter<=counter+1;
			    else
				    wr_en_dataFIFO_out_s<='0';
				end if;

			when second=>
				if  data_avail_in_s='1' and end_of_frame_in_s='0'  then
					secondSlot<=rx_data_in_s;
					thirdSlot<=(others=>'0');
					fourthSlot<=(others=>'0');
					senderState<=third;
					wr_en_dataFIFO_out_s<='0';
				elsif data_avail_in_s='1' and end_of_frame_in_s='1' then
					secondSlot<=rx_data_in_s;
					thirdSlot<=(others=>'0');
					fourthSlot<=(others=>'0');
					senderState<=last;
					wr_en_dataFIFO_out_s<='1';
					counterSlot<=STD_LOGIC_VECTOR(counter);
					counter<=counter+1;
				    
				end if;

			when third=>
				if  data_avail_in_s='1' and end_of_frame_in_s='0'  then
					senderState<=fourth;
					wr_en_dataFIFO_out_s<='0';
					thirdSlot<=rx_data_in_s;
					fourthSlot<=(others=>'0');
				elsif data_avail_in_s='1' and end_of_frame_in_s='1' then
					thirdSlot<=rx_data_in_s;
					fourthSlot<=(others=>'0');
					senderState<=last;
					wr_en_dataFIFO_out_s<='1';
					counterSlot<=STD_LOGIC_VECTOR(counter);
					counter<=counter+1;
				end if;

			when fourth=>
				if  data_avail_in_s='1' and end_of_frame_in_s='0'  then
					senderState<=first;
					fourthSlot<=rx_data_in_s;
					wr_en_dataFIFO_out_s<='1';
					counterSlot<=STD_LOGIC_VECTOR(counter);
					counter<=counter+1;
					if first_send='1' then
						frameSlot<="10";
						first_send<='0';
					else
						frameSlot<="00";
					end if;
				elsif data_avail_in_s='1' and end_of_frame_in_s='1' then
					senderState<=last;
					fourthSlot<=rx_data_in_s;
					wr_en_dataFIFO_out_s<='1';
					counterSlot<=STD_LOGIC_VECTOR(counter);
					counter<=counter+1;
				end if;

			when last=>
				din_dataFIFO_out_s(271 downto 176)<="01000001"&"01010000"&"01000101"&"01000011"&crc_ok_in_s&crc_ok_in_s&crc_ok_in_s&crc_ok_in_s&crc_ok_in_s&crc_ok_in_s&crc_ok_in_s&crc_ok_in_s&rx_disperr_in_s&rx_notintable_in_s&aligment_error_in_s&aligment_error_in_s&aligment_error_in_s&aligment_error_in_s&aligment_error_in_s&aligment_error_in_s&aligment_error_in_s&aligment_error_in_s&crc_calc_in_s;
                din_dataFIFO_out_s(175 downto 16)<=(others=>'0');
                din_dataFIFO_out_s(15 downto 2)<=std_logic_vector(counter);
                din_dataFIFO_out_s(1 downto 0)<="01";
                wr_en_dataFIFO_out_s<='1';
                counter<=(others=>'0');
                first_send<='1';
				if data_avail_in_s='1' and start_of_frame_in_s='1' then
					temp_data<=rx_data_in_s;
					senderState<=recover;
				else
					senderState<=idle;
				end if;
			when recover=>
				firstSlot<=temp_data;
				wr_en_dataFIFO_out_s<='0';
				if data_avail_in_s='1' then
					secondSlot<=rx_data_in_s;
					senderState<=third;
				else
					senderState<=second;
				end if;
			
		end case;

		else
			counter<=(others=>'0');
			din_dataFIFO_out_s<=(others=>'0');
			wr_en_dataFIFO_out_s<='0';
			senderState<=idle;
			first_send<='1';
			temp_data<=(others=>'0');
		end if;
	end if;

end process;



end Behavioral;
