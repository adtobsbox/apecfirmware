----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/29/2020 03:33:05 PM
-- Design Name: 
-- Module Name: pipeline_find_three_commas - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity find_three_commas is
	Port (
		rx_data_in             : in  STD_LOGIC_VECTOR (63 downto 0);
		rx_chariscomma_in      : in  std_logic_vector(7 downto 0);
		rx_charisk_in      : in  std_logic_vector(7 downto 0);
		rx_usrclk              : in  std_logic;
		reset_in               : in  std_logic;
		three_commas_found_out : out std_logic_vector(7 downto 0)
	);
end find_three_commas;

architecture Behavioral of find_three_commas is


	--inputProcess
  signal rx_data_in_s             : std_logic_vector(127 downto 0);
  signal rx_chariscomma_in_s      : std_logic_vector(15 downto 0);
  signal rx_charisk_in_s      : std_logic_vector(15 downto 0);
  signal three_commas             : std_logic_vector(15 downto 0);

	--findThreeCommasProcess
		alias data15 is rx_data_in_s(127 downto 120);--oldest
        alias data14 is rx_data_in_s(119 downto 112);
        alias data13 is rx_data_in_s(111 downto 104);
        alias data12 is rx_data_in_s(103 downto 96);
      
        alias data11 is rx_data_in_s(95 downto 88);
        alias data10 is rx_data_in_s(87 downto 80);
        alias data9 is rx_data_in_s(79 downto 72);
        alias data8 is rx_data_in_s(71 downto 64); --newest
	
	    alias data7 is rx_data_in_s(63 downto 56);--oldest
        alias data6 is rx_data_in_s(55 downto 48);
        alias data5 is rx_data_in_s(47 downto 40);
        alias data4 is rx_data_in_s(39 downto 32);
      
        alias data3 is rx_data_in_s(31 downto 24);
        alias data2 is rx_data_in_s(23 downto 16);
        alias data1 is rx_data_in_s(15 downto 8);
        alias data0 is rx_data_in_s(7 downto 0); --newest


    constant CARRIER_EXTEND : std_Logic_vector(7 downto 0) :="11110111";
begin

	inputProcess : process(rx_usrclk)
	begin
		if rising_edge(rx_usrclk) then
			if reset_in='0' then
        rx_data_in_s(63 downto 0)   <= rx_data_in;
        rx_data_in_s(127 downto 64) <= rx_data_in_s(63 downto 0);
        rx_chariscomma_in_s(7 downto 0)  <= rx_chariscomma_in;
        rx_chariscomma_in_s(15 downto 8) <= rx_chariscomma_in_s(7 downto 0);
        
        rx_charisk_in_s(7 downto 0)  <= rx_charisk_in;
        rx_charisk_in_s(15 downto 8) <= rx_charisk_in_s(7 downto 0);
        
        three_commas_found_out<=three_commas(15 downto 8);

			else
        rx_data_in_s<=(others=>'0');
        rx_chariscomma_in_s<=(others=>'0');
        rx_charisk_in_s<=(others=>'0');
        three_commas_found_out<=(others=>'0');

			end if;
		end if;

	end process;


  --find three commas
  -- Checks the stream for three commas in a row and signals this in three_commas_out
  --this delays the data by one clk since we need to check for commas that can span over two chunks of 64 bit
  findThreeCommasProcess : process(rx_usrclk)
  variable carrier_extend_14_9 : std_logic ;
  variable carrier_extend_13_8 : std_logic ;
  variable carrier_extend_12_7 : std_logic ;
  variable carrier_extend_11_6 : std_logic ;
  variable carrier_extend_10_5 : std_logic ;
  variable carrier_extend_9_4 : std_logic ;
  variable carrier_extend_8_3 : std_logic ;
  variable carrier_extend_7_2 : std_logic ;
  begin
    if rising_edge(rx_usrclk) then
      if reset_in='0' then
        --15
        
        --three_commas(15 downto 8)<= three_commas(7 downto 0);
        --three_commas(7 downto 0)<=(others=>'0');
        if data8=CARRIER_EXTEND and rx_charisk_in_s(8)='1' and data7=CARRIER_EXTEND and rx_charisk_in_s(7)='1' then
           --three_commas(14 downto 9)<=(others=>'1');
           carrier_extend_14_9:= '1';
        else
            carrier_extend_14_9:= '0';
        end if;
        
        if data7=CARRIER_EXTEND and rx_charisk_in_s(7)='1' and data6=CARRIER_EXTEND and rx_charisk_in_s(6)='1' then
           -- three_commas(13 downto 8)<=(others=>'1');
                   carrier_extend_13_8:= '1';
        else
            carrier_extend_13_8:= '0';
        end if;
        
        if data6=CARRIER_EXTEND and rx_charisk_in_s(6)='1' and data5=CARRIER_EXTEND and rx_charisk_in_s(5)='1' then
            --three_commas(12 downto 7)<=(others=>'1');
                       carrier_extend_12_7:= '1';
        else
            carrier_extend_12_7:= '0';
        end if;
        
        if data5=CARRIER_EXTEND and rx_charisk_in_s(5)='1' and data4=CARRIER_EXTEND and rx_charisk_in_s(4)='1' then
            --three_commas(11 downto 6)<=(others=>'1');
                   carrier_extend_11_6:= '1';
        else
            carrier_extend_11_6:= '0';
        end if;
        
        if data4=CARRIER_EXTEND and rx_charisk_in_s(4)='1' and data3=CARRIER_EXTEND and rx_charisk_in_s(3)='1' then
            --three_commas(10 downto 5)<=(others=>'1');
                   carrier_extend_10_5:= '1';
        else
            carrier_extend_10_5:= '0';
        end if;
        
        if data3=CARRIER_EXTEND and rx_charisk_in_s(3)='1' and data2=CARRIER_EXTEND and rx_charisk_in_s(2)='1' then
            --three_commas(9 downto 4)<=(others=>'1');
                   carrier_extend_9_4:= '1';
        else
            carrier_extend_9_4:= '0';
        end if;
        
        if data2=CARRIER_EXTEND and rx_charisk_in_s(2)='1' and data1=CARRIER_EXTEND and rx_charisk_in_s(1)='1' then
            --three_commas(8 downto 3)<=(others=>'1');
                   carrier_extend_8_3:= '1';
        else
            carrier_extend_8_3:= '0';
        end if;
        
        if data1=CARRIER_EXTEND and rx_charisk_in_s(1)='1' and data0=CARRIER_EXTEND and rx_charisk_in_s(0)='1' then
            --three_commas(7 downto 2)<=(others=>'1');
                   carrier_extend_7_2:= '1';
        else
            carrier_extend_7_2:= '0';
        end if;
        
        --three_commas(15) <= data15=CARRIER_EXTEND and data
        three_commas(15) <=
          (rx_chariscomma_in_s(15) and rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) ) or
          three_commas(7);
        --14
        three_commas(14) <=
          (rx_chariscomma_in_s(14) and rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) ) or
          (rx_chariscomma_in_s(15) and rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) ) or
          three_commas(6) or carrier_extend_14_9;
        --13
        three_commas(13) <=
          (rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) ) or
          (rx_chariscomma_in_s(14) and rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) ) or
          (rx_chariscomma_in_s(15) and rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) ) or
          three_commas(5) or carrier_extend_14_9 or carrier_extend_13_8;
        --12
        three_commas(12) <=
          (rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) ) or
          (rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) ) or
          (rx_chariscomma_in_s(14) and rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) ) or
          (rx_chariscomma_in_s(15) and rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) ) or
          three_commas(4) or carrier_extend_14_9 or carrier_extend_13_8 or carrier_extend_12_7;
        --11
        three_commas(11) <=
          (rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) ) or
          (rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) ) or
          (rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) ) or
          (rx_chariscomma_in_s(14) and rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) ) or
          (rx_chariscomma_in_s(15) and rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) )or 
          three_commas(3) or carrier_extend_14_9 or carrier_extend_13_8 or carrier_extend_12_7 or carrier_extend_11_6;
        --10
        three_commas(10) <=
          (rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) ) or
          (rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) ) or
          (rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) ) or
          (rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) ) or
          (rx_chariscomma_in_s(14) and rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10)) or three_commas(2) or
          (rx_chariscomma_in_s(15) and rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11)) or carrier_extend_14_9 or carrier_extend_13_8 or carrier_extend_12_7 or carrier_extend_11_6 or carrier_extend_10_5;
        --9
        three_commas(9) <=
          (rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) and rx_chariscomma_in_s(5) ) or
          (rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) ) or
          (rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) ) or
          (rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) ) or
          (rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9)) or
          (rx_chariscomma_in_s(14) and rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10)) or carrier_extend_14_9 or carrier_extend_13_8 or carrier_extend_12_7 or carrier_extend_11_6 or carrier_extend_10_5 or carrier_extend_9_4;
        --8
        three_commas(8) <=
          (rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) and rx_chariscomma_in_s(4) ) or
          (rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) and rx_chariscomma_in_s(5) ) or
          (rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) ) or
          (rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) ) or
          (rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8)) or
          (rx_chariscomma_in_s(13) and rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9)) or carrier_extend_13_8 or carrier_extend_12_7 or carrier_extend_11_6 or carrier_extend_10_5 or carrier_extend_9_4 or carrier_extend_8_3;
        --7
        three_commas(7) <=
          (rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) and rx_chariscomma_in_s(4) ) or
          (rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) and rx_chariscomma_in_s(5) ) or
          (rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) ) or
          (rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7)) or
          (rx_chariscomma_in_s(12) and rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8)) or carrier_extend_12_7 or carrier_extend_11_6 or carrier_extend_10_5 or carrier_extend_9_4 or carrier_extend_8_3 or carrier_extend_7_2;
        --6
        three_commas(6) <=
          (rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) and rx_chariscomma_in_s(4) ) or
          (rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) and rx_chariscomma_in_s(5) ) or
          (rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6)) or
          (rx_chariscomma_in_s(11) and rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7)) or carrier_extend_11_6 or carrier_extend_10_5 or carrier_extend_9_4 or carrier_extend_8_3 or carrier_extend_7_2;
        --5
        three_commas(5) <=
          (rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) and rx_chariscomma_in_s(4) ) or
          (rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) and rx_chariscomma_in_s(5) )or
          (rx_chariscomma_in_s(10) and rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6)) or carrier_extend_10_5 or carrier_extend_9_4 or carrier_extend_8_3 or carrier_extend_7_2;

        --4
        three_commas(4) <=
          (rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) and rx_chariscomma_in_s(4) )or
          (rx_chariscomma_in_s(9) and rx_chariscomma_in_s(7) and rx_chariscomma_in_s(5)) or carrier_extend_9_4 or carrier_extend_8_3 or carrier_extend_7_2;

        --3
        three_commas(3) <=
          (rx_chariscomma_in_s(8) and rx_chariscomma_in_s(6) and rx_chariscomma_in_s(4)) or carrier_extend_8_3 or carrier_extend_7_2;

        three_commas(2)<= carrier_extend_7_2;
        
        three_commas(1 downto 0 ) <= (others => '0');

      else
        three_commas <= (others => '0');
      end if;

    end if;


  end process;


end Behavioral;
