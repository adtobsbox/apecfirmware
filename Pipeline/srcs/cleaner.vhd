----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/08/2020 04:14:03 PM
-- Design Name: 
-- Module Name: cleaner - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cleaner is
    Port ( rx_usrclk : in STD_LOGIC;
           reset_in : in STD_LOGIC;
           valid_frame_in : in STD_LOGIC_VECTOR (7 downto 0);
           valid_frame_out : out STD_LOGIC_VECTOR(7 downto 0);
           rx_data_in : in STD_LOGIC_VECTOR (63 downto 0);
           rx_data_out : out STD_LOGIC_VECTOR (63 downto 0);
           rx_chariscomma_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_charisk_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_notintable_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_notintable_out : out STD_LOGIC_VECTOR (7 downto 0);
           rx_disperr_in : in STD_LOGIC_VECTOR (7 downto 0);
           rx_disperr_out : out STD_LOGIC_VECTOR (7 downto 0);
           data_avail_out : out STD_LOGIC
           );
end cleaner;

architecture Behavioral of cleaner is
component pipeline_concat is
	generic (width :    positive := 8);
	Port ( data_in : in STD_LOGIC_VECTOR (8*width-1 downto 0);
		data_keep  : in  STD_LOGIC_VECTOR (7 downto 0);
		data_out   : out STD_LOGIC_VECTOR (8*width-1 downto 0);
		data_avail : out STD_LOGIC;
		clk        : in  STD_LOGIC;
		reset      : in  std_logic);
end component;

type cleanerStateType is (idle,frame);
signal cleanerState : cleanerStateType:=idle;
signal flush : STD_LOGIC;

signal concat_data_in_s    : STD_LOGIC_VECTOR (87 downto 0);
signal concat_data_keep_s  : STD_LOGIC_VECTOR (7 downto 0);
signal concat_data_out_s   : STD_LOGIC_VECTOR (87 downto 0);
signal concat_data_avail_s : STD_LOGIC;

signal valid_frame_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal rx_data_in_s : STD_LOGIC_VECTOR(63 downto 0);
signal rx_chariscomma_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal rx_charisk_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal rx_notintable_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal rx_disperr_in_s : STD_LOGIC_VECTOR(7 downto 0);
signal rx_chariscomma_in_s_last_lsb: std_logic;

begin
concat : pipeline_concat generic map (width => 11) port map (data_in => concat_data_in_s, data_keep => concat_data_keep_s, data_out => concat_data_out_s,data_avail => concat_data_avail_s,clk => rx_usrclk,reset => reset_in);

inputProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			valid_frame_in_s<=valid_frame_in;
			rx_data_in_s<=rx_data_in;
			rx_chariscomma_in_s<=rx_chariscomma_in;
			rx_charisk_in_s<=rx_charisk_in;
			rx_notintable_in_s<=rx_notintable_in;
			rx_disperr_in_s<=rx_disperr_in;
			data_avail_out<=concat_data_avail_s;

			concat_data_in_s(87 downto 80)<=rx_data_in_s(63 downto 56);
			rx_data_out(63 downto 56)<=concat_data_out_s(87 downto 80);
			
			concat_data_in_s(79)<=rx_disperr_in_s(7);
			rx_disperr_out(7)<=concat_data_out_s(79);

			concat_data_in_s(78)<=rx_notintable_in_s(7);
			rx_notintable_out(7)<=concat_data_out_s(78);

			concat_data_in_s(77)<=valid_frame_in_s(7);
			valid_frame_out(7)<=concat_data_out_s(77);

			concat_data_in_s(76 downto 69)<=rx_data_in_s(55 downto 48);
			rx_data_out(55 downto 48)<=concat_data_out_s(76 downto 69);

			concat_data_in_s(68)<=rx_disperr_in_s(6);
			rx_disperr_out(6)<=concat_data_out_s(68);

			concat_data_in_s(67)<=rx_notintable_in_s(6);
			rx_notintable_out(6)<=concat_data_out_s(67);

			concat_data_in_s(66)<=valid_frame_in_s(6);
			valid_frame_out(6)<=concat_data_out_s(66);

			concat_data_in_s(65 downto 58)<=rx_data_in_s(47 downto 40);
			rx_data_out(47 downto 40)<=concat_data_out_s(65 downto 58);
			
			concat_data_in_s(57)<=rx_disperr_in_s(5);
			rx_disperr_out(5)<=concat_data_out_s(57);

			concat_data_in_s(56)<=rx_notintable_in_s(5);
			rx_notintable_out(5)<=concat_data_out_s(56);
			
			concat_data_in_s(55)<=valid_frame_in_s(5);
			valid_frame_out(5)<=concat_data_out_s(55);

			concat_data_in_s(54 downto 47)<=rx_data_in_s(39 downto 32);
			rx_data_out(39 downto 32)<=concat_data_out_s(54 downto 47);

			concat_data_in_s(46)<=rx_disperr_in_s(4);
			rx_disperr_out(4)<=concat_data_out_s(46);

			concat_data_in_s(45)<=rx_notintable_in_s(4);
			rx_notintable_out(4)<=concat_data_out_s(45);

			concat_data_in_s(44)<=valid_frame_in_s(4);
			valid_frame_out(4)<=concat_data_out_s(44);

			concat_data_in_s(43 downto 36)<=rx_data_in_s(31 downto 24);
			rx_data_out(31 downto 24)<=concat_data_out_s(43 downto 36);


			concat_data_in_s(35)<=rx_disperr_in_s(3);
			rx_disperr_out(3)<=concat_data_out_s(35);

			concat_data_in_s(34)<=rx_notintable_in_s(3);
			rx_notintable_out(3)<=concat_data_out_s(34);

			concat_data_in_s(33)<=valid_frame_in_s(3);
			valid_frame_out(3)<=concat_data_out_s(33);

			concat_data_in_s(32 downto 25)<=rx_data_in_s(23 downto 16);
			rx_data_out(23 downto 16)<=concat_data_out_s(32 downto 25);

			concat_data_in_s(24)<=rx_disperr_in_s(2);
			rx_disperr_out(2)<=concat_data_out_s(24);

			concat_data_in_s(23)<=rx_notintable_in_s(2);
			rx_notintable_out(2)<=concat_data_out_s(23);

			concat_data_in_s(22)<=valid_frame_in_s(2);
			valid_frame_out(2)<=concat_data_out_s(22);

			concat_data_in_s(21 downto 14)<=rx_data_in_s(15 downto 8);
			rx_data_out(15 downto 8)<=concat_data_out_s(21 downto 14);	

			concat_data_in_s(13)<=rx_disperr_in_s(1);
			rx_disperr_out(1)<=concat_data_out_s(13);

			concat_data_in_s(12)<=rx_notintable_in_s(1);
			rx_notintable_out(1)<=concat_data_out_s(12);

			concat_data_in_s(11)<=valid_frame_in_s(1);
			valid_frame_out(1)<=concat_data_out_s(11);

			concat_data_in_s(10 downto 3)<=rx_data_in_s(7 downto 0);
			rx_data_out(7 downto 0)<=concat_data_out_s(10 downto 3);

			concat_data_in_s(2)<=rx_disperr_in_s(0);
			rx_disperr_out(0)<=concat_data_out_s(2);

			concat_data_in_s(1)<=rx_notintable_in_s(0);
			rx_notintable_out(0)<=concat_data_out_s(1);

			concat_data_in_s(0)<=valid_frame_in_s(0);
			valid_frame_out(0)<=concat_data_out_s(0);

			rx_chariscomma_in_s_last_lsb<=rx_chariscomma_in_s(0);


		else
			valid_frame_in_s<=(others=>'0');
			valid_frame_out<=(others=>'0');
			rx_data_in_s<=(others=>'0');
			rx_data_out<=(others=>'0');
			rx_chariscomma_in_s<=(others=>'0');
			rx_charisk_in_s<=(others=>'0');
			rx_notintable_in_s<=(others=>'0');
			rx_notintable_out<=(others=>'0');
			rx_disperr_in_s<=(others=>'0');
			rx_disperr_out<=(others=>'0');
			concat_data_in_s<=(others=>'0');
			rx_chariscomma_in_s_last_lsb<='0';
		end if;
	end if;

end process;

cleanerProcess : process(rx_usrclk)
begin
	if rising_edge(rx_usrclk) then
		if reset_in='0' then
			case(cleanerState) is
				when idle=>
					--makes sure that the start of each frame is aligned so msb of frame is in 63 downto 56 of rx_data_out
					if valid_frame_in_s="11111111" then
						--concat_data_keep_s<="11111111";
						concat_data_keep_s(7)<=not( rx_chariscomma_in_s_last_lsb or rx_chariscomma_in_s(7) or rx_charisk_in_s(7));
						concat_data_keep_s(6)<=not( rx_chariscomma_in_s(7) or rx_chariscomma_in_s(6) or rx_charisk_in_s(6));
						concat_data_keep_s(5)<=not( rx_chariscomma_in_s(6) or rx_chariscomma_in_s(5) or rx_charisk_in_s(5));
						concat_data_keep_s(4)<=not( rx_chariscomma_in_s(5) or rx_chariscomma_in_s(4) or rx_charisk_in_s(4));
						concat_data_keep_s(3)<=not( rx_chariscomma_in_s(4) or rx_chariscomma_in_s(3) or rx_charisk_in_s(3));
						concat_data_keep_s(2)<=not( rx_chariscomma_in_s(3) or rx_chariscomma_in_s(2) or rx_charisk_in_s(2));
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						cleanerState<=frame;
					elsif valid_frame_in_s="01111111" then
						--concat_data_keep_s<="11111111";
						concat_data_keep_s(7)<='1';
						concat_data_keep_s(6)<=not( rx_chariscomma_in_s(7) or rx_chariscomma_in_s(6) or rx_charisk_in_s(6));
						concat_data_keep_s(5)<=not( rx_chariscomma_in_s(6) or rx_chariscomma_in_s(5) or rx_charisk_in_s(5));
						concat_data_keep_s(4)<=not( rx_chariscomma_in_s(5) or rx_chariscomma_in_s(4) or rx_charisk_in_s(4));
						concat_data_keep_s(3)<=not( rx_chariscomma_in_s(4) or rx_chariscomma_in_s(3) or rx_charisk_in_s(3));
						concat_data_keep_s(2)<=not( rx_chariscomma_in_s(3) or rx_chariscomma_in_s(2) or rx_charisk_in_s(2));
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						cleanerState<=frame;
					elsif valid_frame_in_s="00111111" then
						concat_data_keep_s(7)<='1';
						concat_data_keep_s(6)<='1';
						concat_data_keep_s(5)<=not( rx_chariscomma_in_s(6) or rx_chariscomma_in_s(5) or rx_charisk_in_s(5));
						concat_data_keep_s(4)<=not( rx_chariscomma_in_s(5) or rx_chariscomma_in_s(4) or rx_charisk_in_s(4));
						concat_data_keep_s(3)<=not( rx_chariscomma_in_s(4) or rx_chariscomma_in_s(3) or rx_charisk_in_s(3));
						concat_data_keep_s(2)<=not( rx_chariscomma_in_s(3) or rx_chariscomma_in_s(2) or rx_charisk_in_s(2));
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						--concat_data_keep_s<="11111111";
						cleanerState<=frame;
					elsif valid_frame_in_s="00011111" then
					    concat_data_keep_s(7)<='1';
						concat_data_keep_s(6)<='1';
						concat_data_keep_s(5)<='1';
						concat_data_keep_s(4)<=not( rx_chariscomma_in_s(5) or rx_chariscomma_in_s(4) or rx_charisk_in_s(4));
						concat_data_keep_s(3)<=not( rx_chariscomma_in_s(4) or rx_chariscomma_in_s(3) or rx_charisk_in_s(3));
						concat_data_keep_s(2)<=not( rx_chariscomma_in_s(3) or rx_chariscomma_in_s(2) or rx_charisk_in_s(2));
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						--concat_data_keep_s<="11111111";
						cleanerState<=frame;
					elsif valid_frame_in_s="00001111" then
						--concat_data_keep_s<="11111111";
						concat_data_keep_s(7)<='1';
						concat_data_keep_s(6)<='1';
						concat_data_keep_s(5)<='1';
						concat_data_keep_s(4)<='1';
						concat_data_keep_s(3)<=not( rx_chariscomma_in_s(4) or rx_chariscomma_in_s(3) or rx_charisk_in_s(3));
						concat_data_keep_s(2)<=not( rx_chariscomma_in_s(3) or rx_chariscomma_in_s(2) or rx_charisk_in_s(2));
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						cleanerState<=frame;
					elsif valid_frame_in_s="00000111" then
						--concat_data_keep_s<="11111111";
						concat_data_keep_s(7)<='1';
						concat_data_keep_s(6)<='1';
						concat_data_keep_s(5)<='1';
						concat_data_keep_s(4)<='1';
						concat_data_keep_s(3)<='1';
						concat_data_keep_s(2)<=not( rx_chariscomma_in_s(3) or rx_chariscomma_in_s(2) or rx_charisk_in_s(2));
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						cleanerState<=frame;
					elsif valid_frame_in_s="00000011" then
					concat_data_keep_s(7)<='1';
					concat_data_keep_s(6)<='1';
					concat_data_keep_s(5)<='1';
					concat_data_keep_s(4)<='1';
					concat_data_keep_s(3)<='1';
					concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<=not( rx_chariscomma_in_s(2) or rx_chariscomma_in_s(1) or rx_charisk_in_s(1));
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						--concat_data_keep_s<="11111111";
						cleanerState<=frame;
					elsif valid_frame_in_s="00000001" then
						--concat_data_keep_s<="11111111";
						concat_data_keep_s(7)<='1';
						concat_data_keep_s(6)<='1';
						concat_data_keep_s(5)<='1';
						concat_data_keep_s(4)<='1';
						concat_data_keep_s(3)<='1';
						concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<=not( rx_chariscomma_in_s(1) or rx_chariscomma_in_s(0) or rx_charisk_in_s(0));
						cleanerState<=frame;
					else
						if flush='1' then
							concat_data_keep_s<="11111111";
							flush<='0';
						else
							concat_data_keep_s<="00000000";
						end if;
					end if;
				-- makes sure that the frame is flushed through the concat
				when frame=>
					flush<='1';
					if valid_frame_in_s="11111110" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						if  rx_chariscomma_in_s(6)='1' or rx_chariscomma_in_s(5)='1' or rx_charisk_in_s(5)='1' then
							concat_data_keep_s(5)<='0';
						else
							concat_data_keep_s(5)<='1';
						end if;	
						if  rx_chariscomma_in_s(5)='1' or rx_chariscomma_in_s(4)='1' or rx_charisk_in_s(4)='1' then
							concat_data_keep_s(4)<='0';
						else
							concat_data_keep_s(4)<='1';
						end if;	
						if  rx_chariscomma_in_s(4)='1' or rx_chariscomma_in_s(3)='1' or rx_charisk_in_s(3)='1' then
							concat_data_keep_s(3)<='0';
						else
							concat_data_keep_s(3)<='1';
						end if;
						if  rx_chariscomma_in_s(3)='1' or rx_chariscomma_in_s(2)='1' or rx_charisk_in_s(2)='1' then
							concat_data_keep_s(2)<='0';
						else
							concat_data_keep_s(2)<='1';
						end if;		
						if  rx_chariscomma_in_s(2)='1' or rx_chariscomma_in_s(1)='1' or rx_charisk_in_s(1)='1' then
							concat_data_keep_s(1)<='0';
						else
							concat_data_keep_s(1)<='1';
						end if;	
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="11111100" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						if  rx_chariscomma_in_s(6)='1' or rx_chariscomma_in_s(5)='1' or rx_charisk_in_s(5)='1' then
							concat_data_keep_s(5)<='0';
						else
							concat_data_keep_s(5)<='1';
						end if;	
						if  rx_chariscomma_in_s(5)='1' or rx_chariscomma_in_s(4)='1' or rx_charisk_in_s(4)='1' then
							concat_data_keep_s(4)<='0';
						else
							concat_data_keep_s(4)<='1';
						end if;	
						if  rx_chariscomma_in_s(4)='1' or rx_chariscomma_in_s(3)='1' or rx_charisk_in_s(3)='1' then
							concat_data_keep_s(3)<='0';
						else
							concat_data_keep_s(3)<='1';
						end if;
						if  rx_chariscomma_in_s(3)='1' or rx_chariscomma_in_s(2)='1' or rx_charisk_in_s(2)='1' then
							concat_data_keep_s(2)<='0';
						else
							concat_data_keep_s(2)<='1';
						end if;		
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="11111000" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						if  rx_chariscomma_in_s(6)='1' or rx_chariscomma_in_s(5)='1' or rx_charisk_in_s(5)='1' then
							concat_data_keep_s(5)<='0';
						else
							concat_data_keep_s(5)<='1';
						end if;	
						if  rx_chariscomma_in_s(5)='1' or rx_chariscomma_in_s(4)='1' or rx_charisk_in_s(4)='1' then
							concat_data_keep_s(4)<='0';
						else
							concat_data_keep_s(4)<='1';
						end if;	
						if  rx_chariscomma_in_s(4)='1' or rx_chariscomma_in_s(3)='1' or rx_charisk_in_s(3)='1' then
							concat_data_keep_s(3)<='0';
						else
							concat_data_keep_s(3)<='1';
						end if;
						concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="11110000" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						if  rx_chariscomma_in_s(6)='1' or rx_chariscomma_in_s(5)='1' or rx_charisk_in_s(5)='1' then
							concat_data_keep_s(5)<='0';
						else
							concat_data_keep_s(5)<='1';
						end if;	
						if  rx_chariscomma_in_s(5)='1' or rx_chariscomma_in_s(4)='1' or rx_charisk_in_s(4)='1' then
							concat_data_keep_s(4)<='0';
						else
							concat_data_keep_s(4)<='1';
						end if;	
						concat_data_keep_s(3)<='1';
						concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="11100000" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						if  rx_chariscomma_in_s(6)='1' or rx_chariscomma_in_s(5)='1' or rx_charisk_in_s(5)='1' then
							concat_data_keep_s(5)<='0';
						else
							concat_data_keep_s(5)<='1';
						end if;	
						concat_data_keep_s(4)<='1';
						concat_data_keep_s(3)<='1';
						concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="11000000" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						concat_data_keep_s(5)<='1';
						concat_data_keep_s(4)<='1';
						concat_data_keep_s(3)<='1';
						concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="10000000" then
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						concat_data_keep_s(6)<='1';
						concat_data_keep_s(5)<='1';
						concat_data_keep_s(4)<='1';
						concat_data_keep_s(3)<='1';
						concat_data_keep_s(2)<='1';
						concat_data_keep_s(1)<='1';
						concat_data_keep_s(0)<='1';
						cleanerState<=idle;
					elsif valid_frame_in_s="00000000" then
						concat_data_keep_s<="11111111";
						cleanerState<=idle;
					else
						--cleans out the commas and charisk
						if rx_chariscomma_in_s_last_lsb='1' or rx_chariscomma_in_s(7)='1' or rx_charisk_in_s(7)='1' then
							concat_data_keep_s(7)<='0';
						else
							concat_data_keep_s(7)<='1';
						end if;		
						if  rx_chariscomma_in_s(7)='1' or rx_chariscomma_in_s(6)='1' or rx_charisk_in_s(6)='1' then
							concat_data_keep_s(6)<='0';
						else
							concat_data_keep_s(6)<='1';
						end if;	
						if  rx_chariscomma_in_s(6)='1' or rx_chariscomma_in_s(5)='1' or rx_charisk_in_s(5)='1' then
							concat_data_keep_s(5)<='0';
						else
							concat_data_keep_s(5)<='1';
						end if;	
						if  rx_chariscomma_in_s(5)='1' or rx_chariscomma_in_s(4)='1' or rx_charisk_in_s(4)='1' then
							concat_data_keep_s(4)<='0';
						else
							concat_data_keep_s(4)<='1';
						end if;	
						if  rx_chariscomma_in_s(4)='1' or rx_chariscomma_in_s(3)='1' or rx_charisk_in_s(3)='1' then
							concat_data_keep_s(3)<='0';
						else
							concat_data_keep_s(3)<='1';
						end if;
						if  rx_chariscomma_in_s(3)='1' or rx_chariscomma_in_s(2)='1' or rx_charisk_in_s(2)='1' then
							concat_data_keep_s(2)<='0';
						else
							concat_data_keep_s(2)<='1';
						end if;		
						if  rx_chariscomma_in_s(2)='1' or rx_chariscomma_in_s(1)='1' or rx_charisk_in_s(1)='1' then
							concat_data_keep_s(1)<='0';
						else
							concat_data_keep_s(1)<='1';
						end if;	
						if  rx_chariscomma_in_s(1)='1' or rx_chariscomma_in_s(0)='1' or rx_charisk_in_s(0)='1' then
							concat_data_keep_s(0)<='0';
						else
							concat_data_keep_s(0)<='1';
						end if;	
					end if;
			end case;
		else
			cleanerState<=idle;
			flush<='0';
		end if;
	end if;

end process;


end Behavioral;
