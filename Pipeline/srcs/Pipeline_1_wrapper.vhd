--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2.1 (lin64) Build 2288692 Thu Jul 26 18:23:50 MDT 2018
--Date        : Wed Sep 23 12:29:15 2020
--Host        : PCBE15331 running 64-bit Ubuntu 18.04.5 LTS
--Command     : generate_target Pipeline_1_wrapper.bd
--Design      : Pipeline_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Pipeline_1_wrapper is
  port (
    din_dataFIFO : out STD_LOGIC_VECTOR ( 271 downto 0 );
    reset_in : in STD_LOGIC;
    rx_data : in STD_LOGIC_VECTOR ( 103 downto 0 );
    rx_usrclk : in STD_LOGIC;
    status : out STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en_dataFIFO : out STD_LOGIC
  );
end Pipeline_1_wrapper;

architecture STRUCTURE of Pipeline_1_wrapper is
  component Pipeline_1 is
  port (
    rx_usrclk : in STD_LOGIC;
    reset_in : in STD_LOGIC;
    rx_data : in STD_LOGIC_VECTOR ( 103 downto 0 );
    din_dataFIFO : out STD_LOGIC_VECTOR ( 271 downto 0 );
    wr_en_dataFIFO : out STD_LOGIC;
    status : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component Pipeline_1;
begin
Pipeline_1_i: component Pipeline_1
     port map (
      din_dataFIFO(271 downto 0) => din_dataFIFO(271 downto 0),
      reset_in => reset_in,
      rx_data(103 downto 0) => rx_data(103 downto 0),
      rx_usrclk => rx_usrclk,
      status(31 downto 0) => status(31 downto 0),
      wr_en_dataFIFO => wr_en_dataFIFO
    );
end STRUCTURE;
