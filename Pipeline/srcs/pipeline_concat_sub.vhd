----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/18/2020 01:31:17 PM
-- Design Name: 
-- Module Name: pipeline_concat_sub - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


--library ieee;
--use ieee.std_logic_1164.all;
--use ieee.numeric_std.all;

--package bus_multiplexer_pkg is
--	type bus_array is array(natural range <>) of std_logic_vector;
--	subtype byte_counter_type is integer range 0 to 8;
--end package;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_concat_sub is
	generic (width :    positive := 8);
	port ( data_in : in std_logic_vector( width*8 - 1 downto 0);
		keep_in     : in  std_logic_vector(7 downto 0);
		counter_in  : in  integer range 0 to 8;
		clk         : in  std_logic;
		reset       : in  std_logic;
		data_out    : out std_logic_vector(width*8 - 1 downto 0);
		keep_out    : out std_logic_vector(7 downto 0);
		counter_out : out integer range 0 to 8);
end pipeline_concat_sub;

architecture Behavioral of pipeline_concat_sub is
 	--own types
 	type pipeline_array is array(7 downto 0) of std_logic_vector(width- 1 downto 0);
 	subtype byte_counter_type is integer range 0 to 8;
	
	signal data_in_s     : pipeline_array;
	signal keep_in_s     : std_logic_vector(7 downto 0);
	signal counter_in_s  : byte_counter_type;
	signal data_out_s    : pipeline_array;
	signal keep_out_s    : std_logic_vector(7 downto 0);
	signal counter_out_s : byte_counter_type;

begin

	ioprocess : process(clk)
	begin
		if rising_edge(clk) then
			if reset='0' then
				for I in 7 downto 0 loop
					data_in_s(I) <= data_in((I+1)*width-1 downto I*width);
				end loop;				
				keep_in_s    <= keep_in;
				counter_in_s <= counter_in;
				for I in 7 downto 0 loop
					data_out((I+1)*width-1 downto I*width) <= data_out_s(I);
				end loop;	
				keep_out     <= keep_out_s;
				counter_out  <= counter_out_s;
			else
				
				keep_in_s    <= (others=>'0');
				counter_in_s <= 0;
				for I in 7 downto 0 loop
					data_out((I+1)*width-1 downto I*width)     <= (others=>'0');
					data_in_s(I)    <= (others=>'0');
				end loop;
				keep_out     <= (others=>'0');
				counter_out  <= 0;
			end if;
		end if; 
	end process;

	mainprocess : process(clk)
	begin
		if rising_edge(clk) then
			if reset='0' then
				--skip first byte
				if keep_in_s(7)='0' then
					if counter_in_s=0 then
						for I in 6 downto 0 loop
							data_out_s(I+1) <= data_in_s(I);
						end loop;
					elsif counter_in_s=1 then
						data_out_s(7) <= data_in_s(7);
						for I in 5 downto 0 loop
							data_out_s(I+1) <= data_in_s(I);
						end loop;
					elsif counter_in_s=2 then
						for I in 7 downto 6 loop
							data_out_s(I) <= data_in_s(I);
						end loop;
						for I in 4 downto 0 loop
							data_out_s(I+1) <= data_in_s(I);
						end loop;
					elsif counter_in_s=3 then
						for I in 7 downto 5 loop
							data_out_s(I) <= data_in_s(I);
						end loop;
						for I in 3 downto 0 loop
							data_out_s(I+1) <= data_in_s(I);
						end loop;
					elsif counter_in_s=4 then
						for I in 7 downto 4 loop
							data_out_s(I) <= data_in_s(I);
						end loop;
						for I in 2 downto 0 loop
							data_out_s(I+1) <= data_in_s(I);
						end loop;
					elsif counter_in_s=5 then
						for I in 7 downto 3 loop
							data_out_s(I) <= data_in_s(I);
						end loop;
						for I in 1 downto 0 loop
							data_out_s(I+1) <= data_in_s(I);
						end loop;
					elsif counter_in_s=6 then
						for I in 7 downto 2 loop
							data_out_s(I) <= data_in_s(I);
						end loop;
						data_out_s(1) <= data_in_s(0);
					else
						for I in 7 downto 1 loop
							data_out_s(I) <= data_in_s(I);
						end loop;
					end if;
					data_out_s(0) <= (others => '0');
					counter_out_s <= counter_in_s;
				else
					for I in 7 downto 0 loop
						data_out_s(I) <= data_in_s(I);
					end loop;
					counter_out_s <= counter_in_s+1;
				end if;
				keep_out_s <= keep_in_s(6 downto 0)&'0';
			else
				for I in data_out_s' range loop
					data_out_s(I) <= (others => '0');
				end loop;
				keep_out_s <= (others => '0');
				counter_out_s<=0;
			end if;
		end if;

	end process;


end Behavioral;
