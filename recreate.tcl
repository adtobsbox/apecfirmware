#For you to modify where you want the projects
#----------------------------------------------

#where Pipeline and 10_port_transceivers IP repositories will end up
set ip_repo "/home/msoderen/vivadoProjects_test3/ip_repo"
set modelsim_library "/home/msoderen/vivadoProjects/simlibraries"
set project_dir "/home/msoderen/vivadoProjects_test3"

#----------------------------------------------
exec mkdir -p ${ip_repo}

source ./10_port_transceivers/recreate.tcl
update_compile_order -fileset sources_1
ipx::package_project -root_dir ${ip_repo}/10_port_transceivers_repo -vendor cern.ch -library user -taxonomy /UserIP  -import_files

#set_property value 50000000 [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces S_AXI_DRP -of_objects [ipx::current_core] ]]
set_property core_revision 58 [ipx::current_core]
set_property version 2.6 [ipx::current_core]
#set_property vendorDisplayName CERN [ipx::current_core]
#set_property vendorURL http://www.cern.ch [ipx::current_core]

#ipx::infer_bus_interface clk_125 xilinx.com:signal:clock_rtl:1.0 [ipx::current_core]
#ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces clk_125 -of_objects [ipx::current_core]]


ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property  ip_repo_paths  ${ip_repo}/10_port_transceivers_repo [current_project]


source ./Pipeline/recreate.tcl
update_compile_order -fileset sources_1
ipx::package_project -root_dir ${ip_repo}/Pipeline_repo -vendor user.org -library user -taxonomy /UserIP -module Pipeline_1 -import_files

set_property core_revision 2 [ipx::find_open_core user.org:user:Pipeline_1:1.0]
ipx::create_xgui_files [ipx::find_open_core user.org:user:Pipeline_1:1.0]
ipx::update_checksums [ipx::find_open_core user.org:user:Pipeline_1:1.0]
ipx::save_core [ipx::find_open_core user.org:user:Pipeline_1:1.0]
set_property  ip_repo_paths  ${ip_repo}/Pipeline_repo [current_project]
update_ip_catalog
source ./APECFirmware/recreate.tcl
