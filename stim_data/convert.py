import argparse
import csv
import os

def mainFunction(filename):
    new_file=os.path.splitext(filename)[0]+".txt"
    with open(new_file, 'w') as output:
        with open(filename, "r") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for lines in csv_reader:
                if(len(lines[3])==26):
                    print(lines[3])
                    binary_data=bin(int(lines[3], 16))[2:].zfill(103)
                    output.write(binary_data+"\n")

if __name__ == "__main__":
  parser=argparse.ArgumentParser()
  parser.add_argument("-filename","-f",required=True)
  args = parser.parse_args()
  bunchesToPlot=[]
  mainFunction(args.filename)
